package cz.cvut.fel.michalik.api;

import com.google.gson.JsonElement;
import cz.cvut.fel.michalik.Properties;
import cz.cvut.fel.michalik.Utils;
import cz.cvut.fel.michalik.navigation.Graph;
import cz.cvut.fel.michalik.navigation.Itinerary;
import cz.cvut.fel.michalik.navigation.Path;
import cz.cvut.fel.michalik.navigation.Route;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Spring Web API for itinerary generation and graph routing
 */
@SpringBootApplication
@RestController
public class Application {

    private static Graph g = null;
    // 15 km/h default speed, used for spacing out instructions
    private final String DEFAULT_SPEED = String.valueOf(Properties.getSettings().get("DEFAULT_SPEED"));

    public static void main(String[] args) {
        g = new Graph();
        try {
            Logger.getLogger(Application.class.getName()).log(Level.INFO, "Loading graph");
            g.loadFromGeoJSON((String) Properties.getSettings().get("FILENAME"));
            Logger.getLogger(Application.class.getName()).log(Level.INFO, String.format("Graph with %d nodes and %d edges loaded%n", g.getNodes().size(), g.getEdges().size()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        SpringApplication.run(Application.class, args);
    }

    @CrossOrigin
    @GetMapping("/getItinerary")
    public String getItinerary(@RequestParam(value = "start") String start, @RequestParam(value = "end") String end) {
        Route route = Route.findRoute(Long.parseLong(start), Long.parseLong(end), g);
        return route.getItinerary().toString();
    }

    @CrossOrigin
    @GetMapping("/getItineraryList")
    public List<Map<String, Object>> getItineraryList(@RequestParam(value = "start") String start, @RequestParam(value = "end") String end) {
        Route route = Route.findRoute(Long.parseLong(start), Long.parseLong(end), g);
        return route.getItinerary().toList();
    }

    @CrossOrigin("*")
    @GetMapping(value = "/getRoute", params = "routeId", produces = "application/json")
    public String getPredefinedRoute(@RequestParam(value = "routeId") String routeId,
                                     @RequestParam(value = "speed", defaultValue = "") String speedString) {
        int routeIndex = Integer.parseInt(routeId);
        List<String> predefinedRoutes = Route.getPredefinedRoutes();
        if (routeIndex < 0 || routeIndex >= predefinedRoutes.size())
            return Utils.getGson().toJson(Map.of("error", "Invalid route ID"));
        List<Geometry> geoms = Utils.geoJSONCollectionToGeometries(new StringReader(predefinedRoutes.get(routeIndex)));
        Path path = Path.pathFromEdges(g, geoms);
        Route route = new Route(g, path, null);
        return getRouteResponse(route, speedString);
    }

    @CrossOrigin("*")
    @GetMapping(value = "/getRoute", produces = "application/json")
    public String getRoute(@RequestParam(value = "start") String start, @RequestParam(value = "end") String end,
                           @RequestParam(value = "distWeight", defaultValue = "") String distanceWeight,
                           @RequestParam(value = "secondaryRoadWeight", defaultValue = "") String secondaryRoadWeight,
                           @RequestParam(value = "tertiaryRoadWeight", defaultValue = "") String tertiaryRoadWeight,
                           @RequestParam(value = "sidewalksWeight", defaultValue = "") String sidewalksWeight,
                           @RequestParam(value = "onewaysWeight", defaultValue = "") String onewaysWeight,
                           @RequestParam(value = "speed", defaultValue = "") String speedString) {
        Map<String, Double> weights = new HashMap<>();
        if (!distanceWeight.equals("")) weights.put("distance", Double.parseDouble(distanceWeight));
        if (!secondaryRoadWeight.equals("")) weights.put("secondaryRoad", Double.parseDouble(secondaryRoadWeight));
        if (!tertiaryRoadWeight.equals("")) weights.put("tertiaryRoad", Double.parseDouble(tertiaryRoadWeight));
        if (!sidewalksWeight.equals("")) weights.put("sidewalks", Double.parseDouble(sidewalksWeight));
        if (!onewaysWeight.equals("")) weights.put("oneways", Double.parseDouble(onewaysWeight));
        // Check weights
        for (double weight : weights.values()) {
            if (weight < 0) return Utils.getGson().toJson(Map.of("error", "Weights must be in range greater than 0"));
        }
        Route route = Route.findRoute(Long.parseLong(start), Long.parseLong(end), g, weights);
        return getRouteResponse(route, speedString);
    }

    private String getRouteResponse(Route route, String speedString) {
        // Return the route as a list of GeoJSON LineStrings
        List<Geometry> geometries = route.getPath().getEdges().stream().map(edge -> edge.getLineString()).collect(Collectors.toList());
        // LinkedHashMap for keeping the order of the keys -> the first entry of each feature is its type
        List<LinkedHashMap<String, Object>> geojsonGeometries = geometries.stream().map(geom -> {
            LinkedHashMap<String, Object> feature = new LinkedHashMap<>();
            feature.put("type", "Feature");
            feature.put("geometry", Utils.getGson().fromJson(Utils.geoToGeoJSON(geom, true), JsonElement.class));
            return feature;
        }).toList();
        // Parse speed
        double speed = Double.parseDouble(DEFAULT_SPEED);
        if (speedString != null && !speedString.equals("")) {
            try {
                speed = Double.parseDouble(speedString);
            } catch (NumberFormatException ignored) {
                // Ignore
            }
        }
        if (speed <= 0) {
            return Utils.getGson().toJson(Map.of("error", "Speed must be greater than 0 km/h"));
        }
        // Itinerary = list of instructions
        Itinerary itinerary = route.createItinerary(speed).reduceItinerary();
        LinkedHashMap<String, Object> geometry = new LinkedHashMap<>();
        geometry.put("type", "FeatureCollection");
        geometry.put("features", geojsonGeometries);
        double timeInSeconds = route.getPath().getLength() / (speed / 3.6);
        Map<String, Object> response = Map.of(
                "instruction", itinerary.toList(),
                "geometry", geometry,
                "landmark", g.getAllItineraryLandmarks(itinerary),
                "length", Utils.distanceFormat(route.getPath().getLength()),
                "time", timeInSeconds,
                "timeString", DurationFormatUtils.formatDurationWords(1000 * 60 * Math.round(Math.ceil(timeInSeconds / 60)), true, true),
                "elevation", Math.round(route.getPath().getTotalElevation(0, route.getPath().getEdges().size())),
                "numDecisionPoints", itinerary.getItineraryList().stream()
                        .filter(item -> item.getType().equals(Itinerary.ItineraryItem.ItineraryItemType.Decision))
                        .count()
        );

        return Utils.getGson().toJson(response);
    }

    @CrossOrigin("*")
    @GetMapping(value = "/getClosestNode", produces = "application/json")
    public Map<String, Object> getClosestPoint(@RequestParam(value = "lng") String lng, @RequestParam(value = "lat") String lat) {
        long closestID = g.findClosestNodeID(Double.parseDouble(lng), Double.parseDouble(lat));
        Coordinate coord = Utils.projectBack(g.getNodeByID(closestID).orElseThrow().getAdjacentEdges().get(0).getLineString().getStartPoint()).getCoordinate();
        return Map.of("id", closestID, "lng", coord.x, "lat", coord.y);
    }

    @CrossOrigin("*")
    @GetMapping(value = "/getAllLandmarks", produces = "application/json")
    public String getAllLandmarks() {
        return Utils.getGson().toJson(g.getAllLandmarks());
    }

    @CrossOrigin("*")
    @GetMapping(value = "/getBounds", produces = "application/json")
    public String getBounds() {
        return g.getDatasetBounds();
    }

    @GetMapping("/api")
    public Object index() {
        return Map.of("resources", List.of("/getItinerary", "/getItineraryList", "/getRoute"));
    }

}