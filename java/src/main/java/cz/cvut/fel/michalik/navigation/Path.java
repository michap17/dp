package cz.cvut.fel.michalik.navigation;

import cz.cvut.fel.michalik.Utils;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.tuple.Pair;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Path used for the route and the itinerary.
 * It is essentially just a list of edges with more functionality.
 */
public class Path {
    private final Graph graph;
    private List<Edge> edges;
    private LineString lineString = null;

    public Path(Graph graph) {
        this.graph = graph;
    }

    public Path(Graph graph, List<Edge> edges) {
        this.graph = graph;
        this.edges = edges;
    }

    /**
     * Snaps geometries to edges of the graph and returns this path.
     * This is used during the route evaluation, where we test a set of predetermined routes.
     */
    public static Path pathFromEdges(Graph graph, List<Geometry> geoms) {
        List<Edge> edges = new LinkedList<>();
        Node lastSnappedNode;
        for (int i = 0; i < geoms.size(); ++i) {
            if (geoms.get(i) instanceof LineString lineString) {
                LineString lineStringProjected = (LineString) Utils.projectBack(lineString);
                if (i == 0) {
                    Coordinate initialCoordinate = lineStringProjected.getStartPoint().getCoordinate();
                    long snappedPointId = graph.findClosestNodeID(initialCoordinate.getX(), initialCoordinate.getY());
                    lastSnappedNode = graph.getNodeByID(snappedPointId).orElseThrow();
                } else {
                    lastSnappedNode = edges.get(edges.size() - 1).getTarget();
                }
                Edge closestEdge = null;
                double closestEdgeScore = Double.MAX_VALUE;
                // Try every consequent edge and measure the distance between the target points
                // This snapping is sufficient for this project (GeoJSONs are exported from the same graph network),
                // however a more robust solution would be needed for general GeoJSONs
                for (Edge e : lastSnappedNode.getAdjacentEdges()) {
                    double score = e.getLineString().getEndPoint().distance(lineString.getEndPoint());
                    boolean isContained = lineString.contains(e.getLineString().getEndPoint());
                    if (closestEdge == null || score < closestEdgeScore || isContained) {
                        closestEdge = e;
                        closestEdgeScore = score;
                    }
                }
                edges.add(closestEdge);
                if (closestEdgeScore > 0)
                    Logger.getLogger(Path.class.getName()).log(Level.WARNING, String.format("%d-th linestring not snapped perfectly, gap=%f%n\t%s", i, closestEdgeScore, Utils.geoToGeoJSON(lineString, true)));
            } else {
                throw new NotImplementedException(String.format("Only supported GeoJSONs are GeometryCollections with LineStrings (%d-th geometry is not a LineString)", i));
            }
        }
        Logger.getLogger(Path.class.getName()).log(Level.INFO, String.format("Loaded path with %d edges (%d geometries)", edges.size(), geoms.size()));
        return new Path(graph, edges);
    }

    /**
     * Returns lengths for each segment, where there is no need for instruction during the traversal of it
     * Each segment consists of one or more edges.
     *
     * @return Map of start IDs and lengths (in meters and in number of edges, returned as a Pair).
     */
    public Map<Integer, Pair<Double, Integer>> getSegmentLengths() {
        Map<Integer, Pair<Double, Integer>> lengthMap = new HashMap<>();
        if (edges.isEmpty()) return lengthMap;
        int lastId = 0;
        double accumulatedLength = edges.get(0).getLineString().getLength();
        for (int i = 0; i < this.edges.size() - 1; ++i) {
            Edge e1 = edges.get(i);
            Edge e2 = edges.get(i + 1);
            if (graph.isDecisionPoint(e1, e2)) {
                lengthMap.put(lastId, Pair.of(accumulatedLength, i));
                lastId = i + 1;
                accumulatedLength = 0.0;
            }
            accumulatedLength += e2.getLineString().getLength();
        }
        lengthMap.put(lastId, Pair.of(accumulatedLength, edges.size()));
        return lengthMap;
    }

    /**
     * Returns total elevation between the start and end indices of the path.
     */
    public double getTotalElevation(int start, int end) {
        double elevation = 0.0;
        for (int i = start; i < end; ++i) {
            Edge e1 = edges.get(i);
            elevation += Utils.doubleOrLongToDouble(graph.getEdgeProperties(e1.getId()).get("elevation"));
        }
        return elevation;
    }

    /**
     * Returns total length between the start and end indices of the path.
     */
    public double getTotalLength(int start, int end) {
        double length = 0.0;
        for (int i = start; i < end; ++i) {
            Edge e1 = edges.get(i);
            length += e1.getLineString().getLength();
        }
        return length;
    }

    /**
     * Returns the path as a LineString.
     */
    public LineString getLineString() {
        if (lineString == null) {
            GeometryFactory gf = Utils.getGeometryFactory(null);
            Set<Coordinate> coordinates = new LinkedHashSet<>();
            for (Edge edge : edges) {
                coordinates.addAll(List.of(edge.getLineString().getCoordinates()));
            }
            this.lineString = gf.createLineString(coordinates.toArray(new Coordinate[0]));
        }
        return lineString;
    }

    public List<Edge> getEdges() {
        return edges;
    }

    public double getLength() {
        return edges.stream().map(edge -> edge.getLineString().getLength()).mapToDouble(value -> value).sum();
    }

}
