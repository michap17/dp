package cz.cvut.fel.michalik.navigation;

public enum LandmarkPreposition {
    BEFORE("before"),
    AT("at"),
    AFTER("after");

    private final String name;

    LandmarkPreposition(String name) {
        this.name = name;
    }

    public static LandmarkPreposition angleToPreposition(double angle) {
        if (Math.abs(angle) <= 45 + 15) {
            return BEFORE;
        } else if (Math.abs(angle) >= 180 - 45 - 15) {
            return AFTER;
        } else { // AT is 90 +- 30 degrees
            return AT;
        }
    }

    @Override
    public String toString() {
        return this.name;
    }
}
