package cz.cvut.fel.michalik.navigation.instruction;

import org.apache.commons.lang3.StringUtils;

public class InstructionToken {
    String type;
    String text;
    Long id = null;

    public InstructionToken(String type, String text) {
        this.type = type;
        this.text = text.trim();
    }

    public InstructionToken(String text) {
        this("plain", text);
    }

    public InstructionToken(String type, String text, long id) {
        this(type, text);
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public String getText() {
        return text;
    }

    public long getId() {
        return id;
    }

    public void capitalize() {
        this.text = StringUtils.capitalize(this.text);
    }
}
