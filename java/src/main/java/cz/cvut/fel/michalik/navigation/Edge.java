package cz.cvut.fel.michalik.navigation;

import cz.cvut.fel.michalik.navigation.instruction.ApproachInstruction;
import cz.cvut.fel.michalik.navigation.instruction.ConfirmationInstruction;
import cz.cvut.fel.michalik.navigation.instruction.DecisionInstruction;
import org.locationtech.jts.geom.LineString;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Oriented edge of a Graph.
 * It contains source and target node, IDs, associated instructions, geometry,
 * maneuvers for all continuations from the target node, prior probabilities of each continuation,
 * the best continuation, and the shape of the junction.
 */
public class Edge implements Serializable {
    // Start and end of the edge
    private final Node source;
    private final Node target;
    // ID of edge
    private final long id;
    // OSM ID of the edge
    private final long osmId;
    // LineString for GeoJSON
    private LineString lineString;
    // Instructions associated with this edge
    private ApproachInstruction approachInstruction;
    private DecisionInstruction decisionInstruction;
    private ConfirmationInstruction confirmationInstruction;
    // Relative angles of adjacent edges, used for maneuvers
    private Map<Long, Integer> angles;
    // Maneuvers, they might depend on other outgoing edges (e.g. multiple edges straight ahead)
    private Map<Long, Maneuver> maneuvers;
    // Prior probability for each outgoing edge
    private Map<Long, Double> priors;
    // Edge that is the best continuation
    private Edge bestContinuation;
    // Shape of the intersection at the target node
    private Graph.JunctionType junctionType = Graph.JunctionType.None;

    public Edge(long id, long osmId, Node source, Node target) {
        this.id = id;
        this.osmId = osmId;
        this.source = source;
        this.target = target;
        this.angles = new HashMap<>();
        this.maneuvers = new HashMap<>();
        this.priors = new HashMap<>();
    }

    public long getId() {
        return id;
    }

    public long getOsmId() {
        return osmId;
    }

    public Node getSource() {
        return source;
    }

    public Node getTarget() {
        return target;
    }

    public LineString getLineString() {
        return lineString;
    }

    public void setLineString(LineString lineString) {
        this.lineString = lineString;
    }

    public boolean hasBestContinuation() {
        return bestContinuation != null;
    }

    public Edge getBestContinuation() {
        return bestContinuation;
    }

    public void setBestContinuation(Edge bestContinuation) {
        this.bestContinuation = bestContinuation;
    }

    public void addAdjacentEdge(long e2Id, int angle) {
        angles.put(e2Id, angle);
    }

    public void setManeuver(long e2Id, Maneuver maneuver) {
        maneuvers.put(e2Id, maneuver);
    }

    public Maneuver getManeuver(long e2Id) {
        return maneuvers.get(e2Id);
    }

    public void setPriors(Map<Long, Double> priors) {
        this.priors = priors;
    }

    public double getPrior(long e2Id) {
        if (!priors.containsKey(e2Id))
            throw new RuntimeException(String.format("Missing prior for %d -> %d (map has %d values)", id, e2Id, priors.size()));
        return priors.get(e2Id);
    }

    public int getAngle(long e2Id) {
        return angles.get(e2Id);
    }

    public ApproachInstruction getApproachInstruction() {
        return approachInstruction;
    }

    public void setApproachInstruction(ApproachInstruction approachInstruction) {
        this.approachInstruction = approachInstruction;
    }

    public DecisionInstruction getDecisionInstruction() {
        return decisionInstruction;
    }

    public void setDecisionInstruction(DecisionInstruction decisionInstruction) {
        this.decisionInstruction = decisionInstruction;
    }

    public ConfirmationInstruction getConfirmationInstruction() {
        return confirmationInstruction;
    }

    public void setConfirmationInstruction(ConfirmationInstruction confirmationInstruction) {
        this.confirmationInstruction = confirmationInstruction;
    }

    public Graph.JunctionType getJunctionType() {
        return junctionType;
    }

    public void setJunctionType(Graph.JunctionType junctionType) {
        this.junctionType = junctionType;
    }

    public Map<Long, Maneuver> getManeuvers() {
        return maneuvers;
    }
}
