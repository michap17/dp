package cz.cvut.fel.michalik.geojson;

import com.google.gson.JsonElement;
import cz.cvut.fel.michalik.NearbyCollector;
import cz.cvut.fel.michalik.Road;
import cz.cvut.fel.michalik.Utils;
import cz.cvut.fel.michalik.feature.Query;
import cz.cvut.fel.michalik.navigation.Landmark;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * GeoJSON feature
 * GSON library creates JSON from this class instance
 */
public class Feature {
    public final String type = "Feature";
    // Created by ST_AsGeoJSON
    public final JsonElement geometry;
    // Properties as Map
    public final Map<String, Object> properties;
    // OSM ID
    public long id;

    public Feature(String geometry, Map<String, Object> properties) {
        this.geometry = Utils.getGson().fromJson(geometry, JsonElement.class);
        this.properties = properties;
    }

    public Feature(long id, String geometry, Map<String, Object> properties) {
        this(geometry, properties);
        this.id = id;
    }

    public Feature(String geometry, Map<String, Object> properties, Road road) {
        this(geometry, properties);
        this.id = road.getID();
        this.properties.put("_osm", road.getOsmID());
        this.properties.put("name", road.getRoadData().name());
        //this.properties.put("_segment_count", road.getSegments().size());
        this.properties.put("class", road.getRoadData().wayClass());
        this.properties.put("footway", road.getRoadData().footway());
        this.properties.put("highway", road.getRoadData().highway());
        this.properties.put("bridge", road.getRoadData().bridge());
        this.properties.put("oneway", road.getRoadData().oneway());
        this.properties.put("elevation", road.getRoadData().elevation());
        this.properties.put("incline", road.getRoadData().incline());
    }

    public Feature(String geometry, Map<String, Object> properties, Road road, List<Map<String, Object>> poi) {
        this(geometry, properties, road);
        properties.put("_poi", poi);
    }

    public Feature(String geometry, Map<String, Object> properties, Road road, Map<String, List<Landmark>> landmarks) {
        this(geometry, properties, road);
        properties.put("landmarks", landmarks);
    }

    public Feature(NearbyCollector nc, Map<String, Query> properties, Map<Long, Feature> landmarkFeatures) {
        //this(nc.getRoad().roadToGeoJSON(), nc.mapToValues(properties), nc.getRoad(), nc.collectPOI());
        this(nc.getRoad().roadToGeoJSON(), new HashMap<>(), nc.getRoad(), nc.collectLandmarks(landmarkFeatures));
        this.properties.put("source", nc.getRoad().getSourceID());
        this.properties.put("target", nc.getRoad().getTargetID());
        nc.close();
    }

}
