package cz.cvut.fel.michalik.navigation.instruction;

import cz.cvut.fel.michalik.navigation.Landmark;
import cz.cvut.fel.michalik.navigation.LandmarkPreposition;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class DecisionInstruction extends Instruction implements Serializable {

    @Override
    public List<InstructionToken> toInstructionTokens(Landmark landmark) {
        List<InstructionToken> list = new LinkedList<>();
        if (!this.isEmpty()) {
            // Landmarks crossing the path are treated differently
            if (this.getLandmark().isCrossing()) {
                list.addAll(landmark.getCrossingTokens("decision"));
            } else {
                // Transform prepositions to be used as decision
                if (this.getLandmark().getPreposition() == LandmarkPreposition.AFTER) {
                    list.add(new InstructionToken("decision-preposition", "away from"));
                } else if (this.getLandmark().getPreposition() == LandmarkPreposition.BEFORE) {
                    list.add(new InstructionToken("decision-preposition", "towards"));
                } else {
                    list.add(new InstructionToken("decision-preposition", "by"));
                }
                list.addAll(getLandmark().toInstructionTokens("decision"));
            }
        }
        return list;
    }

    protected DecisionInstruction cloneInstruction(List<Landmark> landmarks) {
        DecisionInstruction instruction = new DecisionInstruction();
        instruction.landmarks = landmarks;
        return instruction;
    }

}
