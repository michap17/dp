package cz.cvut.fel.michalik.navigation.instruction;

import cz.cvut.fel.michalik.navigation.Landmark;
import cz.cvut.fel.michalik.navigation.LandmarkPreposition;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public abstract class Instruction implements Serializable {
    List<Landmark> landmarks = new LinkedList<>();

    public Landmark getLandmark() {
        if (!landmarks.isEmpty()) return landmarks.get(0);
        else return null;
    }

    public List<Landmark> getLandmarks() {
        return landmarks;
    }

    /**
     * Add landmarks for a newly created instruction
     *
     * @param landmarks List of landmarks
     * @return this
     */
    public Instruction setLandmarks(List<Landmark> landmarks) {
        this.landmarks = landmarks;
        if (this.landmarks.size() > 3) {
            this.landmarks = new LinkedList<>(this.landmarks.subList(0, 3));
        }
        return this;
    }

    /**
     * Remove landmarks that have already been passed based on their IDs.
     * Landmark is kept only if the preposition is stricly after the last known preposition.
     * Examples:
     * (Last: AT, Current: AFTER) -> KEPT
     * (Last: AT, Current: AT) -> REMOVED
     * (Last: AT, Current: BEFORE) -> REMOVED
     *
     * @param landmarkIDs Map of pairs [ID, Preposition]
     * @return this or cloned instruction
     */
    public Instruction removePassedLandmarks(Map<Long, LandmarkPreposition> landmarkIDs) {
        List<Landmark> landmarksFiltered = landmarks.stream()
                .filter(lm -> !landmarkIDs.containsKey(lm.getId()) || lm.getPreposition().ordinal() > landmarkIDs.get(lm.getId()).ordinal())
                .toList();
        if (landmarksFiltered.size() == landmarks.size()) {
            return this;
        } else {
            return cloneInstruction(landmarksFiltered);
        }
    }

    abstract Instruction cloneInstruction(List<Landmark> landmarksFiltered);

    /**
     * Returns a list of InstructionTokens for the selected landmark
     *
     * @param landmark Selected landmark
     * @return List of tokens
     */
    abstract List<InstructionToken> toInstructionTokens(Landmark landmark);

    /**
     * Returns a list of InstructionTokens for the most salient landmark*
     *
     * @return List of tokens
     */
    public List<InstructionToken> toInstructionTokens() {
        if (landmarks.isEmpty()) return new LinkedList<>();
        else return toInstructionTokens(getLandmark());
    }

    /**
     * Converts InstructionTokens to String
     *
     * @return String
     */
    public String toString() {
        List<String> strings = this.toInstructionTokens().stream().map(token -> token.getText()).toList();
        return String.join(" ", strings);
    }

    public boolean isEmpty() {
        return this.landmarks.isEmpty();
    }
}
