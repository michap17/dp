package cz.cvut.fel.michalik.navigation;

import com.google.gson.JsonElement;
import cz.cvut.fel.michalik.Properties;
import cz.cvut.fel.michalik.Utils;
import cz.cvut.fel.michalik.navigation.instruction.*;
import org.apache.commons.lang3.tuple.Pair;
import org.locationtech.jts.algorithm.Angle;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.Point;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Itinerary is a list of enriched instructions. Each instruction has its own type (class).
 * Structure of the itinerary are these instructions (repeated):
 * 1. DP->Edge \ Maneuver    == Decision/Orientation Instruction - user selects the right continuation inside the intersection
 * 2. Edge                  == Confirmation Instruction - user is reaffirmed that he chose the right direction
 * 3. Edge->DP               == Approach Instruction - user finds the correct intersection*
 * 4. DP->Edge + Maneuver
 * ...
 * N-1. Edge
 * N. Edge->DP
 * <p>
 * Example:
 * 1. Go towards the pharmacy.
 * 2. Go along the pub Hospoda.
 * 3. Before the hospital turn right*
 * 4. Turn right towards the shop Obchod and continue along the Ulice Street.
 */
public class Itinerary {
    public static final String JUNCTION_KEYSTRING = "junction";
    public static final String ELEVATION_KEYSTRING = "elevation";
    public static final String MANEUVER_KEYSTRING = "maneuver";
    // Speed in m/s (converted from km/h)
    private final double SPEED;
    private final Path path;
    private final Graph graph;
    private List<ItineraryItem> itineraryList;

    public Itinerary(Path path, Graph graph, double speed) {
        itineraryList = new LinkedList<>();
        this.path = path;
        this.graph = graph;
        this.SPEED = speed / 3.6;
        fillAllInstructions();
    }

    public List<ItineraryItem> getItineraryList() {
        return itineraryList;
    }

    public List<Map<String, Object>> toList() {
        List<Map<String, Object>> list = new LinkedList<>();
        if (itineraryList.isEmpty()) {
            return list;
        }
        for (ItineraryItem item : itineraryList) {
            List<InstructionToken> tokens = item.toInstructionTokens();
            // Skip empty items
            if (tokens.isEmpty()) continue;
            list.add(Map.of("text", item.toString(tokens),
                    "json", item,
                    "tokens", tokens));
        }
        return list;
    }

    @Override
    public String toString() {
        if (itineraryList.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < itineraryList.size(); ++i) {
            ItineraryItem item = itineraryList.get(i);
            sb.append(i + 1).append(". ").append(item)
                    .append("\n\t\t(Prior=").append(item.getPriorDecision())
                    .append(", length=").append(item.getLength())
                    .append(", classChanged=").append(item.isRoadClassChanged() ? "true" : "false")
                    .append(")\n");
        }
        return sb.toString();
    }

    /**
     * Fill a newly created Itinerary with instructions based on the found path.
     * Each segment of a path is usually described with one instruction.
     */
    public void fillAllInstructions() {
        Map<Integer, Pair<Double, Integer>> segmentLengths = path.getSegmentLengths();
        // Segment = edges between 2 decision points
        int segmentStart = 0;
        double segmentLength = 0.0;
        // Length from the start of the route
        double totalLength = 0.0;
        for (int i = 0; i < path.getEdges().size(); ++i) {
            Edge e1 = path.getEdges().get(i);
            double e1Length = e1.getLineString().getLength();
            Map<String, Object> e1Props = graph.getEdgeProperties(e1.getId());
            if (i == 0) {
                // Initial instruction = decision instruction without maneuver
                ItineraryItem itemStart = new ItineraryItem(ItineraryItem.ItineraryItemType.Decision, e1, i, 0);
                // Landmarks BY the user do not provide any information to the user about the heading
                // We need to tell the user to go TOWARDS or AWAY FROM something
                Map<Long, LandmarkPreposition> byLandmarks = e1.getDecisionInstruction().getLandmarks()
                        .stream()
                        .filter(landmark -> landmark.getPreposition().equals(LandmarkPreposition.AT))
                        .map(landmark -> Map.entry(landmark.getId(), landmark.getPreposition()))
                        .collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue()));
                DecisionInstruction decisionInstruction = (DecisionInstruction) e1.getDecisionInstruction().removePassedLandmarks(byLandmarks);
                itemStart.setInstruction(decisionInstruction);
                itemStart.setPriorDecision(1.0 / e1.getSource().getDegree());
                itemStart.setPosteriorDecision(1.0 / e1.getSource().getDegree());
                segmentLength = segmentLengths.get(i).getLeft();
                itemStart.setLength(segmentLength);
                itemStart.setEndIdx(i + segmentLengths.get(i).getRight());
                itemStart.setProperties(e1Props);
                this.itineraryList.add(itemStart);
            }
            // Confirmation instruction on the edge e1
            ConfirmationInstruction confirmationInstruction = e1.getConfirmationInstruction();
            ItineraryItem itemConfirmation = new ItineraryItem(ItineraryItem.ItineraryItemType.Confirmation, e1, i, totalLength + e1.getLineString().getLength() / 2);
            itemConfirmation.setInstruction(confirmationInstruction);
            itemConfirmation.setLength(e1Length);
            itemConfirmation.setProperties(e1Props);
            itemConfirmation.setEndIdx(i + 1);
            if (segmentLength > itemConfirmation.getDistanceCovered(SPEED)) this.itineraryList.add(itemConfirmation);
            // Approach and decision instructions with maneuver
            if (i + 1 < path.getEdges().size()) {
                Edge e2 = path.getEdges().get(i + 1);
                Map<String, Object> e2Props = graph.getEdgeProperties(e2.getId());
                // If there is no decision needed, then there is no need for a maneuver and a confirmation instruction is generated
                if (!graph.isDecisionPoint(e1, e2)) {
                    ConfirmationInstruction confirmationInstruction2 = new ConfirmationInstruction();
                    confirmationInstruction2.setLandmarks(e2.getDecisionInstruction().getLandmarks());
                    ItineraryItem itemConfirmation2 = new ItineraryItem(ItineraryItem.ItineraryItemType.Confirmation, e2, i + 1, totalLength + e1Length);
                    itemConfirmation2.setInstruction(confirmationInstruction2);
                    itemConfirmation2.setPriorDecision(e1.getPrior(e2.getId()));
                    itemConfirmation2.setProperties(e2Props);
                    // If the road changes its class, we can use this information
                    if (!graph.isSameClass(e1, e2)) itemConfirmation2.setRoadClassChanged();
                    if (segmentLength > itemConfirmation2.getDistanceCovered(SPEED))
                        this.itineraryList.add(itemConfirmation2);
                } else {
                    Maneuver maneuver = e1.getManeuver(e2.getId());
                    int angle = e1.getAngle(e2.getId());
                    // Approach instructions are added later
                    DecisionInstruction decisionInstruction = e2.getDecisionInstruction();
                    ItineraryItem itemDecision = new ItineraryItem(ItineraryItem.ItineraryItemType.Decision, e2, i + 1, totalLength + e1Length);
                    itemDecision.setInstruction(decisionInstruction);
                    itemDecision.setManeuver(maneuver);
                    itemDecision.setAngle(angle);
                    itemDecision.setPriorDecision(e1.getPrior(e2.getId()));
                    itemDecision.setProperties(e2Props);
                    if (!graph.isSameClass(e1, e2)) itemDecision.setRoadClassChanged();
                    itemDecision.addProperty("intersectionCount", i - segmentStart);
                    itemDecision.addProperty(JUNCTION_KEYSTRING, e1.getJunctionType());
                    segmentStart = i + 1;
                    segmentLength = segmentLengths.get(i + 1).getLeft();
                    itemDecision.setLength(segmentLength);
                    itemDecision.setEndIdx(segmentLengths.get(i + 1).getRight());
                    itemDecision.setElevation(path.getTotalElevation(i + 1, itemDecision.endIdx));
                    this.itineraryList.add(itemDecision);
                }
            } else {
                // Approach instruction on the edge e1 without maneuver (last edge)
                ApproachInstruction approachInstruction = e1.getApproachInstruction();
                ItineraryItem itemApproach = new ItineraryItem(ItineraryItem.ItineraryItemType.Approach, e1, i + 1, totalLength + e1Length);
                itemApproach.setInstruction(approachInstruction);
                itemApproach.setProperties(e1Props);
                itemApproach.addProperty("intersectionCount", i - segmentStart);
                itemApproach.addProperty(JUNCTION_KEYSTRING, e1.getJunctionType());
                this.itineraryList.add(itemApproach);
            }
            totalLength += e1Length;
        }
    }

    /**
     * Takes a list of planning items and plan the out such that no instruction overlaps.
     * Each item has a set starting distance, processing distance and utility.
     * The items are planned out from the highest utility.
     *
     * @param planningItems    Planning items
     * @param deadlineDistance Distance, where no instruction can reach
     * @return List of planned out itinerary items.
     */
    private List<ItineraryItem> scheduleItems(List<PlanningItem> planningItems, double deadlineDistance) {
        // Buffer between consecutive instructions
        double distanceBuffer = SPEED * (double) Properties.getSettings().get("INSTRUCTION_PAUSE");
        List<PlanningItem> scheduledItems = new LinkedList<>();
        // Sort items by utility (saliency) - descending
        planningItems.sort((o1, o2) -> -Double.compare(o1.utility, o2.utility));
        Set<String> passedLandmarks = new HashSet<>();
        // Add items from the highest utility and check for overlap
        for (PlanningItem item : planningItems) {
            Optional<PlanningItem> overlappedItem = scheduledItems.stream()
                    .filter(scheduledItem -> !(item.startDistance - distanceBuffer > scheduledItem.startDistance + scheduledItem.processingDistance ||
                            item.startDistance + item.processingDistance + distanceBuffer < scheduledItem.startDistance))
                    .findAny();
            Landmark landmark = item.itineraryItem.hasLandmark() ? item.itineraryItem.getInstruction().getLandmark() : null;
            boolean duplicate = (landmark == null && !item.itineraryItem.hasRoadInformation()) || (landmark != null && passedLandmarks.contains(landmark.toString()));
            if (!duplicate && item.startDistance + item.processingDistance < deadlineDistance - distanceBuffer && overlappedItem.isEmpty()) {
                scheduledItems.add(item);
                if (landmark != null) passedLandmarks.add(landmark.toString());
            }
        }
        // Sort items by the startDistance (ascending)
        scheduledItems.sort((o1, o2) -> Double.compare(o1.startDistance, o2.startDistance));
        return scheduledItems.stream().map(item -> item.itineraryItem).toList();
    }

    /**
     * Add approach instructions for a route leg into the items list.
     */
    private void addApproachPlanningItems(List<PlanningItem> items, ItineraryItem decisionItem, Path path, int startIdx, int endIdx, double segmentLength) {
        // Add approach instructions on complex intersections = Continue straight...
        for (int i = startIdx; i < endIdx - 1; ++i) {
            Edge e1 = path.getEdges().get(i);
            Edge e2 = path.getEdges().get(i + 1);
            if (graph.isComplexIntersection(e1)) {
                // Decision item in the intersection
                DecisionInstruction decisionInstruction = e2.getDecisionInstruction();
                ItineraryItem itemDecision = new ItineraryItem(ItineraryItem.ItineraryItemType.Decision, e2, i + 1, path.getTotalLength(0, i + 1));
                itemDecision.setInstruction(decisionInstruction);
                itemDecision.setManeuver(Maneuver.STRAIGHT_AHEAD);
                // Approach instruction referring to the complex intersection
                ItineraryItem intersectionConfirmation = new ItineraryItem(ItineraryItem.ItineraryItemType.Approach, e1, startIdx, path.getTotalLength(0, i + 1));
                intersectionConfirmation.length = 0;
                intersectionConfirmation.maneuver = Maneuver.STRAIGHT_AHEAD;
                intersectionConfirmation.setProperties(graph.getEdgeProperties(e1.getId()));
                intersectionConfirmation.addProperty(JUNCTION_KEYSTRING, e1.getJunctionType());
                intersectionConfirmation.addProperty("complex", true);
                intersectionConfirmation.nextItem = itemDecision;
                if (!graph.isSameClass(e1, e2)) intersectionConfirmation.setRoadClassChanged();
                intersectionConfirmation.roadClassTokensPriority = 1.0;
                items.add(new PlanningItem(intersectionConfirmation, path.getTotalLength(startIdx, i + 1), intersectionConfirmation.getDistanceCovered(SPEED), 1.0));
            }
        }
        // Add approach instructions based on distance
        List<Integer> distances = Stream.of(100, 200, 500, 1000).filter(d -> d < segmentLength).toList();
        ApproachInstruction approachInstruction = path.getEdges().get(decisionItem.startIdx - 1).getApproachInstruction();
        for (double distanceFromDecision : distances) {
            ItineraryItem approachItem = decisionItem.clone();
            approachItem.type = ItineraryItem.ItineraryItemType.Approach;
            approachItem.setInstruction(approachInstruction);
            approachItem.nextItem = decisionItem;
            approachItem.length = distanceFromDecision;
            approachItem.waypointDist = decisionItem.waypointDist - distanceFromDecision;
            if (approachItem.waypointDist > 0)
                items.add(new PlanningItem(approachItem, segmentLength - distanceFromDecision, approachItem.getDistanceCovered(SPEED), (segmentLength - distanceFromDecision) / segmentLength));
        }
    }

    /**
     * Apply a series of operations to transform the instructions
     * and integrate them into the itinerary.
     * The steps are:
     * 1. Remove redundant instructions
     * 2. Connect instructions that are too close to each other (Turn left and then turn right)
     * 3. Find and describe zigzag maneuvers
     * 4. Remove already passed landmarks
     * 5. Shift each instruction back, such that they are announced before passing the waypoint
     */
    public Itinerary reduce(Path path) {
        List<ItineraryItem> filteredItems = new LinkedList<>();
        // Remove redundant confirmation instructions
        // 1. Repeated landmark
        // 2. Approach instruction is more useful
        double distanceBuffer = SPEED * (double) Properties.getSettings().get("INSTRUCTION_PAUSE");
        List<PlanningItem> planningItems = new LinkedList<>();
        // Index of the starting decision instruction
        int lastIndex = 0;
        // Distance after the decision instruction, where another instruction can appear
        double minDistance = 0;
        for (ItineraryItem item : itineraryList) {
            if (item.getType().equals(ItineraryItem.ItineraryItemType.Decision)
                    || (item.getType().equals(ItineraryItem.ItineraryItemType.Approach))) {
                if (!planningItems.isEmpty()) {
                    double deadlineDistance = path.getTotalLength(lastIndex, item.startIdx) - item.getDistanceCovered(SPEED);
                    addApproachPlanningItems(planningItems, item, path, lastIndex, item.startIdx, path.getTotalLength(lastIndex, item.startIdx));
                    filteredItems.addAll(scheduleItems(planningItems, deadlineDistance));
                    planningItems.clear();
                }
                lastIndex = item.startIdx;
                minDistance = item.startIdx == 0 ? item.getDistanceCovered(SPEED) + distanceBuffer : distanceBuffer;
                filteredItems.add(item);
            } else {
                double prevDistance = path.getTotalLength(lastIndex, item.startIdx);
                double itemDistance = prevDistance
                        + (item.startIdx == item.endIdx - 1 ? path.getEdges().get(item.startIdx).getLineString().getLength() / 2 : 0);
                if (itemDistance > minDistance)
                    planningItems.add(new PlanningItem(item, itemDistance, item.getDistanceCovered(SPEED), item.getSaliency()));
            }
        }
        itineraryList = filteredItems;
        // Decision instruction with another one closer than INSTRUCTION_PAUSE seconds close will announce the next one too
        double AHEAD_THRESHOLD = (double) Properties.getSettings().get("INSTRUCTION_PAUSE") * SPEED;
        // Add information for two consecutive decision maneuvers (which are too close)
        for (int i = 0; i < itineraryList.size() - 1; ++i) {
            ItineraryItem i1 = itineraryList.get(i);
            ItineraryItem i2 = itineraryList.get(i + 1);
            if (i1.getType().equals(ItineraryItem.ItineraryItemType.Decision) && i2.getType().equals(ItineraryItem.ItineraryItemType.Decision)
                    && i1.getLength() < AHEAD_THRESHOLD) {
                i1.nextItem = i2;
            }
        }
        /*
        Iterate over triplets of instructions and find a shorter segment -> zigzag maneuver is simplified
        This preprocessing is inspired by the method in the article
        Towards landmark-based instructions for pedestrian navigation systems using OpenStreetMap by Anita Graser (chapter 3.1)
        This problem is also mentioned here: https://wazeopedia.waze.com/wiki/Global/Junction_Style_Guide#Offset_Roads
        */
        if (itineraryList.size() > 2) {
            // Segments shorter than THRESHOLD meters are modified
            double ZIGZAG_THRESHOLD = (double) Properties.getSettings().get("ZIGZAG_MAXLENGTH");
            List<ItineraryItem> zigzaggedItems = new LinkedList<>();
            for (ItineraryItem itineraryItem : itineraryList) {
                zigzaggedItems.add(itineraryItem);
                int size = zigzaggedItems.size();
                if (size < 3) continue;
                // Check last three items
                ItineraryItem item1 = zigzaggedItems.get(size - 3);
                ItineraryItem item2 = zigzaggedItems.get(size - 2);
                ItineraryItem item3 = zigzaggedItems.get(size - 1);
                if ((item1.type != ItineraryItem.ItineraryItemType.Decision || item1.getLength() > ZIGZAG_THRESHOLD)
                        && item2.type == ItineraryItem.ItineraryItemType.Decision
                        && item3.type == ItineraryItem.ItineraryItemType.Decision
                        && (item3.nextItem == null || item3.nextItem.type != ItineraryItem.ItineraryItemType.Decision)
                        && item2.length < ZIGZAG_THRESHOLD
                ) {
                    zigzaggedItems.remove(item3);
                    int angle1 = item2.angle;
                    int angle2 = item3.angle;
                    int newAngle = angle1 + angle2;
                    Maneuver newManeuver = Maneuver.angleToManeuver(newAngle);
                    // Item 2 = itineraryItem with the new maneuver (e.g. continue onto the sidewalk)
                    item2.maneuver = newManeuver;
                    item2.angle = newAngle;
                    item2.length = item2.length + item3.length;
                    item2.elevation = item2.elevation + item3.elevation;
                    item2.roadClassChanged = item2.roadClassChanged || item3.roadClassChanged;
                    item2.nextItem = item3;
                    item3.roadClassTokensPriority = item3.hasRoadInformation() ? 1.0 : item3.roadClassTokensPriority;
                    item3.maneuver = null;
                    item2.priorDecision = item2.priorDecision * item3.priorDecision;
                }
            }
            itineraryList = zigzaggedItems;
        }
        /*
        Remove referenced landmarks, that have already been passed
         */
        Map<Long, LandmarkPreposition> seenLandmarks = new HashMap<>();
        for (ItineraryItem item : itineraryList) {
            item.instruction = item.instruction.removePassedLandmarks(seenLandmarks);
            if (item.nextItem != null) {
                item.nextItem.instruction = item.nextItem.instruction.removePassedLandmarks(seenLandmarks);
            }
            if (!item.type.equals(ItineraryItem.ItineraryItemType.Approach)) {
                item.instruction.getLandmarks().forEach(lm -> {
                    long lmID = lm.getId();
                    LandmarkPreposition thisPreposition = lm.getPreposition();
                    seenLandmarks.put(lmID, thisPreposition);
                });
            }
        }
        // Shift waypoints back for each instruction, such that the instruction is announced before
        // the passed waypoint
        // Minimum distance that the instruction can be shifted back, such that the order of instructions is kept
        double minDist = 0.0;
        for (ItineraryItem item : itineraryList) {
            double waypointDist = item.getWaypointDist();
            item.setWaypoint(Utils.getPointAlongLine(path.getLineString(), waypointDist));
            item.setWaypointShifted(Utils.getPointAlongLine(path.getLineString(), Math.max(waypointDist - item.getDistanceCovered(SPEED), minDist)));
            minDist = waypointDist;
        }
        // Approach instructions refer to some decision instructions -> markers should reflect it
        for (ItineraryItem item : itineraryList) {
            if (item.type.equals(ItineraryItem.ItineraryItemType.Approach)) {
                if (item.nextItem != null) item.waypoint = item.nextItem.waypoint;
                else item.setWaypoint(path.getEdges().get(path.getEdges().size() - 1).getLineString().getEndPoint());
            }
        }
        return this;
    }

    private record PlanningItem(ItineraryItem itineraryItem, double startDistance, double processingDistance,
                                double utility) {
    }

    /**
     * Each itineraryItem in the itinerary is a set of instructions
     * that are used between two set of nodes (intersections)
     */
    public static class ItineraryItem implements Cloneable {
        ItineraryItemType type;
        Instruction instruction;
        Maneuver maneuver = null;
        Integer angle = null;
        transient Edge edge;
        int startIdx;
        int endIdx;
        long edgeId;
        double priorDecision;
        double posteriorDecision;
        double length = 0.0;
        double elevation = 0.0;
        // Change of type/class of the path segments
        boolean roadClassChanged = false;
        // Tokens to notify the user about the change of the road class
        List<InstructionToken> roadClassTokens;
        // Priority of the road class tokens
        // Bridges and crosswalks more important
        double roadClassTokensPriority = 0.0;
        // Next itineraryItem in case of close instruction after a decision instruction
        // Next itineraryItem is announced right after: "Turn left and the turn right" or "Turn left and continue over the railway crossing"
        ItineraryItem nextItem = null;
        // Detecting zebra crossing, sidewalks or bridges from edge properties
        Map<String, Object> properties;
        // Distance from the start of the route, where the instruction is announced
        double waypointDist = 0.0;
        // Waypoint where this instruction is announced - shifted slightly back
        // in order to complete the instruction before the waypoint
        JsonElement waypointShifted;
        // Waypoint where the instruction should already been announced and the user should
        // be mentally processing it
        JsonElement waypoint;
        // Distance covered while saying the instruction
        Double distanceCovered = null;

        public ItineraryItem(ItineraryItemType type, Edge edge, int startIdx, double waypointDist) {
            this.type = type;
            this.edge = edge;
            this.edgeId = edge.getId();
            this.startIdx = startIdx;
            this.endIdx = startIdx;
            this.roadClassTokens = new LinkedList<>();
            this.waypointDist = waypointDist;
        }

        public void setEndIdx(int endIdx) {
            this.endIdx = endIdx;
        }

        public void setManeuver(Maneuver maneuver) {
            this.maneuver = maneuver;
        }

        public void setAngle(Integer angle) {
            this.angle = angle;
        }

        public void setRoadClassChanged() {
            this.roadClassChanged = true;
            if (this.properties != null && !this.properties.isEmpty()) this.setRoadClassChangeTokens();
        }

        public void addProperty(String key, Object value) {
            if (this.properties == null) this.properties = new HashMap<>();
            this.properties.put(key, value);
        }

        public void setWaypoint(Point waypointGeometry) {
            this.waypoint = Utils.getGson().fromJson(Utils.geoToGeoJSON(waypointGeometry, true), JsonElement.class);
        }

        public void setWaypointShifted(Point waypointGeometry) {
            this.waypointShifted = Utils.getGson().fromJson(Utils.geoToGeoJSON(waypointGeometry, true), JsonElement.class);
        }

        public double getWaypointDist() {
            return waypointDist;
        }

        public double getLength() {
            return length;
        }

        public void setLength(double length) {
            this.length = length;
        }

        public double getElevation() {
            return elevation;
        }

        public void setElevation(double elevation) {
            this.elevation = elevation;
        }

        public double getPriorDecision() {
            return priorDecision;
        }

        public void setPriorDecision(double priorDecision) {
            this.priorDecision = priorDecision;
        }

        public double getPosteriorDecision() {
            return posteriorDecision;
        }

        public void setPosteriorDecision(double posteriorDecision) {
            this.posteriorDecision = posteriorDecision;
        }

        public Instruction getInstruction() {
            return instruction;
        }

        public void setInstruction(Instruction instruction) {
            this.instruction = instruction;
        }

        public Map<String, Object> getProperties() {
            return properties;
        }

        public void setProperties(Map<String, Object> properties) {
            this.properties = properties;
            // Elevation is a separate variable of ItineraryItem
            if (properties.containsKey(ELEVATION_KEYSTRING))
                this.elevation = Utils.doubleOrLongToDouble(properties.get(ELEVATION_KEYSTRING));
            this.properties.remove(ELEVATION_KEYSTRING);
            // Steepness in degrees, for this edge, not the whole segment
            this.properties.put("steepness", Math.atan(elevation / edge.getLineString().getLength()) * 180 / Math.PI);
        }

        public ItineraryItemType getType() {
            return type;
        }

        public void setType(ItineraryItemType type) {
            this.type = type;
        }

        public boolean isRoadClassChanged() {
            return roadClassChanged;
        }

        public boolean hasLandmark() {
            return instruction != null && !instruction.isEmpty();
        }

        public boolean usesLandmark() {
            switch (type) {
                case Approach:
                    return hasLandmark() && instruction.getLandmark().saliency > getJunctionSaliency() && length < (double) Properties.getSettings().get("APPROACH_DIST_DETAILS");
                case Decision, Confirmation:
                    return hasLandmark() && instruction.getLandmark().saliency > getRoadClassTokensSaliency();
                default:
                    throw new RuntimeException(String.format("Unknown instruction type %s", type));
            }
        }

        public List<InstructionToken> getRoadClassTokens() {
            return roadClassTokens;
        }

        public double getRoadClassTokensSaliency() {
            return roadClassTokensPriority;
        }

        public double getJunctionSaliency() {
            if (properties.containsKey(JUNCTION_KEYSTRING)) {
                return 0.5 - (((Graph.JunctionType) properties.get(JUNCTION_KEYSTRING)).ordinal() / 6.0);
            } else {
                return 0.0;
            }
        }

        public double getSaliency() {
            if (usesLandmark()) {
                return instruction.getLandmark().saliency;
            } else if (type.equals(ItineraryItemType.Approach) && properties.containsKey(JUNCTION_KEYSTRING)) {
                return getJunctionSaliency();
            } else if (hasRoadInformation()) {
                return getRoadClassTokensSaliency();
            }
            return 0.0;
        }

        public boolean hasRoadInformation() {
            return !this.roadClassTokens.isEmpty();
        }

        public ItineraryItem clone() {
            try {
                return (ItineraryItem) super.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
                return null;
            }
        }

        /**
         * Distance covered to announce the instruction
         *
         * @param speed Speed in km/h
         * @return Distance in meters
         */
        public double getDistanceCovered(double speed) {
            if (distanceCovered == null) {
                String text = this.toString();
                // Speed of speech is assumed to be 130 words/minute, source http://www.speechinminutes.com/
                double wpm = 130;
                int words = text.split(" ").length;
                // Time in seconds taken to say the text and a pause 10 seconds
                double timeTaken = words / (wpm / 60);
                distanceCovered = speed * timeTaken;
            }
            return distanceCovered;
        }

        public List<InstructionToken> toInstructionTokens() {
            List<InstructionToken> tokens = new LinkedList<>();
            switch (type) {
                case Approach -> {
                    ApproachInstruction approachInstruction = (ApproachInstruction) instruction;
                    boolean hasJunction = properties.containsKey(JUNCTION_KEYSTRING) && !properties.get(JUNCTION_KEYSTRING).equals(Graph.JunctionType.None);
                    boolean junctionIsClose = length <= (double) Properties.getSettings().get("APPROACH_DIST_DETAILS") && length > 0;
                    List<InstructionToken> approachTokens = new LinkedList<>();
                    if (usesLandmark() && junctionIsClose)
                        approachTokens.addAll(approachInstruction.toInstructionTokens());
                    else if (hasJunction && junctionIsClose) {
                        Graph.JunctionType junctionType = (Graph.JunctionType) properties.get(JUNCTION_KEYSTRING);
                        switch (junctionType) {
                            case Fourway -> approachTokens.add(new InstructionToken("at the four-way junction"));
                            case Y_shape -> approachTokens.add(new InstructionToken("at the Y junction"));
                            case T_shape -> approachTokens.add(new InstructionToken("at the T junction"));
                        }
                    } else if (length > 0) {
                        approachTokens.add(new InstructionToken("after"));
                        approachTokens.add(new InstructionToken("length", Utils.distanceFormat(length)));
                    } else {
                        // We could use counting (At the third intersection ... ),
                        // however these instructions are not used in favor of the distance information (After 500 meters ... )
                        approachTokens.add(new InstructionToken("at the next intersection"));
                    }
                    if (nextItem != null && nextItem.type == ItineraryItemType.Decision) {
                        tokens.addAll(approachTokens);
                        if (nextItem.maneuver != null)
                            tokens.add(new InstructionToken(MANEUVER_KEYSTRING, nextItem.maneuver.toInstruction()));
                        else tokens.add(new InstructionToken("you will turn"));
                    } else { // Approaching the end
                        tokens.add(new InstructionToken("The target is"));
                        if (junctionIsClose && (usesLandmark() || hasJunction)) tokens.addAll(approachTokens);
                        else tokens.add(new InstructionToken("in front of you"));
                    }
                }
                case Decision -> {
                    if (maneuver != null)
                        tokens.add(new InstructionToken(MANEUVER_KEYSTRING, maneuver.toInstruction()));
                    else {
                        tokens.add(new InstructionToken("Go"));
                        if (!usesLandmark() && !hasRoadInformation()) {
                            // Metric projection works well for lengths and areas, but not so well for orientation (slightly skewed)
                            LineString edgeLngLat = (LineString) Utils.projectBack(edge.getLineString());
                            Coordinate c1 = edgeLngLat.getCoordinateN(0);
                            Coordinate c2 = edgeLngLat.getCoordinateN(1);
                            // Angle in [-180,180]
                            // X and Y coordinates are swapped
                            double heading = Angle.toDegrees(Angle.angle(new Coordinate(c1.y, c1.x), new Coordinate(c2.y, c2.x)));
                            double step = 360. / 8;
                            if (Math.abs(heading) <= step / 2) {
                                tokens.add(new InstructionToken("north"));
                            } else if (Math.abs(heading) >= 180 - step / 2) {
                                tokens.add(new InstructionToken("south"));
                            } else if (heading < 0) {
                                if (-heading >= 1.5 * step && -heading <= 2.5 * step)
                                    tokens.add(new InstructionToken("west"));
                                else if (-heading < 1.5 * step) tokens.add(new InstructionToken("northwest"));
                                else if (-heading > 2.5 * step) tokens.add(new InstructionToken("southwest"));
                            } else {
                                if (heading >= 1.5 * step && -heading <= 2.5 * step)
                                    tokens.add(new InstructionToken("east"));
                                else if (heading < 1.5 * step) tokens.add(new InstructionToken("northeast"));
                                else if (heading > 2.5 * step) tokens.add(new InstructionToken("southeast"));
                            }
                        }
                    }
                    if (usesLandmark()) {
                        DecisionInstruction decisionInstruction = (DecisionInstruction) instruction;
                        tokens.addAll(decisionInstruction.toInstructionTokens());
                    } else if (hasRoadInformation()) {
                        tokens.addAll(roadClassTokens);
                    }
                    if (length > 0) {
                        if (nextItem != null && (nextItem.hasLandmark() || nextItem.hasRoadInformation())) {
                            if (nextItem.type == ItineraryItemType.Decision && nextItem.maneuver != null && nextItem.maneuver != Maneuver.STRAIGHT_AHEAD) { // Turn left and then turn right
                                tokens.add(new InstructionToken("and then"));
                                tokens.add(new InstructionToken(MANEUVER_KEYSTRING, nextItem.maneuver.toInstruction()));
                            } else { // Turn left and continue ...
                                if (nextItem.usesLandmark()) {
                                    // Do not announce duplicate landmarks
                                    if (!(this.hasLandmark() && nextItem.instruction.getLandmark().getId() == instruction.getLandmark().getId())) {
                                        tokens.add(new InstructionToken("and then continue"));
                                        tokens.addAll(nextItem.instruction.toInstructionTokens());
                                    }
                                } else if (nextItem.hasRoadInformation()) {
                                    tokens.add(new InstructionToken("and then continue"));
                                    tokens.addAll(nextItem.roadClassTokens);
                                }
                            }
                        } else if (length > 100) {
                            tokens.add(new InstructionToken("and then continue for"));
                            tokens.add(new InstructionToken("length", Utils.distanceFormat(length)));
                        }
                    }
                }
                case Confirmation -> {
                    if (usesLandmark()) {
                        ConfirmationInstruction confirmationInstruction = (ConfirmationInstruction) instruction;
                        tokens.addAll(confirmationInstruction.toInstructionTokens());
                    } else if (hasRoadInformation()) {
                        tokens.add(new InstructionToken("continue"));
                        tokens.addAll(roadClassTokens);
                    }
                }
                default -> {
                    throw new RuntimeException(String.format("Unknown type: %s", type));
                }
            }
            // Capitalize the first letter of the text in the first token
            if (!tokens.isEmpty()) tokens.get(0).capitalize();
            return tokens;
        }

        /**
         * Finds road class tokens, assuming the class of the road changes.
         */
        private void setRoadClassChangeTokens() {
            this.roadClassTokens = new LinkedList<>();
            String footway = (String) properties.get("footway");
            String highway = (String) properties.get("highway");
            String bridge = (String) properties.get("bridge");
            String incline = (String) properties.get("incline");
            double steepness = (double) properties.get("steepness");
            boolean up = (incline != null && incline.equals("up")) || (incline == null && steepness > 0);
            boolean down = (incline != null && incline.equals("down")) || (incline == null && steepness < 0);
            if (highway != null && highway.equals("steps")) {
                if (down) this.roadClassTokens.add(new InstructionToken("down the stairs"));
                if (up) this.roadClassTokens.add(new InstructionToken("up the stairs"));
                roadClassTokensPriority = 1.0;
            } else if (bridge != null) {
                this.roadClassTokens.add(new InstructionToken("over the"));
                String name = (String) properties.get("name");
                if (name != null && !name.equals("")) {
                    this.roadClassTokens.add(new InstructionToken("bridgename", name));
                    this.roadClassTokens.add(new InstructionToken("Bridge"));
                } else {
                    this.roadClassTokens.add(new InstructionToken("bridge"));
                }
                roadClassTokensPriority = 1.0;
            } else if (footway != null) {
                if ("crossing".equals(footway)) {
                    this.roadClassTokens.add(new InstructionToken("over the crosswalk"));
                    roadClassTokensPriority = 0.99;
                } else if ("sidewalk".equals(footway)) {
                    this.roadClassTokens.add(new InstructionToken("onto the sidewalk"));
                    roadClassTokensPriority = 0.5;
                }
            } else if (Math.abs(steepness) > (double) Properties.getSettings().get("STEEPNESS_THRESHOLD")) {
                if (up) {
                    this.roadClassTokens.add(new InstructionToken("uphill"));
                } else if (down) {
                    this.roadClassTokens.add(new InstructionToken("downhill"));
                }
                roadClassTokensPriority = 0.25;
            } else if (highway != null && roadClassChanged) {
                String streetName = (String) properties.get("name");
                boolean hasStreetName = streetName != null && !streetName.equals("");
                String streetArticle = hasStreetName ? streetName : "the";
                // Roads, that are types of road, and are formatted as [primary, ...] road
                List<String> roadClasses = List.of("trunk", "primary", "secondary", "tertiary", "unclassified", "residential", "service", "pedestrian");
                if (roadClasses.contains(highway)) {
                    if (hasStreetName)
                        this.roadClassTokens.add(new InstructionToken(String.format("onto %s Street", streetName)));
                    else this.roadClassTokens.add(new InstructionToken(String.format("onto the %s road", highway)));
                } else if (highway.equals("pedestrian")) {
                    this.roadClassTokens.add(new InstructionToken("onto the pedestrian way"));
                } else {
                    this.roadClassTokens.add(new InstructionToken(String.format("onto %s %s", streetArticle, highway.replace("_", " "))));
                }
                roadClassTokensPriority = 0.1;
            }
        }

        public String toString(List<InstructionToken> tokens) {
            List<String> strings = tokens.stream().map(token -> token.getText()).toList();
            return String.join(" ", strings);
        }

        @Override
        public String toString() {
            return this.toString(this.toInstructionTokens());
        }

        public enum ItineraryItemType {
            Approach,
            Decision,
            Confirmation
        }
    }
}
