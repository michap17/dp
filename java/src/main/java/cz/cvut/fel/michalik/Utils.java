package cz.cvut.fel.michalik;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import cz.cvut.fel.michalik.geojson.Feature;
import cz.cvut.fel.michalik.navigation.LandmarkPreposition;
import net.postgis.jdbc.PGgeometry;
import org.geotools.geojson.geom.GeometryJSON;
import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.CRS;
import org.locationtech.jts.algorithm.Angle;
import org.locationtech.jts.geom.*;
import org.locationtech.jts.io.geojson.GeoJsonWriter;
import org.locationtech.jts.linearref.LengthIndexedLine;
import org.locationtech.jts.operation.buffer.BufferParameters;
import org.locationtech.jts.operation.buffer.OffsetCurveBuilder;
import org.locationtech.jts.operation.distance.DistanceOp;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.referencing.operation.TransformException;

import java.io.IOException;
import java.io.Reader;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Static class with useful functions
 */
public class Utils {

    private static final int SRID = (int) Properties.getSettings().get("SRID");
    private static GeometryFactory geometryFactory = null;
    private static Gson gson = null;

    /**
     * Translates geometry into GeoJSON
     *
     * @param conn DB connection
     * @param geo  PGgeometry
     * @return GeoJSON string
     */
    public static String geoToString(Connection conn, PGgeometry geo) {
        try {
            PreparedStatement ps_geoToString = conn.prepareStatement("SELECT ST_AsGeoJSON(ST_Transform(?,4326))");
            ps_geoToString.setObject(1, geo);
            ResultSet res = ps_geoToString.executeQuery();
            res.next();
            return res.getString(1);
        } catch (SQLException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }

    /**
     * Translates geometry into GeoJSON
     *
     * @param geo Geometry
     * @return GeoJSON string
     */
    public static String geoToGeoJSON(Geometry geo, boolean projectBack) {
        if (projectBack) geo = projectBack(geo);
        GeoJsonWriter writer = new GeoJsonWriter((Integer) Properties.getSettings().get("DECIMAL_PRECISION"));
        writer.setEncodeCRS(false);
        return writer.write(geo);
    }

    /**
     * Translates GeoJSON back into Geometry with metric projection
     *
     * @param geoJSON GeoJSON string
     * @return Geometry
     */
    public static Geometry geoJSONToGeometry(String geoJSON) {
        Geometry geometry = null;
        try {
            geometry = projectMetric(new GeometryJSON().read(geoJSON));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return geometry;
    }

    public static List<Geometry> geoJSONCollectionToGeometries(Reader inputReader) {
        JsonReader reader = new JsonReader(inputReader);
        Iterator<Feature> featuresIterator = Utils.geoJSONCollectionToFeatures(new JsonReader(inputReader));
        List<Geometry> geometries = new LinkedList<>();
        featuresIterator.forEachRemaining(feature -> geometries.add(Utils.geoJSONToGeometry(feature.geometry.toString())));
        return geometries;
    }

    public static Iterator<Feature> geoJSONCollectionToFeatures(JsonReader reader) {
        Gson gson = new GsonBuilder().setObjectToNumberStrategy(ToNumberPolicy.LONG_OR_DOUBLE).create();
        return new Iterator<Feature>() {
            private boolean headerSkipped = false;

            private void skipHeader() {
                try {
                    reader.beginObject();
                    // Skip the "type":"FeatureCollection"
                    String nextName = reader.nextName();
                    if (nextName.equals("type")) {
                        reader.skipValue();
                        nextName = reader.nextName();
                    }
                    if (nextName.equals("features")) {
                        reader.beginArray();
                    }
                    headerSkipped = true;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public boolean hasNext() {
                if (!headerSkipped) skipHeader();
                try {
                    boolean hasNext = reader.hasNext() && !reader.peek().equals(JsonToken.END_ARRAY);
                    if (!hasNext) {
                        if (reader.peek().equals(JsonToken.END_ARRAY)) reader.endArray();
                        if (reader.peek().equals(JsonToken.END_OBJECT)) reader.endObject();
                        reader.close();
                    }
                    return hasNext;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return false;
            }

            @Override
            public Feature next() {
                if (!headerSkipped) skipHeader();
                return gson.fromJson(reader, new TypeToken<Feature>() {
                }.getType());
            }
        };
    }


    /**
     * Translates geometry back into WGS 84 CRS
     * using GeoTools
     *
     * @param geo Geometry
     * @return GeoJSON string
     */
    public static Geometry projectBack(Geometry geo) {
        Geometry geoCopy = geo.copy();
        geoCopy.apply(new ProjectBack());
        return geoCopy;
    }

    /**
     * Translates geometry into EPSG:3035 (metric)
     * using GeoTools
     *
     * @param geo Geometry
     * @return GeoJSON string
     */
    public static Geometry projectMetric(Geometry geo) {
        Geometry geoCopy = geo.copy();
        geoCopy.apply(new ProjectToMetric());
        return geoCopy;
    }

    public static PGgeometry getSideLine(Connection conn, PGgeometry road, double distance) {
        try {
            PreparedStatement ps_sideLine = conn.prepareStatement("SELECT ST_OffsetCurve(?, ?)");
            ps_sideLine.setObject(1, road);
            ps_sideLine.setDouble(2, distance);
            ResultSet rs = ps_sideLine.executeQuery();
            rs.next();
            return rs.getObject(1, PGgeometry.class);
        } catch (SQLException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, "Failed getting polygon to the side of a road", ex);
        }
        return null;
    }

    /**
     * Gets line that is distance meters away from road
     *
     * @param road     Road geometry
     * @param distance Distance (can be negative)
     * @return Returns Geometry (in most cases LineString) with given offset from road
     */
    public static Geometry getSideLine_jts(Geometry road, double distance) {
        BufferParameters bufParams = new BufferParameters();
        bufParams.setSingleSided(true);
        OffsetCurveBuilder builder = new OffsetCurveBuilder(road.getPrecisionModel(), bufParams);
        Coordinate[] offsetCoordinates = builder.getOffsetCurve(road.getCoordinates(), distance);
        GeometryFactory gf = Utils.getGeometryFactory(null);
        if (offsetCoordinates.length == 1) {
            return gf.createPoint(offsetCoordinates[0]);
        } else {
            return gf.createLineString(offsetCoordinates);
        }
    }

    /**
     * Returns GeometryFactory for creating geometries
     *
     * @param obj PGgeometry for getting SRID (if null, SRID is used)
     * @return GeometryFactory
     */
    public static GeometryFactory getGeometryFactory(PGgeometry obj) {
        if (geometryFactory != null) return geometryFactory;
        int srid = obj == null ? SRID : obj.getGeometry().srid;
        geometryFactory = new GeometryFactory(new PrecisionModel(), srid);
        return geometryFactory;
    }

    public static Gson getGson() {
        if (gson == null) {
            // Limit the number of decimal places, from https://stackoverflow.com/questions/52837686/gson-format-double-values-to-4-decimal-places
            GsonBuilder builder = new GsonBuilder();
            builder.registerTypeAdapter(Double.class, (JsonSerializer<Double>) (src, typeOfSrc, context) -> {
                DecimalFormat df = new DecimalFormat();
                df.setMaximumFractionDigits((Integer) Properties.getSettings().get("DECIMAL_PRECISION"));
                df.setRoundingMode(RoundingMode.HALF_UP);
                df.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.ENGLISH));
                try {
                    return new JsonPrimitive(df.parse(df.format(src)));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return new JsonPrimitive(0.0);
            });
            gson = builder.create();
        }
        return gson;
    }

    public static double angleWayRelative(LineString way, Coordinate target, Coordinate viewpoint) {
        Coordinate p1_reflection;
        double OFFSET = (double) Properties.getSettings().get("DISTANCE");
        if (viewpoint.equals(way.getCoordinateN(way.getNumPoints() - 1))) {
            LengthIndexedLine lengthIndexedLine = new LengthIndexedLine(way.reverse());
            // Previous point, it is a point at OFFSET distance or midpoint
            // This increases stability due to short sharp segments near the end of some ways
            Coordinate p1 = lengthIndexedLine.extractPoint(OFFSET);
            // reflected point: intersection + (intersection - point) = 2*intersection - point
            p1_reflection = new Coordinate(2 * viewpoint.x - p1.x, 2 * viewpoint.y - p1.y);
        } else {
            LengthIndexedLine lengthIndexedLine = new LengthIndexedLine(way);
            double index = lengthIndexedLine.indexOfAfter(viewpoint, lengthIndexedLine.getEndIndex());
            p1_reflection = lengthIndexedLine.extractPoint(index);
        }
        double angle = Angle.angleBetweenOriented(p1_reflection, viewpoint, target);
        return Angle.toDegrees(angle);
    }

    public static double angleBetweenWays(LineString lsA, LineString lsB, Coordinate intersection) {
        double OFFSET = (double) Properties.getSettings().get("SEG_LEN");
        Coordinate pointA = new LengthIndexedLine(lsA.reverse()).extractPoint(OFFSET);
        Coordinate pointA_reflection = new Coordinate(2 * intersection.x - pointA.x, 2 * intersection.y - pointA.y);
        Coordinate pointB = new LengthIndexedLine(lsB).extractPoint(OFFSET);
        double angle = Angle.angleBetweenOriented(pointA_reflection, intersection, pointB);
        return Angle.toDegrees(angle);
    }

    public static Map<LandmarkPreposition, Double> getPreferredPreposition(String location) {
        Map<String, Map<LandmarkPreposition, Double>> locations = Map.of(
                "approach", Map.of(LandmarkPreposition.AFTER, 0.1, LandmarkPreposition.AT, 0.6, LandmarkPreposition.BEFORE, 1.0),
                "decision", Map.of(LandmarkPreposition.AFTER, 0.1, LandmarkPreposition.AT, 1.0, LandmarkPreposition.BEFORE, 0.5),
                "confirmation", Map.of(LandmarkPreposition.AFTER, 0.5, LandmarkPreposition.AT, 1.0, LandmarkPreposition.BEFORE, 0.5)
        );
        String key = location.replace("source_", "").replace("target_", "");
        return locations.get(key);
    }

    /**
     * Transforms PGgeometry to JTS geometry
     *
     * @param obj PGgeometry object
     * @return Geometry
     */
    public static Geometry PGtoJTS(PGgeometry obj) {
        GeometryFactory factory = getGeometryFactory(obj);
        return PGtoJTS(obj.getGeometry(), factory);
    }

    private static Geometry PGtoJTS(net.postgis.jdbc.geometry.Geometry geometry, GeometryFactory factory) {
        Geometry geometryResult;
        switch (geometry.getType()) {
            case 0:
                System.out.println(geometry.getTypeString());
                geometryResult = factory.createLinearRing(PGtoCoordinates(geometry));
                break;
            case 1:
                geometryResult = factory.createPoint(PGtoCoordinates(geometry)[0]);
                break;
            case 2:
                geometryResult = factory.createLineString(PGtoCoordinates(geometry));
                break;
            case 3:
                net.postgis.jdbc.geometry.Polygon polygon = (net.postgis.jdbc.geometry.Polygon) geometry;
                LinearRing shell = null;
                LinearRing[] rings = new LinearRing[polygon.numRings() - 1];
                for (int i = 0; i < polygon.numRings(); i++) {
                    Coordinate[] coordinates = PGtoCoordinates(polygon.getRing(i));
                    if (i == 0) {
                        shell = factory.createLinearRing(coordinates);
                    } else {
                        rings[i - 1] = factory.createLinearRing(coordinates);
                    }
                }
                geometryResult = factory.createPolygon(shell, rings);
                break;
            case 4:
                net.postgis.jdbc.geometry.MultiPoint multiPoint = (net.postgis.jdbc.geometry.MultiPoint) geometry;
                Point[] points = new Point[multiPoint.numGeoms()];
                for (int i = 0; i < multiPoint.numGeoms(); i++) {
                    points[i] = (Point) PGtoJTS(multiPoint.getSubGeometry(i), factory);
                }
                geometryResult = factory.createMultiPoint(points);
                break;
            case 5:
                net.postgis.jdbc.geometry.MultiLineString multiLine = (net.postgis.jdbc.geometry.MultiLineString) geometry;
                LineString[] lines = new LineString[multiLine.numLines()];
                for (int i = 0; i < multiLine.numLines(); i++) {
                    lines[i] = (LineString) PGtoJTS(multiLine.getSubGeometry(i), factory);
                }
                geometryResult = factory.createMultiLineString(lines);
                break;
            case 6:
                net.postgis.jdbc.geometry.MultiPolygon multiPolygon = (net.postgis.jdbc.geometry.MultiPolygon) geometry;
                Polygon[] polygons = new Polygon[multiPolygon.numPolygons()];
                for (int i = 0; i < multiPolygon.numPolygons(); i++) {
                    polygons[i] = (Polygon) PGtoJTS(multiPolygon.getSubGeometry(i), factory);
                }
                geometryResult = factory.createMultiPolygon(polygons);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + geometry.getTypeString());
        }
        return geometryResult;
    }

    private static Coordinate[] PGtoCoordinates(net.postgis.jdbc.geometry.Geometry geometry) {
        Coordinate[] coords = new Coordinate[geometry.numPoints()];
        for (int i = 0; i < geometry.numPoints(); i++) {
            net.postgis.jdbc.geometry.Point point = geometry.getPoint(i);
            coords[i] = new Coordinate(point.x, point.y);
        }
        return coords;
    }

    /**
     * For given OSM ID, finds name of
     * that road
     *
     * @param osmId ID
     * @return Name of street
     */
    public static String getStreetName(Connection conn, long osmId) {
        try {
            PreparedStatement nextWays = conn.prepareStatement(
                    "SELECT name FROM planet_osm_line WHERE osm_id = ?");
            nextWays.setLong(1, osmId);
            ResultSet resultSet = nextWays.executeQuery();
            resultSet.next();
            return resultSet.getString("name");
        } catch (SQLException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static Point getPointAlongLine(LineString lineString, double dist) {
        LengthIndexedLine indexedLine = new LengthIndexedLine(lineString);
        return getGeometryFactory(null).createPoint(indexedLine.extractPoint(dist));
    }

    public static Point getMidPoint(LineString lineString) {
        return getPointAlongLine(lineString, lineString.getLength() / 2);
    }

    public static LineString shortestConnectingLine(Geometry a, Geometry b) {
        DistanceOp distanceOp = new DistanceOp(a, b);
        return getGeometryFactory(null).createLineString(distanceOp.nearestPoints());
    }

    public static double doubleOrLongToDouble(Object o) {
        double result = 0.0;
        if (o instanceof Long) result = (Long) o;
        else if (o instanceof Double) result = (Double) o;
        else throw new RuntimeException("Object is neither long nor double");
        return result;
    }

    public static String distanceFormat(double dist) {
        if (dist < 1000) return String.format("%d m", (int) Math.ceil(dist / 10.0) * 10);
        else return String.format("%.1f km", Math.ceil(dist / 100.0) / 10);
    }

    /**
     * Translates key-value pair to short description
     * for better instructions
     *
     * @param type POI key-value pair
     * @return String as a short description
     */
    public static String translatePoiType(String type, boolean hasName) {
        String key = type.split("=")[0];
        String value = type.split("=")[1];
        switch (key) {
            case "shop":
                return "shop";
            case "public_transport":
            case "railway":
                Map<String, String> valueMap = new HashMap<>();
                valueMap.putAll(Map.of("rail", "railway", "level_crossing", "railway crossing", "crossing", "railway crossing",
                        "abandoned", "abandoned railway", "tram", "tram rails", "subway", "subway", "tram_stop", "tram stop",
                        "subway_entrance", "subway entrance", "tram_level_crossing", "tram crossing", "tram_crossing", "tram crossing"));
                valueMap.putAll(Map.of("subway_station", "subway station"));
                String defaultValue = key.replace("_", " ") + " " + value.replace("_", " ");
                return valueMap.getOrDefault(value, defaultValue);
            case "office":
                return value.replace("_", " ") + " office";
            case "substation":
                return "power substation";
            case "power":
                if (value.equals("tower") || value.equals("pole")) {
                    return "power pole";
                }
                return "power " + value.replace("_", " ");
            case "building":
                Map<String, String> genericBuildings = new HashMap();
                genericBuildings.putAll(Map.of("yes", "building", "residential", "residential building",
                        "detached", "detached house", "industrial", "industrial building", "farm_auxiliary", "farm",
                        "commercial", "commercial building", "retail", "retail building", "service", "service building",
                        "civic", "civic building", "public", "public building"));
                genericBuildings.putAll(Map.of("collapsed", "collapsed building", "semi", "semi-detached house",
                        "damaged", "damaged building"));
                String defaultBuilding = value.replace("_", " ");
                // If building has a specific name, refer to it using just its name
                return hasName ? "" : genericBuildings.getOrDefault(value, defaultBuilding);
            case "recycling_type":
                return "recycling " + value.replace("_", " ");
            case "amenity":
                Map<String, String> amenities = new HashMap();
                amenities.putAll(Map.of("doctors", "doctor's office", "ice_cream", "ice cream place", "taxi", "taxi place",
                        "dentist", "dentist practice", "police", "police station", "bbq", "BBQ place",
                        "drinking_water", "drinking fountain", "recycling", "recycling site"));
                return amenities.getOrDefault(value, value.replace("_", " "));
            case "bridge":
                return "bridge";
            case "landuse":
                return value.replace("_", " ") + " area";
            case "information":
                List<String> informationList = List.of("guidepost", "map", "route_marker", "trail_blaze");
                return (informationList.contains(value) ? "" : "information ") + value.replace("_", " ");
            case "garden:type":
                return value.replace("_", " ") + " garden";
            case "vending":
                Map<String, String> vendingMachines = Map.of("parking_tickets", "parking ticket machine",
                        "public_transport_tickets", "public transport ticket machine", "excrement_bags", "excrement bags");
                String defaultVending = value.replace("_", "") + " vending machine";
                return vendingMachines.getOrDefault(value, defaultVending);
            case "addr:housenumber":
                return value;
            case "historic":
                if (value.equals("yes")) return "historical place";
                // else return default
            default:
                return value.replace("_", " ");
        }
    }

    private static class ProjectBack implements CoordinateFilter {
        private static MathTransform transform = null;

        @Override
        public void filter(Coordinate coordinate) {
            try {
                if (transform == null) {
                    CoordinateReferenceSystem source = CRS.decode("EPSG:" + SRID);
                    CoordinateReferenceSystem target = CRS.decode("EPSG:4326");
                    transform = CRS.findMathTransform(source, target);
                }
                /*
                 Swapping coordinates intentionally,
                 because coordinate transformation back to EPSG:4326 without it wouldn't work.
                 This won't affect any measurements.
                 The root cause is that PostGIS Geometry uses swapped X and Y coordinates
                 The order of coordinates does not really matter, you can read more 
                 for example here: https://macwright.com/lonlat/
                 */
                Coordinate projected = JTS.transform(new Coordinate(coordinate.y, coordinate.x), null, transform);
                coordinate.x = projected.y;
                coordinate.y = projected.x;
            } catch (FactoryException | TransformException e) {
                e.printStackTrace();
            }
        }
    }

    private static class ProjectToMetric implements CoordinateFilter {
        private static MathTransform transform = null;

        @Override
        public void filter(Coordinate coordinate) {
            try {
                if (transform == null) {
                    CoordinateReferenceSystem source = CRS.decode("EPSG:4326");
                    CoordinateReferenceSystem target = CRS.decode("EPSG:" + SRID);
                    transform = CRS.findMathTransform(source, target);
                }
                /*
                 Swapping coordinates intentionally, see comment above.
                 */
                Coordinate projected = JTS.transform(new Coordinate(coordinate.y, coordinate.x), null, transform);
                coordinate.x = projected.y;
                coordinate.y = projected.x;
            } catch (FactoryException | TransformException e) {
                e.printStackTrace();
            }
        }
    }
}
