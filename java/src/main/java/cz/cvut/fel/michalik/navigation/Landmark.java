package cz.cvut.fel.michalik.navigation;

import com.google.gson.JsonElement;
import cz.cvut.fel.michalik.MatchPair;
import cz.cvut.fel.michalik.Properties;
import cz.cvut.fel.michalik.Utils;
import cz.cvut.fel.michalik.navigation.instruction.InstructionToken;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;

/**
 * Landmark ~ point of interest ~ salient object
 * It is some object from OpenStreetMap and the salience is scored based on its properties.
 */
public class Landmark implements Serializable, Cloneable {
    String type;
    String name;
    double weight;
    // Area of this landmark, used in saliency function
    double area = 0;
    // Two distances are measured: Length of the closestLine and distance from the reference point
    // 1. Length of the closest line is used to determine the spatial relation
    // 2. Distance from reference point influences salience
    double dist;
    double distReference;
    int angle;
    // Number of landmarks that are of the same category, including this one
    int sameCategoryLandmarks = 1;
    // Layer = if the landmark is above, under the road or at the same layer
    int layer = 0;
    // If the landmark crosses the road (intersects with the interior of the road)
    boolean crosses = false;
    // Age in years of the landmark = when it was added or modified in the OSM
    int age;
    // Saliency is a score of this landmark
    Double saliency = null;
    private long id = 0;
    private transient Geometry geometry = null;
    private transient LineString closestLineGeometry = null;
    private JsonElement closestLine = null;

    public Landmark(String type, String name, double weight, double dist, int angle) {
        this.type = type;
        this.name = name;
        this.weight = weight;
        this.dist = dist;
        this.angle = angle;
    }

    public Landmark(long id, String name, MatchPair landmarkMatch, int layer, Geometry landmarkGeometry, Geometry road, LineString closestLineGeometry, double distReference, boolean crosses, int age) {
        this.closestLineGeometry = closestLineGeometry;
        this.angle = (int) Utils.angleWayRelative((LineString) road, closestLineGeometry.getEndPoint().getCoordinate(), closestLineGeometry.getStartPoint().getCoordinate());
        this.distReference = distReference;
        this.dist = closestLineGeometry.getLength();
        this.id = id;
        this.type = landmarkMatch.getMatch();
        this.name = name;
        this.weight = landmarkMatch.getWeight();
        this.geometry = landmarkGeometry;
        this.layer = layer;
        this.crosses = crosses;
        this.age = age;
        this.area = geometry.getArea();
        // Long line segments are penalized too, even though length is a unit of area
        if (this.area == 0.0) {
            this.area = geometry.getLength();
        }
        this.closestLine = Utils.getGson().fromJson(Utils.geoToGeoJSON(this.closestLineGeometry, true), JsonElement.class);
    }

    @Override
    protected Landmark clone() {
        try {
            return (Landmark) super.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(Landmark.class.getName()).log(Level.SEVERE, "Clone of Landmark failed");
            return this;
        }
    }

    /**
     * Weights for the saliency measure
     */
    private List<Double> getSaliencyWeights() {
        List<Double> weights = new ArrayList<>();
        //distFactor
        weights.add(1.0);
        //nameFactor
        weights.add(2.0);
        //nameLengthFactor
        weights.add(1.0);
        //relationPenalty
        weights.add(1.0);
        //areaPenalty
        weights.add(1.0);
        //uniqueness
        weights.add(1.0);
        //relativePositionFactor
        weights.add(1.0);
        //buildingBias
        weights.add(3.0);
        //crossingFactor
        weights.add(6.0);
        //ageFactor
        weights.add(1.0);
        //weightFactor
        weights.add(1.0);
        return weights;
    }

    /**
     * Returns saliency of the landmark. The saliency is influenced by the instruction type (DECISION, APPROACH, CONFIRMATION),
     * the relative position factors must be specified.
     * Example: Before is useful for decision, After is useful for approach, etc.
     *
     * @return saliency
     */
    public double getSaliency(Map<LandmarkPreposition, Double> relativePositionFactors) {
        if (saliency == null) {
            List<Double> weights = getSaliencyWeights();
            double distFactor = 1 - (distReference / (double) Properties.getSettings().get("DISTANCE"));
            // Objects with name are preferred
            double nameFactor = (name == null || name.equals("")) ? 0 : 1;
            // Objects with shorter names are even more preferred
            double nameLengthFactor = nameFactor == 1 ? 1.0 / name.length() : 0;
            // Relations (group of more objects) are not desirable, single objects are better
            double relationPenalty = id >= 0 ? 1 : 0;
            // Objects that are smaller should be more salient, points and lines = 1
            double areaPenalty = 1 / (area + 1);
            // Objects that are unique are preferred
            double uniqueness = 1.0 / sameCategoryLandmarks;
            // Objects that are passed before the decision point (intersection) are preferred
            double relativePositionFactor = relativePositionFactors.get(getPreposition());
            // Buildings with names are more salient
            double buildingBias = type.split("=")[0].equals("building") && name != null ? 1.0 : 0.0;
            // Landmark crossing the road are more salient
            double crossingFactor = crosses ? 1 : 0;
            // Newer landmarks are more likely to be still relevant
            // Older landmarks may be missing
            double ageFactor = 1.0 / Math.max(1, this.age - 3);
            List<Double> factors = List.of(distFactor, nameFactor, nameLengthFactor, relationPenalty, areaPenalty, uniqueness, relativePositionFactor, buildingBias, crossingFactor, ageFactor, weight);
            // Inspired by https://stackoverflow.com/questions/31963297/how-to-zip-two-java-lists
            saliency = IntStream.range(0, factors.size()).mapToDouble(i -> factors.get(i) * weights.get(i)).sum();
            // Normalize saliency into range [0,1]
            double maxSaliency = weights.stream().reduce(Double::sum).orElseGet(() -> 0.0);
            saliency /= maxSaliency;
        }
        return saliency;
    }

    public LandmarkPreposition getPreposition() {
        return dist < (double) Properties.getSettings().get("DISTANCE_EPSILON") ? LandmarkPreposition.AT : LandmarkPreposition.angleToPreposition(angle);
    }

    public double getDist() {
        return dist;
    }

    public double getDistReference() {
        return distReference;
    }

    public long getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public LineString getClosestLineGeometry() {
        return closestLineGeometry;
    }

    public boolean isTypeEmpty() {
        return type == null || type.equals("");
    }

    public double getArea() {
        return area;
    }

    public void setSameCategoryLandmarks(int sameCategoryLandmarks) {
        this.sameCategoryLandmarks = sameCategoryLandmarks;
    }

    public int getLayer() {
        return layer;
    }

    public int getAngle() {
        return angle;
    }

    public boolean isCrossing() {
        return crosses;
    }

    public boolean hasName() {
        return name != null && !name.equals("");
    }

    /**
     * Landmarks crossing the path are treated differently.
     * This function returns preposition based on the height relationship.
     */
    public List<InstructionToken> getCrossingTokens(String prefix) {
        List<InstructionToken> list = new LinkedList<>();
        if (getLayer() > 0 && crosses) {
            list.add(new InstructionToken(prefix + "-preposition", "under"));
        } else if (getLayer() <= 0 && crosses) {
            list.add(new InstructionToken(prefix + "-preposition", "over"));
        }
        list.addAll(toInstructionTokens(prefix));
        return list;
    }

    public List<InstructionToken> toInstructionTokens(String prefix) {
        LinkedList<InstructionToken> list = new LinkedList<>();
        if (!isTypeEmpty()) {
            String typeString = Utils.translatePoiType(type, hasName());
            if (!typeString.equals(""))
                list.add(new InstructionToken(prefix + "-landmark-type", Utils.translatePoiType(type, hasName()), id));
        }
        if (hasName()) {
            list.add(new InstructionToken(prefix + "-landmark-name", name, id));
        }
        if (!list.isEmpty()) {
            List<Character> vowels = List.of('a', 'e', 'i', 'o', 'u');
            if (hasName() || type.split("=")[0].equals("addr:housenumber") || list.get(0).getText().endsWith("s"))
                list.addFirst(new InstructionToken("the"));
            else if (vowels.contains(list.get(0).getText().charAt(0))) {
                list.addFirst(new InstructionToken("an"));
            } else {
                list.addFirst(new InstructionToken("a"));
            }
        }
        return list;
    }

    @Override
    public String toString() {
        List<String> strings = this.toInstructionTokens("").stream().map(token -> token.getText()).toList();
        return String.join(" ", strings);
    }
}
