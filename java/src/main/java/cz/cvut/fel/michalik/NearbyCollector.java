package cz.cvut.fel.michalik;

import cz.cvut.fel.michalik.feature.Query;
import cz.cvut.fel.michalik.geojson.Feature;
import cz.cvut.fel.michalik.navigation.Landmark;
import cz.cvut.fel.michalik.navigation.LandmarkPreposition;
import net.postgis.jdbc.PGgeometry;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.Point;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * For given road geometry collects all nearby objects and computes their
 * property values
 *
 * @author petr
 */
public class NearbyCollector implements AutoCloseable {

    // Maximum distance for the POI / landmark
    private static final double POI_DIST = (double) Properties.getSettings().get("DISTANCE");
    // List of possible points of interest
    private static final Query POI_MAP = Properties.getWeightedPOIs();
    private final Connection conn;
    // Distance in meters
    private final double DISTANCE;
    // Road object
    private final Road road;
    // ResultSets with nearby objects
    private Map<DbTable, ResultSet> resultMap = null;

    /**
     * Creates collector of objects near given road
     *
     * @param roadID Road OSM ID
     * @param dist   Distance in meters
     * @param table  Database table with referenced road ID
     */
    public NearbyCollector(long roadID, double dist, DbTable table) {
        this.conn = Connector.connect();
        this.road = new Road(conn, roadID, table);
        this.DISTANCE = dist;
    }


    /**
     * Creates collector of objects near given road
     *
     * @param road Road object
     * @param dist Distance in meters*
     */
    public NearbyCollector(Road road, double dist) {
        this.conn = Connector.connect();
        this.road = road;
        this.road.init();
        this.DISTANCE = dist;
    }

    public Road getRoad() {
        return road;
    }

    /**
     * Queries database and finds all nearby objects. Objects are searched in
     * given list of tables. If list of tables is null, searching in tables with
     * points, lines and polygons.
     *
     * @param tables Database tables
     * @return this
     */
    public NearbyCollector collectAll(List<DbTable> tables) {
        this.resultMap = new HashMap<>();
        if (tables == null) {
            tables = new LinkedList<>(Arrays.asList(DbTable.READ_POINT, DbTable.READ_LINE, DbTable.READ_POLY));
        }
        for (DbTable table : tables) {
            try {
                // Object has to be on the same level
                boolean hasLayer = road.getRoadData().layer() != null;
                // PreparedStatement is scrollable for reusability
                PreparedStatement ps = conn.prepareStatement("SELECT near.*, ST_Crosses(near.way, ST_Buffer((SELECT way FROM planet_osm_ways WHERE id = ?),-1)) AS crosses, "
                                + " EXTRACT(YEAR FROM age((near.tags->'osm_timestamp')::timestamp)) AS age "
                                + "FROM " + table.toString() + " AS near "
                                + "WHERE ST_DWithin(?, near.way,?) "
                                + (hasLayer ?
                                "AND (near.layer = ? OR near.tags->'level' = ?) " :
                                "AND (near.layer IS NULL OR near.layer NOT LIKE '-%') AND (near.tags->'level' IS NULL OR near.tags->'level' NOT LIKE '-%') ")
                                + "AND (tunnel IS NULL OR tunnel NOT IN ('yes','culvert','flooded'))",
                        ResultSet.TYPE_SCROLL_INSENSITIVE,
                        ResultSet.CONCUR_READ_ONLY);
                ps.setObject(1, road.getID());
                ps.setObject(2, road.getRoadDBGeometry());
                ps.setDouble(3, DISTANCE);
                if (hasLayer) {
                    ps.setString(4, road.getRoadData().layer());
                    ps.setString(5, road.getRoadData().layer());
                }
                ResultSet resultSet = ps.executeQuery();
                // After closing ResultSet, PreparedStatement closes too
                ps.closeOnCompletion();
                this.resultMap.put(table, resultSet);
            } catch (SQLException ex) {
                Logger.getLogger(NearbyCollector.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return this;
    }

    /**
     * Maps multiple categories with key value pairs to their respective values.
     * See countAll().
     *
     * @param queryMap Map of keywords to Query (key value pairs)
     * @return Map of counts
     */
    public Map<String, Object> mapToValues(Map<String, Query> queryMap) {
        Map<String, Object> res = new HashMap<>();
        queryMap.forEach((key, value) -> res.putAll(this.countAll(key, value)));
        return res;
    }

    /**
     * For a given single Query, computes values for its subcategories.
     * Example:
     * Keyword "green" is mapped to ("green_by"=0.5,"green_through"=0.1)
     *
     * @param key   Name of category (keyword)
     * @param query String of key and values.
     * @return Map of subcategories with values
     */
    private Map<String, Object> countAll(String key, Query query) {
        if (resultMap == null) {
            return null;
        }
        Map<String, Object> result = new HashMap<>();
        // Initialize values for each road segment
        road.initSegmentValues(0.0);
        // Matched objects as string
        StringBuilder matched = new StringBuilder();
        // List of obstacles
        Map<String, List<String>> obstaclesList = Properties.getObstacles();
        // Try every row (object)
        for (ResultSet rs : resultMap.values()) {
            try {
                while (rs.next()) {
                    // Filter out only relevant objects
                    MatchPair filtered = testWeightedKeys(rs, query.getWeightedKeyvals());
                    // Object is not in given category, but it might be an obstacle
                    if (filtered == null) {
                        String obstacle = testKeys(rs, obstaclesList);
                        if (obstacle != null) {
                            road.processObstacle(rs);
                        }
                    } else {
                        // String of matched objects is created for every category
                        // Add key-value pair to string, if not already present
                        int idx = matched.indexOf(filtered.getMatch());
                        if (idx == -1) {
                            matched.append(filtered.getMatch()).append(";");
                        }
                        // And process given object
                        road.processObject(rs, key, query, filtered.getWeight());
                    }
                }
                // Reset ResultSet
                rs.beforeFirst();
            } catch (SQLException ex) {
                Logger.getLogger(NearbyCollector.class.getName()).log(Level.SEVERE, "Failed processing object", ex);
            }
        }
        //Finalize road segments
        road.removeObstructed();
        result.put(key + "_by", road.calculateGoingBy());
        // Way can go through park, between roads or between lakes
        if (key.equals("green")) {
            result.put(key + "_through", road.calculateGoingThrough());
        } else if (key.equals("roads")) {
            // Roads are split into "through" and "between"
            result.put(key + "_between", (road.throughRoad ? 0 : 1) * road.calculateGoingThrough());
            result.put(key + "_through", (road.throughRoad ? 1 : 0) * road.calculateGoingThrough());
        } else {
            result.put(key + "_between", road.calculateGoingThrough());
        }
        result.put("_" + key + "_matched", matched.toString());
        return result;
    }

    /**
     * Tests every key-value with weights for given row.
     * If multiple weights are satisfied, returns highest
     * weight with found key value as String.
     * Values are returned as Pair object.
     *
     * @param rs              ResultSet with loaded row
     * @param weightedKeyVals List with weighted strings of key value pairs
     * @return Map with highest weight and String
     */
    private MatchPair testWeightedKeys(ResultSet rs, Map<Double, Map<String, List<String>>> weightedKeyVals) {
        for (Map.Entry<Double, Map<String, List<String>>> entry : weightedKeyVals.entrySet()) {
            double weight = entry.getKey();
            String match = testKeys(rs, entry.getValue());
            // Weights are sorted (see Properties.getWeightedPOIs()), first match = highest match
            if (match != null) return new MatchPair(weight, match);
        }
        return null;
    }

    /**
     * Tests if at least one key value pair is in given row.
     * Key is column in table and value is text in that column.
     * Keyvalue pair example: "key1=value1,value2,value3;key2=value4;key3=*"
     *
     * @param rs      ResultSet with loaded row
     * @param keyVals Map of key value pairs
     * @return String with matched string or null
     */
    private String testKeys(ResultSet rs, Map<String, List<String>> keyVals) {
        var contained = keyVals.entrySet().stream()
                .map(entry -> testKeyVal(rs, entry.getKey(), entry.getValue()))
                .filter(val -> val != null)
                .findFirst();
        return contained.orElse(null);
    }

    /**
     * For one row and one key tests if given key contains one of given values.
     * Value can be * - this means, that value can be anything.
     *
     * @param rs     ResultSet with loaded row
     * @param key    Key String
     * @param values List of String values
     * @return String with satisfied key value
     */
    private String testKeyVal(ResultSet rs, String key, List<String> values) {
        // Key is always in the column 'tags' as hstore (and can be also in a separate column, based on osm2pgsql schema)
        try {
            Map<String, String> tags_val = (Map<String, String>) rs.getObject("tags");
            // Test if present
            if (!tags_val.containsKey(key)) {
                return null;
            } else {
                String column_val = tags_val.get(key);
                // Column contains wildcard or some defined value
                if (values.contains("*") || values.contains(column_val)) {
                    return key + "=" + column_val;
                } else {
                    return null;
                }
            }
        } catch (SQLException ex2) {
            Logger.getLogger(NearbyCollector.class.getName()).log(Level.SEVERE, "Problem when testing key value pairs with ResultSet", ex2);
            return null;
        }
    }

    /**
     * Finds all landmarks and places them into the map based on their use (approach, decision, confirmation).
     */
    private void fillLandmarks(Map<String, List<Landmark>> landmarks) {
        // List of obstacle categories
        Map<String, List<String>> obstaclesList = Properties.getObstacles();
        // List of nearby obstacles with IDs
        List<ObstacleData> obstacles = new LinkedList<>();
        // Get viewpoints to measure distances and assign landmarks
        LineString roadGeometry = road.getRoadJTSGeometry();
        double roadLen = roadGeometry.getLength();
        int roadLayer = 0;
        try {
            roadLayer = road.getRoadData().layer() != null && !road.getRoadData().layer().equals("") ? Integer.parseInt(road.getRoadData().layer()) : 0;
        } catch (NumberFormatException ex) {
            //Logger.getLogger(NearbyCollector.class.getName()).log(Level.INFO, String.format("Layer of way %d is not a number", road.getOsmID()));
        }
        double DISTANCE_EPSILON = Math.min((double) Properties.getSettings().get("DISTANCE_EPSILON"), roadLen / 4);
        // Waypoint = intersection = decision point = point, where the maneuver is executed
        Point sourceWaypoint = roadGeometry.getStartPoint();
        Point targetWaypoint = roadGeometry.getEndPoint();
        // Reference point = point, from where a waypoint is approached = point where the instruction can be announced
        Point sourceRefPoint = roadLen > 4 * POI_DIST ? Utils.getPointAlongLine(roadGeometry, POI_DIST) : Utils.getPointAlongLine(roadGeometry, roadLen / 4);
        Point targetRefPoint = roadLen > 4 * POI_DIST ? Utils.getPointAlongLine(roadGeometry, roadLen - POI_DIST) : Utils.getPointAlongLine(roadGeometry, roadLen * 3 / 4);
        Point midPoint = Utils.getMidPoint(roadGeometry);
        for (ResultSet rs : resultMap.values()) {
            try {
                while (rs.next()) {
                    long candidateId = rs.getLong("osm_id");
                    Geometry candidateGeometry = Utils.PGtoJTS(rs.getObject("way", PGgeometry.class));
                    MatchPair poiMatch = testWeightedKeys(rs, POI_MAP.getWeightedKeyvals());
                    String name = rs.getString("name");
                    // Buildings must have names
                    if (poiMatch != null && (!poiMatch.getMatch().split("=")[0].equals("building") || name != null)) {
                        // Bridges may intersect the road either
                        //      in the middle -> bridge goes over or under
                        //      at the start or end of the road -> bridge is at the same level
                        // Crosses = intersects any interior point of the road
                        int layer = 0;
                        boolean crosses = candidateId == road.getOsmID() || rs.getBoolean("crosses");
                        try {
                            String candidateLayer = rs.getString("layer");
                            if (!rs.wasNull()) layer = Integer.parseInt(candidateLayer) - roadLayer;
                        } catch (NumberFormatException ignored) {
                        }
                        // Age in years of the landmark = when it was added or modified in the OSM
                        int age = rs.getInt("age");
                        LineString shortestLineSource = Utils.shortestConnectingLine(sourceWaypoint, candidateGeometry);
                        LineString shortestLineTarget = Utils.shortestConnectingLine(targetWaypoint, candidateGeometry);
                        LineString shortestLineMid = Utils.shortestConnectingLine(midPoint, candidateGeometry);
                        double distSourceRefPoint = sourceRefPoint.distance(candidateGeometry);
                        double distTargetRefPoint = targetRefPoint.distance(candidateGeometry);
                        double distMidpoint = shortestLineMid.getLength();
                        // Landmark should be somewhere further from the road, with 2 exceptions:
                        //      1. Road itself is a landmark (bridge)
                        //      2. Road crosses a point or a line (traffic lights, railway)
                        if ((distMidpoint > DISTANCE_EPSILON || candidateId == road.getOsmID() || crosses)
                                && distMidpoint < Math.min(POI_DIST, roadLen / 2)) {
                            landmarks.get("confirmation").add(new Landmark(candidateId, name, poiMatch, layer, candidateGeometry, roadGeometry, shortestLineMid, roadGeometry.distance(candidateGeometry), crosses, age));
                        }
                        if (distSourceRefPoint > DISTANCE_EPSILON && distSourceRefPoint < POI_DIST &&
                                (distSourceRefPoint < distTargetRefPoint || roadGeometry.getLength() < POI_DIST)) {
                            landmarks.get("source_approach").add(new Landmark(candidateId, name, poiMatch, layer, candidateGeometry, roadGeometry.reverse(), shortestLineSource, distSourceRefPoint, crosses, age));
                            landmarks.get("source_decision").add(new Landmark(candidateId, name, poiMatch, layer, candidateGeometry, roadGeometry, shortestLineSource, distSourceRefPoint, crosses, age));

                        }
                        if (distTargetRefPoint > DISTANCE_EPSILON && distTargetRefPoint < POI_DIST &&
                                (distTargetRefPoint < distSourceRefPoint || roadGeometry.getLength() < POI_DIST)) {
                            landmarks.get("target_approach").add(new Landmark(candidateId, name, poiMatch, layer, candidateGeometry, roadGeometry, shortestLineTarget, distTargetRefPoint, crosses, age));
                            landmarks.get("target_decision").add(new Landmark(candidateId, name, poiMatch, layer, candidateGeometry, roadGeometry.reverse(), shortestLineTarget, distTargetRefPoint, crosses, age));
                        }
                    }
                    // Test if matches an obstacle
                    String obstacleMatch = testKeys(rs, obstaclesList);
                    if (obstacleMatch != null && candidateId != road.getOsmID()) {
                        //Logger.getLogger(NearbyCollector.class.getName()).log(Level.INFO, String.format("Obstacle: %d %s", candidateId, obstacleMatch));
                        obstacles.add(new ObstacleData(candidateGeometry, candidateId, obstacleMatch));
                    }
                }
                // Reset ResultSet
                rs.beforeFirst();
            } catch (SQLException ex) {
                Logger.getLogger(NearbyCollector.class.getName()).log(Level.SEVERE, "Failed processing POIs", ex);
            }
            // Remove all obstructed landmarks
            for (ObstacleData obstacleData : obstacles) {
                double obstacleDist = obstacleData.geom.distance(roadGeometry);
                landmarks.replaceAll((loc, landmarkList) ->
                        landmarkList.stream().filter(landmark -> {
                            return  // Cases where an obstacle is not affecting the landmark
                                    // ID is the same - e.g. building can be both obstacle and landmark
                                    landmark.getId() == obstacleData.id
                                            // ID is the same as the ID of the road
                                            || road.getOsmID() == obstacleData.id
                                            // Or obstacle is further than the landmark
                                            || landmark.getDist() < obstacleDist
                                            // Or line of sight is not blocked by the obstacle
                                            || !landmark.getClosestLineGeometry().intersects(obstacleData.geom)
                                            // Or landmark is covered with the obstacle -> the landmark is part of the obstacle
                                            //      e.g. a building that contains a point with the information about the whole building
                                            // coveredBy() is not a cheap operation
                                            || (obstacleData.match.split("=")[0].equals("building") && landmark.getArea() == 0 && obstacleData.geom.contains(landmark.getGeometry()))
                                            // Or an obstacle is part of the landmark (relation)
                                            // Example: A fenced school area
                                            || landmark.getGeometry().contains(obstacleData.geom);
                        }).collect(Collectors.toList())
                );
            }
            // Calculate uniqueness by counting category (tag=value) occurrence and keeping track of IDs
            Map<String, Set<Long>> categoryCounts = new HashMap<>();
            landmarks.forEach((loc, landmarkList) -> {
                landmarkList.forEach(landmark -> {
                    String category = landmark.getType();
                    if (!categoryCounts.containsKey(category)) categoryCounts.put(category, new HashSet<>());
                    categoryCounts.get(category).add(landmark.getId());
                });
            });
            // Update uniqueness of each landmark
            landmarks.forEach((loc, landmarkList) -> {
                landmarkList.forEach(landmark -> {
                    String category = landmark.getType();
                    int count = categoryCounts.get(category).size();
                    landmark.setSameCategoryLandmarks(count);
                });
            });
        }
    }

    /**
     * Collects landmarks for this road for source, target and midpoint (for instructions).
     * Example: {'source_approach': Landmark1, 'source_decision': Landmark2, 'target_source': ..., 'confirmation': Landmark5}
     *
     * @return Map with landmarks
     */
    public Map<String, List<Landmark>> collectLandmarks(Map<Long, Feature> landmarkFeatures) {
        Map<String, List<Landmark>> landmarksAtLocations = new HashMap<>();
        landmarksAtLocations.put("source_approach", new LinkedList<>());
        landmarksAtLocations.put("source_decision", new LinkedList<>());
        landmarksAtLocations.put("target_approach", new LinkedList<>());
        landmarksAtLocations.put("target_decision", new LinkedList<>());
        landmarksAtLocations.put("confirmation", new LinkedList<>());
        fillLandmarks(landmarksAtLocations);
        Map<String, List<Landmark>> selectedLandmarksAtLocations = new HashMap<>();
        for (String location : landmarksAtLocations.keySet()) {
            Map<LandmarkPreposition, Double> relativePositionFactors = Utils.getPreferredPreposition(location);
            // Initialize and calculate saliency for each landmark
            landmarksAtLocations.get(location).forEach(landmark -> landmark.getSaliency(relativePositionFactors));
            // Collect and sort
            selectedLandmarksAtLocations.put(location, landmarksAtLocations.get(location)
                    // Sorting from the largest saliency to smallest
                    .stream().sorted((landmark1, landmark2) -> -Double.compare(landmark1.getSaliency(relativePositionFactors), landmark2.getSaliency(relativePositionFactors)))
                    .limit((Long) Properties.getSettings().get("LANDMARK_COUNT"))
                    .collect(Collectors.toList())
            );
        }
        // Fill map with landmark GeoJSONs, synchronization not needed for the ConcurrentHashMap
        for (List<Landmark> landmarks : selectedLandmarksAtLocations.values()) {
            for (Landmark l : landmarks) {
                if (!landmarkFeatures.containsKey(l.getId())) {
                    Map<String, Object> properties = new HashMap<>();
                    properties.put("type", l.getType());
                    properties.put("name", l.getName());
                    properties.put("weight", l.getWeight());
                    properties.put("area", l.getArea());
                    landmarkFeatures.put(l.getId(), new Feature(l.getId(), Utils.geoToGeoJSON(l.getGeometry(), true), properties));
                }
            }
        }
        return selectedLandmarksAtLocations;
    }

    /**
     * When this instance is not needed,
     * it closes all ResultSets and returns
     * connection to connection pool
     */
    public void close() {
        // Close every database ResultSet
        resultMap.forEach((dbTable, resultSet) -> {
            try {
                resultSet.close();
            } catch (SQLException e) {
                Logger.getLogger(NearbyCollector.class.getName()).log(Level.SEVERE, "Failed closing ResultSet", e);
            }
        });
        // And finally return connection to connection pool
        try {
            conn.close();
        } catch (SQLException e) {
            Logger.getLogger(NearbyCollector.class.getName()).log(Level.SEVERE, "Failed closing DB connection", e);
        }
        Progress.step();
    }

    record ObstacleData(Geometry geom, long id, String match) {
    }

}
