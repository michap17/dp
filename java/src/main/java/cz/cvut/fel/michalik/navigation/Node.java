package cz.cvut.fel.michalik.navigation;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Node of the graph. It contains all adjacent oriented edges.
 */
public class Node implements Serializable {
    private final long id;
    private final List<Edge> adjacentEdges;

    public Node(long id) {
        this.id = id;
        adjacentEdges = new LinkedList<>();
    }

    public long getId() {
        return id;
    }

    public List<Edge> getAdjacentEdges() {
        return adjacentEdges;
    }

    public void addAdjacentEdge(Edge e) {
        this.adjacentEdges.add(e);
    }

    public int getDegree() {
        return adjacentEdges.size();
    }
}
