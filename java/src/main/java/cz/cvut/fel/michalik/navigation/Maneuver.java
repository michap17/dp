package cz.cvut.fel.michalik.navigation;

import cz.cvut.fel.michalik.Properties;

/**
 * 4 basic directions: Front, Left, Right, Back
 * 4 more precise directions: [Bear/Sharp] [Left/Right]
 * Order of declaration matters, they are declared from straight to sharper directions
 */
public enum Maneuver {
    STRAIGHT_AHEAD("straight"),
    BEAR_LEFT("slight left"),
    BEAR_RIGHT("slight right"),
    LEFT("left"),
    RIGHT("right"),
    SHARP_LEFT("sharp left"),
    SHARP_RIGHT("sharp right"),
    BACK("back");

    private final String name;

    Maneuver(String name) {
        this.name = name;
    }

    public static Maneuver angleToManeuver(double angle) {
        // Angles are determined using:
        //      Towards landmark-based instructions for pedestrian navigation systems using OpenStreetMap by Anita Graser (Figure 5)
        //      https://wazeopedia.waze.com/wiki/Global/Junction_Style_Guide/Controlling_turn_instructions
        //      https://wazeopedia.waze.com/wiki/Global/How_Waze_determines_turn_/_keep_/_exit_maneuvers
        double ANGLE_THRESHOLD = (double) Properties.getSettings().get("ANGLE_THRESHOLD");
        double ANGLE_STRAIGHT = (double) Properties.getSettings().get("ANGLE_STRAIGHT");
        if (Math.abs(angle) < ANGLE_THRESHOLD) {
            if (Math.abs(angle) < ANGLE_STRAIGHT) return STRAIGHT_AHEAD;
            else if (Math.abs(angle) < 0) return BEAR_RIGHT;
            else return BEAR_LEFT;
        }
        if (Math.abs(angle) > 180 - ANGLE_THRESHOLD / 2) {
            return BACK;
        } else if (angle < 0) { // Right
            if (angle >= -(180 - ANGLE_THRESHOLD)) {
                return RIGHT;
            } else {
                return SHARP_RIGHT;
            }
        } else {
            if (angle <= 180 - ANGLE_THRESHOLD) {
                return LEFT;
            } else {
                return SHARP_LEFT;
            }
        }
    }

    /**
     * Maps ordinal values from 0..N to 0,1,1,2, ... +N/2
     */
    public int getIndex() {
        return (int) Math.round(Math.ceil(this.ordinal() / 2.0));
    }

    @Override
    public String toString() {
        return this.name;
    }

    public String toInstruction() {
        if (this == Maneuver.STRAIGHT_AHEAD) {
            return "continue straight";
        } else {
            return String.format("turn %s", this.toString());
        }
    }
}
