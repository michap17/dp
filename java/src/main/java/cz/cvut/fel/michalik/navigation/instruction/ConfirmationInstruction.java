package cz.cvut.fel.michalik.navigation.instruction;

import cz.cvut.fel.michalik.Properties;
import cz.cvut.fel.michalik.navigation.Landmark;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class ConfirmationInstruction extends Instruction implements Serializable {

    public ConfirmationInstruction() {
    }

    public ConfirmationInstruction(Instruction i) {
        this.landmarks = i.getLandmarks();
    }

    public List<InstructionToken> toInstructionTokens(Landmark landmark) {
        double ANGLE_THRESHOLD = (double) Properties.getSettings().get("ANGLE_STRAIGHT");
        double EPSILON = (double) Properties.getSettings().get("DISTANCE_EPSILON");
        List<InstructionToken> list = new LinkedList<>();
        if (!this.isEmpty()) {
            // Landmarks crossing the path are treated differently
            boolean crosses = landmark.isCrossing();
            if (crosses) {
                list.add(new InstructionToken("continue"));
                list.addAll(landmark.getCrossingTokens("confirmation"));
            } else if (landmark.getDist() > EPSILON) {
                // Based on the relative angle, use appropriate preposition
                if (Math.abs(landmark.getAngle()) <= ANGLE_THRESHOLD) {
                    list.addAll(landmark.toInstructionTokens("confirmation"));
                    list.add(new InstructionToken("is in front of you"));
                } else if (Math.abs(landmark.getAngle()) >= 180 - ANGLE_THRESHOLD) {
                    list.add(new InstructionToken("Continue away from"));
                    list.addAll(landmark.toInstructionTokens("confirmation"));
                } else {
                    list.addAll(landmark.toInstructionTokens("confirmation"));
                    list.add(new InstructionToken(landmark.getAngle() > 0 ? "is on your left" : "is on your right"));
                }
            } else {
                // When a landmark is closer than 1 meter but does not cross the road, it is most likely to interfere with the path
                list.addAll(landmark.toInstructionTokens("confirmation"));
                list.add(new InstructionToken("is close"));
            }
        }
        return list;
    }

    protected ConfirmationInstruction cloneInstruction(List<Landmark> landmarks) {
        ConfirmationInstruction instruction = new ConfirmationInstruction();
        instruction.landmarks = landmarks;
        return instruction;
    }

    @Override
    public List<InstructionToken> toInstructionTokens() {
        if (!this.isEmpty()) {
            return toInstructionTokens(getLandmark());
        } else {
            return new LinkedList<>();
        }
    }


}
