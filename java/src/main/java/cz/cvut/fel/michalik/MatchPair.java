package cz.cvut.fel.michalik;

/**
 * Class used in NearbyCollector.countAll().
 * Holds result of matched row by NearbyCollector.testWeightedKeys().
 */
public class MatchPair {
    public final double weight;
    public final String match;

    public MatchPair(double weight, String match) {
        this.weight = weight;
        this.match = match;
    }

    public double getWeight() {
        return weight;
    }

    public String getMatch() {
        return match;
    }
}
