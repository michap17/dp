package cz.cvut.fel.michalik;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializer;
import com.google.gson.stream.JsonWriter;
import cz.cvut.fel.michalik.geojson.Feature;
import cz.cvut.fel.michalik.navigation.*;
import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarBuilder;
import me.tongfei.progressbar.ProgressBarStyle;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;

import java.io.*;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

public class Navigation {
    public static void main(String[] args) throws InterruptedException, IOException {
        String filename = "graph.obj";
        File file = new File(filename);
        ProgressBarBuilder pbb = new ProgressBarBuilder()
                .setTaskName("Loading features from GeoJSON")
                .setStyle(ProgressBarStyle.ASCII)
                .setUnit("MiB", 1048576); // setting the progress bar to use MiB as the unit
        FileInputStream fileInputStream = new FileInputStream(file);
        Reader inputReader = new BufferedReader(new InputStreamReader(ProgressBar.wrap(fileInputStream, pbb), StandardCharsets.UTF_8));
        System.out.println(file.length());
        Thread.sleep(5000);
        //linestringTest();
        //gsonDoubleTest();
        //geojsonTest();
        /*
        //simpleGraphSaveLoadTest();
        Graph g = geoJsonLoadTest((String) Properties.getSettings().get("FILENAME"));
        /*if(file.exists()){
            g = loadGraph(filename);
        }else {
            g = geoJsonLoadTest((String) Properties.getSettings().get("FILENAME"));
            saveGraph(g, filename);
        }*/
        //pathTest(g);
        //routeTest(g);
        //strTreeTest(g);
    }

    private static void geojsonTest() {
        String jsonPolygon = "{\"type\":\"Polygon\",\"coordinates\":[[[14.3909356,50.103284141],[14.3910893,50.103199341],[14.3913991,50.103432841],[14.3915629,50.103343041],[14.3915445,50.103329241],[14.3914364,50.103248541],[14.3913928,50.103216041],[14.3914795,50.103167841],[14.391549,50.103129341],[14.3916091,50.103173941],[14.3917017,50.103242941],[14.3917206,50.103257041],[14.3918837,50.103167041],[14.3915712,50.102934041],[14.3917272,50.102847941],[14.3920415,50.103082441],[14.3922111,50.102988941],[14.3920524,50.102870541],[14.392198,50.102790241],[14.3923602,50.102911241],[14.3925354,50.102814641],[14.3922233,50.102581741],[14.3923749,50.102498141],[14.3926819,50.102727141],[14.3928426,50.102638541],[14.3926772,50.102515141],[14.3927574,50.102471041],[14.3928276,50.102432541],[14.3929972,50.102560041],[14.3931697,50.102465641],[14.3928615,50.102233941],[14.3929995,50.102158441],[14.3933459,50.102418841],[14.3933041,50.102441741],[14.3933775,50.102496941],[14.3933243,50.102526041],[14.393112,50.102642241],[14.3931281,50.102654341],[14.39318,50.102693341],[14.3932492,50.102745341],[14.3933575,50.102826841],[14.3935002,50.102934241],[14.393477,50.102946941],[14.3933559,50.103012641],[14.3933466,50.103017641],[14.3930917,50.102822141],[14.3929647,50.102724741],[14.3924689,50.102990741],[14.3928611,50.103291441],[14.3927024,50.103376541],[14.3923243,50.103086641],[14.3918388,50.103347141],[14.391968,50.103446141],[14.3922183,50.103636941],[14.3922032,50.103645041],[14.392081,50.103711941],[14.3920654,50.103720341],[14.3919264,50.103615341],[14.3918142,50.103530441],[14.391746,50.103478941],[14.391681,50.103430241],[14.3914071,50.103580241],[14.3913372,50.103528441],[14.3912925,50.103552241],[14.3912503,50.103520541],[14.391234,50.103508141],[14.3912245,50.103500941],[14.3909356,50.103284141]]]}";
        String jsonLine = "{\"type\":\"LineString\",\"coordinates\":[[14.3925559,50.102307541],[14.3927574,50.102471041]]}";
        Geometry polygon = Utils.geoJSONToGeometry(jsonPolygon);
        Geometry line = Utils.geoJSONToGeometry(jsonLine);
        System.out.println(polygon.crosses(line));
        System.out.println(line.crosses(polygon));
        System.out.println(Utils.geoToGeoJSON(polygon.intersection(line), true));
    }

    private static void gsonDoubleTest() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Double.class, (JsonSerializer<Double>) (src, typeOfSrc, context) -> {
            DecimalFormat df = new DecimalFormat();
            df.setMaximumFractionDigits((Integer) Properties.getSettings().get("DECIMAL_PRECISION"));
            df.setRoundingMode(RoundingMode.HALF_UP);
            df.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.ENGLISH));
            return new JsonPrimitive(Double.parseDouble(df.format(src)));
        });
        Gson gson = builder.create();
        Geometry geom = Utils.getGeometryFactory(null).createPoint(new Coordinate(1, 2));
        Map<String, Object> landmarks = Map.of("l1", new Landmark("a", "b", 1.23456789123, 123.9, -40));
        Map<String, Object> props = new HashMap<>();
        Road road = new Road(null);
        Feature feature = new Feature(Utils.geoToGeoJSON(geom, true), props, road, List.of(landmarks));
        System.out.println(gson.toJson(feature));
        try {
            JsonWriter writer = gson.newJsonWriter(new OutputStreamWriter(System.out));
            writer.beginArray();
            writer.jsonValue(gson.toJson(feature));
            writer.endArray();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void routeTest(Graph g) {
        Route route = Route.findRoute(1, 4000, g);
        System.out.println(route.getItinerary().toString());
        List<Geometry> geometries = route.getPath().getEdges().stream().map(edge -> edge.getLineString()).collect(Collectors.toList());
        try {
            g.saveAsGeoJSON(geometries, "path.geojson");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void linestringTest() {
        GeometryFactory gf = Utils.getGeometryFactory(null);
        Coordinate intersection = new Coordinate(1, 1);
        Coordinate[] points1 = new Coordinate[]{new Coordinate(-100, 0), new Coordinate(0, 0), intersection};
        Coordinate[] points2 = new Coordinate[]{intersection, new Coordinate(2, 0)};
        LineString lsA = gf.createLineString(points1);
        LineString lsB = gf.createLineString(points2);
        double angle = Utils.angleBetweenWays((LineString) lsA.reverse(), (LineString) lsB.reverse(), intersection);
        double angle2 = Utils.angleWayRelative(lsA, intersection, lsA.getEndPoint().getCoordinate());
        System.out.println(angle2);
        System.out.println(LandmarkPreposition.angleToPreposition(angle2));
        System.out.println(Utils.getMidPoint(lsA));
        System.out.println(Utils.getMidPoint(lsB));
        System.out.println(angle);
    }

    private static void pathTest(Graph g) {
        Node start = g.getNodeByID(1).orElseThrow();
        Node goal = g.getNodeByID(3918).orElseThrow();
        List<Edge> path = g.dijkstra(start, goal);
        System.out.printf("Path from %d to %d\n", start.getId(), goal.getId());
        if (path.isEmpty()) {
            System.out.println("No path found!");
            return;
        }
        for (int step = 0; step < path.size(); ++step) {
            Edge e1 = path.get(step);
            System.out.printf("DI: %s\n", e1.getDecisionInstruction());
            System.out.printf("CI: %s\n", e1.getConfirmationInstruction());
            if (step + 1 < path.size()) {
                Edge e2 = path.get(step + 1);
                System.out.printf("AI: %s \n", e1.getApproachInstruction());
                System.out.printf("MA: %s \n", e1.getManeuver(e2.getId()));
            }
        }
        List<Geometry> geometries = path.stream().map(edge -> edge.getLineString()).collect(Collectors.toList());
        try {
            g.saveAsGeoJSON(geometries, "path.geojson");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Graph geoJsonLoadTest(String filename) {
        Graph g = new Graph();
        try {
            g.loadFromGeoJSON(filename);
            System.out.printf("Graph with %d nodes and %d edges loaded\n", g.getNodes().size(), g.getEdges().size());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return g;
    }

    private static void simpleGraphSaveLoadTest() {
        // Basic graph
        Graph g = new Graph();
        g.addNode(new Node(1));
        g.addNode(new Node(2));
        g.addNode(new Node(3));
        g.addEdge(new Edge(10, 10, g.getNodeByID(1).get(), g.getNodeByID(2).get()));
        g.addEdge(new Edge(14, 14, g.getNodeByID(2).get(), g.getNodeByID(1).get()));
        g.addEdge(new Edge(11, 11, g.getNodeByID(2).get(), g.getNodeByID(3).get()));
        g.addEdge(new Edge(12, 12, g.getNodeByID(3).get(), g.getNodeByID(1).get()));
        String path = "graph.obj";
        saveGraph(g, path);
        System.out.println("Serialized data is saved");
        g = loadGraph(path);
        System.out.println("Serialized data is opened");
    }

    private static void saveGraph(Graph g, String path) {
        try {
            FileOutputStream fileOut = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(g);
            out.close();
            fileOut.close();
        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    private static Graph loadGraph(String path) {
        try {
            FileInputStream fileIn = new FileInputStream(path);
            ObjectInputStream inObj = new ObjectInputStream(fileIn);
            Graph g = (Graph) inObj.readObject();
            inObj.close();
            fileIn.close();
            return g;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
