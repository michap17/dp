package cz.cvut.fel.michalik;

import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import cz.cvut.fel.michalik.feature.Query;
import cz.cvut.fel.michalik.geojson.Feature;
import net.postgis.jdbc.PGgeometry;
import org.geotools.referencing.CRS;
import org.opengis.referencing.FactoryException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 * Enriches transport network graph from OpenStreetMap data.
 * Each way contains information about nearby greenery, water and
 * busy roads.
 */
public class Main {

    // Distance in meters
    private static final double DISTANCE = (double) Properties.getSettings().get("DISTANCE");
    // Output filename
    private static final String FILENAME = (String) Properties.getSettings().get("FILENAME");
    // Landmarks' geojsons
    private static final Map<Long, Feature> landmarkFeatures = new ConcurrentHashMap<>();
    // Stream with edges from graph
    private static Stream<RoadData> roadStream = null;
    // GSON object with Double rounding
    private static Gson gson;
    // Stream writer for output file
    private static JsonWriter writer = null;

    public static void main(String[] args) {
        // Shutdown hook saves files with all currently processed roads
        Runtime.getRuntime().addShutdownHook(new Thread(() -> Main.terminate()));
        Logger.getLogger(Main.class.getName()).setLevel(Level.ALL);
        try {
            Connection conn = Connector.connect();
            List<RoadData> roads = collectAllWays(conn);
            System.out.printf("%d roads found\n", roads.size());
            conn.close();
            // Initialize CRS dataFileCache
            CRS.decode("EPSG:4326");
            // Start progress bar
            Progress.init(roads.size());
            List<DbTable> tables = new LinkedList<>(Arrays.asList(DbTable.READ_POINT, DbTable.READ_LINE, DbTable.READ_POLY));
            Map<String, Query> properties = Properties.getProperties();
            gson = Utils.getGson();
            // Open new File stream
            writer = gson.newJsonWriter(new OutputStreamWriter(new FileOutputStream(FILENAME), StandardCharsets.UTF_8));
            // Write header of GeoJSON file
            writer.beginObject();
            writer.name("type").value("FeatureCollection");
            writer.name("features");
            writer.beginArray();
            // Start pool of threads
            roadStream = roads.parallelStream();
            // Process all ways
            roadStream
                    .map(roadData -> new Road(roadData))
                    .map(road -> new NearbyCollector(road, DISTANCE).collectAll(tables))
                    .map(nc -> new Feature(nc, properties, landmarkFeatures))
                    .forEach(Main::writeResult);
            /*roadStream.forEach(roadData -> {
                Road road = new Road(roadData.geom, roadData.wayID, roadData.osmID, roadData.sourceID, roadData.targetID, roadData.name, roadData.wayClass, roadData.footway);
                NearbyCollector nc = new NearbyCollector(road, DISTANCE).collectAll(tables);
                Feature feature = new Feature(nc, properties);
                Main.writeResult(feature);
            });*/
            // Exit (run shutdown hook)
            System.exit(0);
        } catch (SQLException | IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FactoryException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Failed getting EPSG:4326", ex);
        }
    }

    /**
     * Finds all ways in database
     *
     * @param conn DB connection
     * @return List of all ways as Road objects
     * @throws SQLException In case connection to DB fails
     */
    private static List<RoadData> collectAllWays(Connection conn) throws SQLException {
        List<RoadData> resultList = new LinkedList<>();
        Statement stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(
                "SELECT planet_osm_ways.*, planet_osm_line.tags->'footway' AS footway, planet_osm_line.highway, "
                        + " planet_osm_line.bridge AS bridge, planet_osm_line.tags->'incline' AS incline, "
                        + " ROUND((target.tags->'height')::numeric - (source.tags->'height')::numeric, 2) AS elevation, "
                        + " COALESCE(planet_osm_line.layer, planet_osm_line.tags->'level') AS layer FROM planet_osm_ways"
                        + " LEFT JOIN planet_osm_line ON planet_osm_ways.osm_id = planet_osm_line.osm_id "
                        + " LEFT JOIN planet_osm_point AS source ON (source.osm_id = osm_source_id) LEFT JOIN planet_osm_point AS target ON (target.osm_id = osm_target_id)"
        );
        while (rs.next()) {
            long wayID = rs.getLong("id");
            long osmID = rs.getLong("osm_id");
            PGgeometry geom = rs.getObject("way", PGgeometry.class);
            String name = rs.getString("osm_name");
            long sourceID = rs.getLong("source");
            long targetID = rs.getLong("target");
            int wayClass = rs.getInt("clazz");
            String footway = rs.getString("footway");
            String highway = rs.getString("highway");
            String bridge = rs.getString("bridge");
            String layer = rs.getString("layer");
            int oneway = rs.getInt("oneway");
            double elevation = rs.getDouble("elevation");
            String incline = rs.getString("incline");
            resultList.add(new RoadData(wayID, osmID, geom, name, sourceID, targetID, wayClass, highway, footway, bridge, layer, elevation, incline, oneway));
        }
        return resultList;
    }

    /**
     * Thread safely write processed way to output stream
     *
     * @param feature Processed road as Feature
     */
    private static void writeResult(Feature feature) {
        try {
            synchronized (writer) {
                if (writer != null) writer.jsonValue(gson.toJson(feature));
            }
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Could not write to file stream", ex);
        }
    }

    /**
     * Method used in shutdown hook.
     * Closes all connections and streams.
     */
    private static void terminate() {
        if (roadStream != null) {
            roadStream.close();
        }
        Progress.close();
        if (writer != null) {
            synchronized (writer) {
                try {
                    // Write landmark features
                    for (Feature feature : landmarkFeatures.values()) {
                        writeResult(feature);
                    }
                    writer.endArray();
                    writer.endObject();
                    writer.close();
                    writer = null;
                    System.out.printf("GeoJSON file saved to %s\n", new File(FILENAME).getCanonicalPath());
                } catch (IOException e) {
                    System.out.printf("Could not save result to %s\n", FILENAME);
                }
            }
        }
    }

    /**
     * Record for holding data about all the collected roads.
     */
    public record RoadData(long wayID, long osmID, PGgeometry geom, String name, long sourceID, long targetID,
                           int wayClass, String highway, String footway, String bridge, String layer, double elevation,
                           String incline, int oneway) {
    }

    ;
}
