package cz.cvut.fel.michalik.navigation;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.ToNumberPolicy;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import cz.cvut.fel.michalik.Properties;
import cz.cvut.fel.michalik.Utils;
import cz.cvut.fel.michalik.geojson.Feature;
import cz.cvut.fel.michalik.navigation.instruction.ApproachInstruction;
import cz.cvut.fel.michalik.navigation.instruction.ConfirmationInstruction;
import cz.cvut.fel.michalik.navigation.instruction.DecisionInstruction;
import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarBuilder;
import me.tongfei.progressbar.ProgressBarStyle;
import org.apache.commons.lang3.tuple.Pair;
import org.locationtech.jts.geom.*;
import org.locationtech.jts.index.strtree.STRtree;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Routable graph with nodes and edges.
 * It contains properties of each edge, geometries of landmarks, and STR tree.
 * The Graph is built in these steps:
 * 1. Load enriched GeoJSON
 * 2. Add properties of each edge
 * 3. Build graph
 * 4. Find the largest connected component
 * 5. Remove all nodes, edges and properties that do not belong to the largest component
 * 6. Compute maneuvers for each continuation of every edge
 * 7. Find the shape (if any) of each intersection
 * 8. Build STR tree
 */
public class Graph implements Serializable {
    private final Map<Long, Node> nodeMap;
    private final Map<Long, Edge> edgeMap;
    // Edge properties - green_by, water_by, ..., class, footway
    private final Map<Long, Map<String, Object>> properties;
    // Landmarks' GeoJSONs
    private final Map<Long, Feature> landmarkFeatures;
    // KD-Tree for closest node lookup
    private final STRtree strTree;
    // Largest graph component
    private Set<Long> graphComponent;
    // Bounding box of the dataset
    private Geometry boundingBox;

    public Graph() {
        this.nodeMap = new HashMap<>();
        this.edgeMap = new HashMap<>();
        this.properties = new HashMap<>();
        this.landmarkFeatures = new HashMap<>();
        this.graphComponent = new HashSet<>();
        this.strTree = new STRtree();
    }

    public List<Edge> getEdges() {
        return new LinkedList<>(edgeMap.values());
    }

    public List<Node> getNodes() {
        return new LinkedList<>(nodeMap.values());
    }

    public Map<Long, Map<String, Object>> getProperties() {
        return properties;
    }

    public void addNode(Node n) {
        nodeMap.put(n.getId(), n);
    }

    public void addEdge(Edge e) {
        edgeMap.put(e.getId(), e);
        e.getSource().addAdjacentEdge(e);
    }

    public Optional<Node> getNodeByID(long id) {
        if (this.nodeMap.containsKey(id)) {
            return Optional.of(nodeMap.get(id));
        } else {
            return Optional.empty();
        }
    }

    public Node getNodeByIDOrCreate(long id) {
        if (this.nodeMap.containsKey(id)) {
            return nodeMap.get(id);
        } else {
            Node n = new Node(id);
            addNode(n);
            return n;
        }
    }

    public Optional<Edge> getEdgeByID(long id) {
        if (this.edgeMap.containsKey(id)) {
            return Optional.of(edgeMap.get(id));
        } else {
            return Optional.empty();
        }
    }

    public Map<String, Object> getEdgeProperties(long edgeId) {
        return this.properties.get(Math.abs(edgeId)).entrySet().stream()
                .map(entry -> {
                    // Fix elevation and
                    // incline for opposite edges
                    if (entry.getKey().equals("elevation") && edgeId < 0) {
                        double elevationData = Utils.doubleOrLongToDouble(entry.getValue());
                        return Map.entry("elevation", elevationData * -1);
                    } else if (entry.getKey().equals("incline") && edgeId < 0) {
                        Object inclineObject = entry.getValue();
                        if (inclineObject instanceof String incline) {
                            if (incline.equals("up")) {
                                return Map.entry("incline", "down");
                            } else if (incline.equals("down")) {
                                return Map.entry("incline", "up");
                            }
                        }
                    }
                    return entry;
                }).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    /**
     * Returns true if Edge e1 and e2 are of the same class.
     * If they do not have the same class, the user should be informed about such a change.
     * We deem all types of footway (sidewalk, crosswalk) to be the same class.
     *
     * @return True if same class
     */
    public boolean isSameClass(Edge e1, Edge e2) {
        Map<String, Object> e1Props = this.getEdgeProperties(e1.getId());
        Map<String, Object> e2Props = this.getEdgeProperties(e2.getId());
        long e1Class = (long) e1Props.get("class");
        long e2Class = (long) e2Props.get("class");
        String e1Bridge = (String) e1Props.get("bridge");
        String e2Bridge = (String) e2Props.get("bridge");
        return (e1Class == e2Class
                && ((e1Bridge == null && e2Bridge == null) || (e1Bridge != null && e1Bridge.equals(e2Bridge)))
        );
    }

    /**
     * The user is coming along the Edge e1 and the route continues with Edge e2.
     * Returns true if the user needs a decision instruction (would most likely get lost without it).
     */
    public boolean isDecisionPoint(Edge e1, Edge e2) {
        boolean sameClass = isSameClass(e1, e2);
        boolean isChosen = e1.hasBestContinuation() && e1.getBestContinuation().getId() == e2.getId();
        return (!isChosen || !sameClass) && e1.getTarget().getAdjacentEdges().size() > 2;
    }

    /**
     * Returns if the target node of the Edge e1 is a complex intersection.
     * Complex intersections contain at least two continuations of third or higher class.
     */
    public boolean isComplexIntersection(Edge e1) {
        List<String> roadClasses = List.of("trunk", "primary", "secondary", "tertiary");
        long numRoads = e1.getTarget().getAdjacentEdges().stream()
                .map(edge -> (String) getEdgeProperties(edge.getId()).get("highway"))
                .filter(roadClass -> roadClass != null && roadClasses.contains(roadClass))
                .count();
        return numRoads > 2;
    }

    /**
     * Returns all segments connected to the same junction that are relatively straight,
     * coming from the edge e1
     *
     * @param e1
     * @return List
     */
    public List<BestContinuationCandidate> findBestContinuationCandidates(Edge e1) {
        double ANGLE_THRESHOLD = (double) Properties.getSettings().get("ANGLE_THRESHOLD");
        String e1Name = (String) getEdgeProperties(e1.getId()).get("name");
        return e1.getTarget().getAdjacentEdges()
                .stream().filter(e2 -> Math.abs(e1.getAngle(e2.getId())) < ANGLE_THRESHOLD)
                .map(e2 -> {
                    String e2Name = (String) getEdgeProperties(e2.getId()).get("name");
                    return new BestContinuationCandidate(e2, e1.getAngle(e2.getId()), (isSameClass(e1, e2) ? 1 : 0) + ((e1Name != null && e1Name.equals(e2Name)) ? 1 : 0));
                }).toList();
    }

    /**
     * This function the best continuation of the edge e1.
     * It is inspired by this article https://wazeopedia.waze.com/wiki/Global/How_Waze_determines_turn_/_keep_/_exit_maneuvers#Best_Continuation
     *
     * @param e1 Incoming edge
     * @return Edge e2 or null (if there is no best continuation)
     */
    public Edge getBestContinuation(Edge e1) {
        List<BestContinuationCandidate> straightContinuations = findBestContinuationCandidates(e1);
        // Case 1 - no straight segment
        if (straightContinuations.isEmpty()) return null;
        // Case 2 - only one straight
        if (straightContinuations.size() == 1) return straightContinuations.get(0).edge;
        // Check criteria for best continuation and determine a score for each segment
        // There is at least two edges -> at least two score should be present
        int bestScore = straightContinuations.stream()
                .map(candidate -> candidate.score)
                .max(Integer::compareTo).orElseThrow();
        long numBestScores = straightContinuations.stream().filter(candidate -> candidate.score == bestScore).count();
        // Case 3 - multiple continuations with the same quality -> no unique best continuation
        if (numBestScores > 1) {
            return null;
        } else {
            for (BestContinuationCandidate straightContinuation : straightContinuations) {
                int score = straightContinuation.score;
                if (score == bestScore) return straightContinuation.edge;
            }
        }
        return null;
    }

    /**
     * For each edge, add the best continuation and all maneuvers leading from the target node.
     */
    private void addManeuvers() {
        for (Edge edge1 : edgeMap.values()) {
            // Fill angles
            for (Edge edge2 : edge1.getTarget().getAdjacentEdges()) {
                // Based on relative angle, different maneuvers are used
                double angle = Utils.angleBetweenWays(edge1.getLineString(), edge2.getLineString(), edge1.getLineString().getEndPoint().getCoordinate());
                edge1.addAdjacentEdge(edge2.getId(), (int) angle);
            }
            // Then determine the best continuation
            edge1.setBestContinuation(getBestContinuation(edge1));
            // and resolve any straight ahead segments
            List<BestContinuationCandidate> straightAheads = new LinkedList<>(findBestContinuationCandidates(edge1));
            // Sort the angles from right to left (positive angles are on the left side)
            straightAheads.sort((c1, c2) -> Integer.compare(c1.angle, c2.angle));
            // Resolve based on the number of straight segments
            // Case 1: Straight ahead
            if (straightAheads.size() == 1) {
                edge1.setManeuver(straightAheads.get(0).edge.getId(), Maneuver.STRAIGHT_AHEAD);
            }
            // Case 2.1: Y-junction -> Bear left + Bear right
            // Case 2.2: Continuation -> Straight ahead + Bear left / right
            else if (straightAheads.size() == 2) {
                double ANGLE_THRESHOLD = (double) Properties.getSettings().get("ANGLE_STRAIGHT");
                int angle1 = straightAheads.get(0).angle;
                int angle2 = straightAheads.get(1).angle;
                if (Math.abs(angle1) < ANGLE_THRESHOLD && Math.abs(angle2) > ANGLE_THRESHOLD) {
                    edge1.setManeuver(straightAheads.get(0).edge.getId(), Maneuver.STRAIGHT_AHEAD);
                    edge1.setManeuver(straightAheads.get(1).edge.getId(), Maneuver.BEAR_LEFT);
                } else if (Math.abs(angle1) > ANGLE_THRESHOLD && Math.abs(angle2) < ANGLE_THRESHOLD) {
                    edge1.setManeuver(straightAheads.get(0).edge.getId(), Maneuver.BEAR_RIGHT);
                    edge1.setManeuver(straightAheads.get(1).edge.getId(), Maneuver.STRAIGHT_AHEAD);
                } else {
                    edge1.setManeuver(straightAheads.get(0).edge.getId(), Maneuver.BEAR_RIGHT);
                    edge1.setManeuver(straightAheads.get(1).edge.getId(), Maneuver.BEAR_LEFT);
                }
            }
            // Case 3: Bear left + Straight ahead + Bear right
            else if (straightAheads.size() == 3) {
                edge1.setManeuver(straightAheads.get(0).edge.getId(), Maneuver.BEAR_RIGHT);
                edge1.setManeuver(straightAheads.get(1).edge.getId(), Maneuver.STRAIGHT_AHEAD);
                edge1.setManeuver(straightAheads.get(2).edge.getId(), Maneuver.BEAR_LEFT);
            } else if (!straightAheads.isEmpty()) {  // More than 3, unresolvable, no maneuver added
                Logger.getLogger(Graph.class.getName()).log(Level.SEVERE,
                        String.format("Edge %d (%d -> %d) has %d outgoing best continuations candidates, no straight maneuvers added%n\t%s",
                                edge1.getId(), edge1.getSource().getId(), edge1.getTarget().getId(), straightAheads.size(),
                                String.join(",", straightAheads.stream().map(candidate -> String.valueOf(candidate.edge.getTarget().getId())).toList())));
            }
            // Resolve the rest and add priors
            Set<Long> straightAheadIds = new HashSet<>(straightAheads.stream().map(candidate -> candidate.edge.getId()).toList());
            for (Edge edge2 : edge1.getTarget().getAdjacentEdges()) {
                edge1.setPriors(getPriorProbabilities(edge1));
                if (straightAheadIds.contains(edge2.getId())) continue;
                edge1.setManeuver(edge2.getId(), Maneuver.angleToManeuver(edge1.getAngle(edge2.getId())));
            }
            // Detect other types of junctions, Y-junction already set
            List<Maneuver> maneuvers = edge1.getManeuvers().values().stream().toList();
            if (maneuvers.size() == 3 && maneuvers.contains(Maneuver.LEFT) && maneuvers.contains(Maneuver.RIGHT) && maneuvers.contains(Maneuver.BACK)) {
                edge1.setJunctionType(JunctionType.T_shape);
            } else if (maneuvers.size() == 4 && maneuvers.contains(Maneuver.LEFT) && maneuvers.contains(Maneuver.RIGHT) && maneuvers.contains(Maneuver.BACK) && maneuvers.contains(Maneuver.STRAIGHT_AHEAD)) {
                edge1.setJunctionType(JunctionType.Fourway);
            } else if (maneuvers.size() == 3 && maneuvers.contains(Maneuver.BEAR_LEFT) && maneuvers.contains(Maneuver.BEAR_RIGHT) && maneuvers.contains(Maneuver.BACK)) {
                edge1.setJunctionType(JunctionType.Y_shape);
            }
        }
    }

    /**
     * Get all landmark features that occur in the Itinerary
     */
    public Map<Long, Feature> getAllItineraryLandmarks(Itinerary itinerary) {
        Map<Long, Feature> itineraryLandmarks = new HashMap<>();
        for (Itinerary.ItineraryItem item : itinerary.getItineraryList()) {
            if (item.hasLandmark()) {
                for (Landmark l : item.getInstruction().getLandmarks()) {
                    itineraryLandmarks.put(l.getId(), landmarkFeatures.get(l.getId()));
                }
            }
            if (item.nextItem != null && item.nextItem.hasLandmark()) {
                for (Landmark l : item.nextItem.getInstruction().getLandmarks()) {
                    itineraryLandmarks.put(l.getId(), landmarkFeatures.get(l.getId()));
                }
            }
        }
        return itineraryLandmarks;
    }

    public Map<Long, Feature> getAllLandmarks() {
        return landmarkFeatures;
    }

    public String getDatasetBounds() {
        if (boundingBox == null) {
            Envelope envelope = new Envelope();
            for (Node node : nodeMap.values()) {
                Point p = node.getAdjacentEdges().get(0).getLineString().getStartPoint();
                envelope.expandToInclude(p.getCoordinate());
            }
            envelope.expandBy(100);
            this.boundingBox = Utils.getGeometryFactory(null).toGeometry(envelope);
            this.boundingBox = Utils.projectBack(boundingBox).getEnvelope();
        }
        return Utils.geoToGeoJSON(boundingBox, false);
    }

    /**
     * Fills the STR tree with all nodes of the graph.
     * Used later for finding the closest node.
     */
    private void fillSTRTree() {
        for (Node node : nodeMap.values()) {
            if (node.getAdjacentEdges().isEmpty()) continue;
            Point p = node.getAdjacentEdges().get(0).getLineString().getStartPoint();
            strTree.insert(p.getEnvelopeInternal(), Pair.of(node.getId(), p));
        }
    }

    /**
     * Get the prior distribution between all continuations leading from the target node of the Edge e1.*
     *
     * @return Map that for each consequent edge gives the prior probability of being selected by the user.
     */
    public Map<Long, Double> getPriorProbabilities(Edge e1) {
        Map<Long, Double> priors = new HashMap<>();
        for (Edge e2 : e1.getTarget().getAdjacentEdges()) {
            priors.put(e2.getId(), 0.0);
        }
        // Best continuation is chosen with 100% probability
        if (e1.hasBestContinuation()) {
            priors.put(e1.getBestContinuation().getId(), 1.0);
            return priors;
        }
        List<BestContinuationCandidate> straightContinuations = findBestContinuationCandidates(e1);
        // No straight continuation -> needs instruction for each adjacent edge
        if (straightContinuations.isEmpty()) {
            return priors;
        }
        // Each straight continuation with the best score has uniform probability
        int bestScore = straightContinuations.stream()
                .map(candidate -> candidate.score)
                .max(Integer::compareTo).orElseThrow();
        long numBestScores = straightContinuations.stream().filter(candidate -> candidate.score == bestScore).count();
        for (BestContinuationCandidate straightContinuation : straightContinuations) {
            int score = straightContinuation.score;
            if (score == bestScore) priors.put(straightContinuation.edge.getId(), 1.0 / numBestScores);
        }
        return priors;
    }

    /**
     * Find the largest component in undirected graph using BFS
     */
    private Set<Long> findLargestComponent() {
        Set<Long> largestComponent = new HashSet<>();
        Set<Long> currentComponent = new HashSet<>();
        Set<Long> closed = new HashSet<>();
        List<Node> queue = new LinkedList<>();
        for (Node node : this.nodeMap.values()) {
            long nodeId = node.getId();
            if (closed.contains(nodeId)) continue;
            queue.add(node);
            while (!queue.isEmpty()) {
                Node thisNode = queue.remove(0);
                long id = thisNode.getId();
                if (closed.contains(id)) continue;
                currentComponent.add(id);
                closed.add(id);
                for (Edge e : thisNode.getAdjacentEdges()) {
                    Node nextNode = e.getTarget();
                    if (!closed.contains(nextNode.getId())) {
                        queue.add(nextNode);
                    }
                }
            }
            // Component explored, check for size
            if (currentComponent.size() > largestComponent.size()) largestComponent = currentComponent;
            currentComponent = new HashSet<>();
        }
        return largestComponent;
    }

    /**
     * Reduces the nodeMap, edgeMap and properties to all nodes that are
     * in the graph component and edges that have both nodes in the component
     *
     * @param graphComponent Graph component
     */
    private void reduceGraphToComponent(Set<Long> graphComponent) {
        nodeMap.entrySet().removeIf(entry -> !graphComponent.contains(entry.getKey()));
        edgeMap.entrySet().removeIf(entry -> !graphComponent.contains(entry.getValue().getSource().getId()) || !graphComponent.contains(entry.getValue().getTarget().getId()));
        properties.entrySet().removeIf(entry -> !edgeMap.containsKey(entry.getKey()));
    }

    /**
     * Loads the enriched GeoJSON and builds a graph based on it.
     */
    public void loadFromGeoJSON(String filename) throws IOException {
        Gson gson = new GsonBuilder().setObjectToNumberStrategy(ToNumberPolicy.LONG_OR_DOUBLE).create();
        Logger.getLogger(Graph.class.getName()).log(Level.INFO, "Loading features from GeoJSON");
        File file = new File(filename);
        // ProgressBar wraps the FileInputStream and logs the progress
        // From: https://tongfei.me/progressbar/declarative-usage/
        ProgressBarBuilder pbb = new ProgressBarBuilder()
                .setTaskName("Loading GeoJSON")
                .setStyle(ProgressBarStyle.ASCII)
                .setInitialMax(file.length())
                .setUnit("MiB", 1048576); // setting the progress bar to use MiB as the unit
        Reader inputReader = new BufferedReader(new InputStreamReader(ProgressBar.wrap(new FileInputStream(filename), pbb), StandardCharsets.UTF_8));
        Iterator<Feature> featureIterator = Utils.geoJSONCollectionToFeatures(new JsonReader(inputReader));
        while (featureIterator.hasNext()) {
            Feature feature = featureIterator.next();
            // Landmark feature (GeoJSON + type, name, weight, area)
            if (feature.properties != null && !feature.properties.containsKey("landmarks")) {
                landmarkFeatures.put(feature.id, feature);
                continue;
            }
            assert feature.properties != null;
            long sourceID = (long) feature.properties.get("source");
            long targetID = (long) feature.properties.get("target");
            Node source = this.getNodeByIDOrCreate(sourceID);
            Node target = this.getNodeByIDOrCreate(targetID);
            LineString lineString = (LineString) Utils.geoJSONToGeometry(gson.toJson(feature.geometry));
            this.properties.put(feature.id,
                    feature.properties.entrySet().stream()
                            .filter(entry -> entry.getKey().matches(".*_by|.*_between|.*_through|class|footway|highway|name|oneway|bridge|elevation|incline|_osm"))
                            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));
            // Process landmarks for instructions
            LinkedTreeMap<String, Object> instructionsTree = (LinkedTreeMap<String, Object>) feature.properties.get("landmarks");
            Map<String, List<Landmark>> landmarks = gson.fromJson(gson.toJsonTree(instructionsTree), new TypeToken<Map<String, List<Landmark>>>() {
            }.getType());
            // Confirmation instructions
            ConfirmationInstruction confirmationInstruction = (ConfirmationInstruction) new ConfirmationInstruction().setLandmarks(landmarks.get("confirmation"));
            // Reverse edge needs to change the angles (from left or right and vice versa)
            List<Landmark> landmarksConfirmationReverse = landmarks.get("confirmation").stream().map(landmark -> landmark.clone()).toList();
            // Angles are rotated 180 degrees, which is done using the modulo that changes the interval from [0,360] to [-180,180]
            landmarksConfirmationReverse.forEach(landmark -> landmark.angle = ((landmark.angle + 360) % 360) - 180);
            ConfirmationInstruction confirmationInstructionReverse = (ConfirmationInstruction) new ConfirmationInstruction().setLandmarks(landmarksConfirmationReverse);
            // Add edges, even if they are oneway - user should avoid them, but can use them if necessary (cyclists gets off the bike)
            Edge e1 = new Edge(feature.id, (long) feature.properties.get("_osm"), source, target);
            e1.setLineString(lineString);
            this.addEdge(e1);
            // Source and target instructions
            e1.setApproachInstruction((ApproachInstruction) new ApproachInstruction().setLandmarks(landmarks.get("target_approach")));
            e1.setDecisionInstruction((DecisionInstruction) new DecisionInstruction().setLandmarks(landmarks.get("source_decision")));
            e1.setConfirmationInstruction(confirmationInstruction);
            Edge e2 = new Edge(-feature.id, (long) feature.properties.get("_osm"), target, source);
            e2.setLineString(lineString.reverse());
            this.addEdge(e2);
            // Source and target instructions
            e2.setApproachInstruction((ApproachInstruction) new ApproachInstruction().setLandmarks(landmarks.get("source_approach")));
            e2.setDecisionInstruction((DecisionInstruction) new DecisionInstruction().setLandmarks(landmarks.get("target_decision")));
            e2.setConfirmationInstruction(confirmationInstructionReverse);
        }
        inputReader.close();
        Logger.getLogger(Graph.class.getName()).log(Level.INFO, "Finding the largest component of the undirected graph");
        this.graphComponent = findLargestComponent();
        Logger.getLogger(Graph.class.getName()).log(Level.INFO, String.format("The largest component has %d nodes", this.graphComponent.size()));
        Logger.getLogger(Graph.class.getName()).log(Level.INFO, "Removing all nodes and edges that do not belong to the largest component");
        reduceGraphToComponent(this.graphComponent);
        Logger.getLogger(Graph.class.getName()).log(Level.INFO, "Adding maneuvers");
        addManeuvers();
        Logger.getLogger(Graph.class.getName()).log(Level.INFO, "Filling spatial tree");
        fillSTRTree();
    }

    public List<Edge> dijkstra(Node start, Node goal) {
        return dijkstra(start, goal, new HashMap<>());
    }

    /**
     * Dijkstra algorithm that searches for the cheapest path between start and end nodes.
     * Weights for the algorithm are passed as a map.*
     *
     * @return List of edges in the cheapest path
     */
    public List<Edge> dijkstra(Node start, Node goal, Map<String, Double> weights) {
        Set<Long> closed = new HashSet<>();
        Map<Long, Long> predecessorEdge = new HashMap<>();
        Map<Long, Double> edgeCost = new HashMap<>();
        PriorityQueue<PriorityQueueItem> queue = new PriorityQueue<>();
        start.getAdjacentEdges().forEach(e -> {
            queue.add(new PriorityQueueItem(0, e.getId()));
            edgeCost.put(e.getId(), 0.0);
        });
        while (!queue.isEmpty()) {
            PriorityQueueItem item = queue.remove();
            long id = item.getId();
            double cost = item.getCost();
            if (closed.contains(id)) continue;
            closed.add(id);
            Edge e = this.getEdgeByID(id).orElseThrow();
            if (goal.getId() == e.getTarget().getId()) {
                List<Edge> path = new LinkedList<>();
                path.add(e);
                // Find predecessors
                while (predecessorEdge.containsKey(e.getId())) {
                    e = this.getEdgeByID(predecessorEdge.get(e.getId())).orElseThrow();
                    path.add(e);
                }
                Collections.reverse(path);
                Logger.getLogger(Graph.class.getName()).log(Level.INFO, String.format("Found path in %d steps\t(cost=%f,params=%s)", closed.size(), cost, Utils.getGson().toJson(weights)));
                return path;
            }
            for (Edge nextEdge : e.getTarget().getAdjacentEdges()) {
                long nextId = nextEdge.getId();
                double lengthCost = weights.getOrDefault("distance", 0.1);
                double sidewalkCost = (getEdgeProperties(nextId).getOrDefault("footway", "").equals("sidewalk")
                        ? weights.getOrDefault("sidewalks", 0.05)
                        : 0);
                String highwayClass = (String) getEdgeProperties(nextId).getOrDefault("highway", "");
                double secondaryRoadCost = (highwayClass.equals("secondary")
                        ? weights.getOrDefault("secondaryRoad", 0.02)
                        : 0);
                double tertiaryRoadCost = (highwayClass.equals("tertiary")
                        ? weights.getOrDefault("tertiaryRoad", 0.01)
                        : 0);
                // Oneway streets can be traversed, but at a very high cost
                double onewayPenalty = (long) getEdgeProperties(nextId).get("oneway") * Math.signum(nextId) < 0
                        ? 100 * weights.getOrDefault("oneways", 0.5)
                        : 0.0;
                // Turn back maneuvers are also penalized, since it probably means a detour for the user
                double turnBackPenalty = e.getManeuver(nextId) == Maneuver.BACK ? 10000.0 : 0.0;
                double intersectionDifficulty = (1 - e.getPrior(nextId)) - (nextEdge.getDecisionInstruction().getLandmarks().isEmpty() ? 0.0 : 0.1);
                double nextCost = cost + intersectionDifficulty
                        + (lengthCost + sidewalkCost + secondaryRoadCost + tertiaryRoadCost) * nextEdge.getLineString().getLength()
                        + onewayPenalty + turnBackPenalty;
                if (!closed.contains(nextEdge.getId()) &&
                        (!predecessorEdge.containsKey(nextEdge.getId()) || nextCost < edgeCost.get(nextId))) {
                    queue.add(new PriorityQueueItem(nextCost, nextId));
                    edgeCost.put(nextId, nextCost);
                    predecessorEdge.put(nextId, id);
                }
            }
        }
        Logger.getLogger(Graph.class.getName()).log(Level.INFO, String.format("No path found, explored %d nodes", closed.size()));
        return new LinkedList<>();
    }

    /**
     * For the latitude and longitude, finds the closest node in the graph.
     *
     * @return Node ID
     */
    public long findClosestNodeID(double lng, double lat) {
        GeometryFactory geometryFactory = new GeometryFactory(new PrecisionModel(), 4326);
        Point query = (Point) Utils.projectMetric(geometryFactory.createPoint(new Coordinate(lng, lat)));
        Pair<Long, Geometry> result = (Pair<Long, Geometry>) strTree.nearestNeighbour(query.getEnvelopeInternal(), Pair.of(-1, query), (item1, item2) -> {
            Geometry i1 = ((Pair<Long, Geometry>) item1.getItem()).getRight();
            Geometry i2 = ((Pair<Long, Geometry>) item2.getItem()).getRight();
            return i1.distance(i2);
        });
        return result.getLeft();
    }

    public void saveAsGeoJSON(List<Geometry> geometries, String filename) throws IOException {
        JsonWriter writer = new JsonWriter(new OutputStreamWriter(new FileOutputStream(filename), StandardCharsets.UTF_8));
        // Write header of GeoJSON file
        writer.beginObject();
        writer.name("type").value("FeatureCollection");
        writer.name("features");
        writer.beginArray();
        for (Geometry geometry : geometries) {
            writer.jsonValue(Utils.getGson().toJson(new Feature(Utils.geoToGeoJSON(geometry, true), null)));
        }
        writer.endArray();
        writer.endObject();
        writer.close();
    }

    /**
     * Shapes of junctions
     */
    public enum JunctionType {
        T_shape,
        Y_shape,
        Fourway,
        None
    }

    /**
     * Each considered segment is checked, if it has the same class and same name as the incoming segment and score accordingly
     */
    private record BestContinuationCandidate(Edge edge, int angle, int score) {
    }

    /**
     * Item in the priority queue for the Dijkstra algorithm
     */
    private class PriorityQueueItem implements Comparable<PriorityQueueItem> {
        double cost;
        long id;

        public PriorityQueueItem(double cost, long id) {
            this.cost = cost;
            this.id = id;
        }

        public double getCost() {
            return cost;
        }

        public long getId() {
            return id;
        }

        @Override
        public int compareTo(PriorityQueueItem o) {
            double o_cost = o.getCost();
            // If cost < o_cost, return negative number (higher priority)
            return Double.compare(cost, o_cost);
        }
    }

}
