package cz.cvut.fel.michalik;

import cz.cvut.fel.michalik.feature.Query;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Static class for properties and settings.
 */
public class Properties {

    /**
     * Properties are weighted categories
     *
     * @return List of properties
     */
    public static Map<String, Query> getProperties() {
        Map<String, Query> properties = new HashMap<>();
        properties.put("roads",
                new Query(Map.of(1.0, Map.of("highway",
                                List.of("motorway", "trunk", "primary", "secondary",
                                        "motorway_link", "trunk_link", "primary_link", "secondary_link")),
                        0.01, Map.of("highway", List.of("tertiary", "tertiary_link"))),
                        50.0));
        properties.put("green",
                new Query(Map.of(1.0, Map.of(
                                "natural", List.of("wood"),
                                "landuse", List.of("forest"),
                                "leisure", List.of("park", "nature_reserve")),
                        0.99, Map.of(
                                "natural", List.of("tree_row", "scrub,heath"),
                                "landuse", List.of("meadow", "orchard", "vineyard"),
                                "leisure", List.of("playground"),
                                "garden:type", List.of("botanical", "castle", "community", "monastery")),
                        0.01, Map.of(
                                "natural", List.of("grassland"),
                                "landuse", List.of("farmland", "village_green", "grass", "greenfield"))),
                        50.0));
        properties.put("water",
                new Query(Map.of(1.0, Map.of(
                                "natural", List.of("water", "wetland", "glacier", "spring", "hot_spring", "geyser"),
                                "landuse", List.of("basin", "reservoir", "pond"),
                                "waterway", List.of("river", "riverbank", "tidal_channel", "canal", "dam", "weir", "waterfall")),
                        0.99, Map.of("waterway", List.of("stream", "ditch"))),
                        50.0));
        return properties;
    }

    /**
     * List of possible obstacles
     *
     * @return List
     */
    public static Map<String, List<String>> getObstacles() {
        return Map.of(
                "barrier", List.of("city_wall", "hedge", "retaining_wall", "wall"),
                "wall", List.of("*"),
                "highway", List.of("motorway", "trunk",
                        "motorway_link", "trunk_link"),
                "building", List.of("*"));
    }


    /**
     * List of possible points of interest
     *
     * @return List
     */
    public static Map<String, List<String>> getPOIs() {
        return Map.of(
                "shop", List.of("*"),
                "public_transport", List.of("*"),
                "amenity", List.of("bank", "fuel", "bus_station", "embassy", "hospital", "clinic", "school",
                        "restaurant", "pub", "cafe", "bar", "fast_food", "food_court", "theatre", "kindergarten",
                        "pharmacy", "library", "college", "university", "fountain", "fire_station", "police",
                        "shelter", "telephone", "playground", "grave_yard"),
                "historic", List.of("*")
        );
    }

    /**
     * List of possible points of interest weighted by category
     * and sorted by the weights from highest to lowest
     *
     * @return List
     */
    public static Query getWeightedPOIs() {
        TreeMap<Double, Map<String, List<String>>> pois = new TreeMap<>();
        pois.putAll(Map.of(
                1.0, Map.of("amenity", List.of("place_of_worship"),
                        "building", List.of("cathedral", "chapel", "church",
                                "mosque", "synagogue", "temple")),
                0.91, Map.of("amenity", List.of("fountain", "fuel")),
                0.9, Map.of("amenity", List.of("police", "fire_station")),
                0.85, Map.of("amenity", List.of("cinema", "theatre")),
                0.84, Map.of("tourism", List.of("hotel"),
                        "amenity", List.of("restaurant", "pub", "cafe", "bar", "post_office", "fast_food")),
                0.83, Map.of("railway", List.of("subway_entrance", "subway_station", "tram_stop", "station",
                                "level_crossing", "tram_level_crossing", "tram_crossing", "crossing"),
                        "bridge", List.of("*"),
                        "man_made", List.of("bridge")),
                0.81, Map.of("shop", List.of("supermarket")),
                0.80, Map.of("historic", List.of("memorial", "attraction", "monument", "statue",
                        "wayside_cross", "ruins", "yes", "wayside_shrine", "building", "boundary_stone", "castle",
                        "tomb", "citywalls", "heritage", "mine_shaft", "manor", "mine", "church", "milestone",
                        "fort", "city_gate", "house", "stone", "aircraft", "cannon", "farm", "bunker", "tower",
                        "monastery")),
                0.75, Map.of("amenity", List.of("school", "university", "pharmacy", "hospital", "clinic", "library", "college"),
                        "building", List.of("school", "university"))
        ));
        pois.putAll(Map.of(
                0.70, Map.of("leisure", List.of("park", "playground", "pitch", "stadium", "sports_centre")),
                0.65, Map.of("shop", List.of("bakery", "chemist", "jewelry", "kiosk")),
                0.45, Map.of("amenity", List.of("embassy", "bank", "courthouse", "town_hall"),
                        "diplomatic", List.of("*"),
                        "office", List.of("*"),
                        "tourism", List.of("museum", "artwork")),
                0.42, Map.of("shop", List.of("*")),
                0.3, Map.of("public_transport", List.of("*"),
                        "amenity", List.of("bicycle_rental", "bus_station"),
                        "substation", List.of("minor_distribution", "distribution", "transmission",
                                "transition", "generation", "converter", "measurement"),
                        "power", List.of("tower", "pole", "generator", "substation", "transformer", "switch", "plant"),
                        "man_made", List.of("antenna", "tower", "pier", "storage_tank", "silo", "works", "water_tower", "chimney")),
                // Elements with amenity=vending_machine or amenity=recycling should have these additional tags
                // and they are further distinguished by these values
                0.25, Map.of("vending", List.of("*"),
                        "recycling_type", List.of("*")),
                0.2, Map.of("amenity", List.of("*"),
                        "club", List.of("*"),
                        "highway", List.of("traffic_signals", "bus_stop", "motorway"),
                        "crossing", List.of("traffic_signals"),
                        "landuse", List.of("cemetery", "recreation_ground", "garages", "military", "industrial", "religious"),
                        "garden:type", List.of("botanical", "castle", "community", "monastery"),
                        "water", List.of("*"),
                        "waterway", List.of("river", "riverbank", "tidal_channel", "canal", "dam", "weir", "waterfall", "stream", "ditch"),
                        "information", List.of("guidepost", "board", "map", "route_marker", "office", "trail_blaze", "terminal")),
                0.18, Map.of("natural", List.of("water", "wetland", "glacier", "spring", "hot_spring", "geyser"),
                        "landuse", List.of("basin", "reservoir", "pond")),
                0.1, Map.of("building", List.of("*"),
                        "barrier", List.of("gate", "wall", "hedge", "bollard", "city_wall", "retaining_wall",
                                "cycle_barrier", "lift_gate", "turnstile"),
                        "railway", List.of("rail", "tram", "subway"))
        ));
        return new Query(pois.descendingMap(), 50.0);
    }

    /**
     * Application settings and configuration
     *
     * @return Map with settings
     */
    public static Map<String, Object> getSettings() {
        Map<String, Object> settings = new HashMap<>();
        settings.putAll(Map.of(
                "DB_URL", "jdbc:postgresql://localhost/prague",
                "DB_NAME", "petr",
                "DB_PASS", "post",
                "DB_CONNS", 32,
                "DISTANCE", 50.0,
                "OFFSET", 2.0,
                "SEG_LEN", 15.0,
                "SRID", 3035,
                // Decimal places: https://en.wikipedia.org/wiki/Decimal_degrees#Precision
                "DECIMAL_PRECISION", 6,
                "FILENAME", "export.geojson"));
        settings.putAll(Map.of(
                "LANDMARK_COUNT", 3L,
                // Segments shorter than this (in meters) are joined with the next instructions
                // Used in Itinerary.reduce()
                "ZIGZAG_MAXLENGTH", 10.0,
                // Defines the tolerance of what is considered a straight continuation
                // 0 = only the 'straightest' maneuver (eg. straight ahead)
                // 1 = the 'straightest' segment + second straightest (eg. straight ahead + slight left/right)
                "STRAIGHT_STEP", 1,
                // Distance smaller than epsilon is considered to be 0
                "DISTANCE_EPSILON", 1.0,
                // Angle that determines, which segments are considered to be straight (and best) continuations
                "ANGLE_THRESHOLD", 60.0,
                // Angle that determines, which segments are considered to be completely straight, regardless of being the best continuation
                "ANGLE_STRAIGHT", 15.0,
                // Time in seconds between each instruction
                "INSTRUCTION_PAUSE", 5.0,
                // Distance, where the approach instructions will contain more details (shape of the intersection, landmark)
                "APPROACH_DIST_DETAILS", 200.0,
                // Default speed in km/h when no speed is given
                "DEFAULT_SPEED", 15.0,
                // Incline angle that is considered to be steep - downhill or uphill
                // Threshold value is based on https://wiki.openstreetmap.org/wiki/Key:mtb:scale
                //      where the mtb:scale=0 is in [0%,10%], 10% ~ 6 degrees
                "STEEPNESS_THRESHOLD", 6.3
        ));
        return settings;
    }
}
