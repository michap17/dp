package cz.cvut.fel.michalik.navigation.instruction;

import cz.cvut.fel.michalik.navigation.Landmark;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class ApproachInstruction extends Instruction implements Serializable {

    @Override
    public List<InstructionToken> toInstructionTokens(Landmark landmark) {
        List<InstructionToken> list = new LinkedList<>();
        if (!this.isEmpty()) {
            list.add(new InstructionToken("approach-preposition", this.getLandmark().getPreposition().toString()));
            list.addAll(getLandmark().toInstructionTokens("approach"));
        }
        return list;
    }

    protected ApproachInstruction cloneInstruction(List<Landmark> landmarks) {
        ApproachInstruction instruction = new ApproachInstruction();
        instruction.landmarks = landmarks;
        return instruction;
    }
}
