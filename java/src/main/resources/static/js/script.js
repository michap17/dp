var map;
var route = null;
var landmarkDataset = null;
var markers = { 'start': null, 'end': null, 'waypoint': null };
let inputSelected = null;
const darkmode = (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches);
const colors = {
  start: getCssColor("--start"),
  end: getCssColor("--end"),
  route: getCssColor("--route"),
  decision: getCssColor("--decision"),
  approach: getCssColor("--approach"),
  confirmation: getCssColor("--confirmation")
};
let featureColors = {};
let lastTTSTimeout = null;
let lastAnimationTime = undefined;
let geolocate = null;
const queriedLayers = ['landmarks', 'landmarks-fill', 'landmarks-points'];
let wakeLock = null;
var host = "";

function getCssColor(name) {
  return getComputedStyle(document.documentElement).getPropertyValue(name);
}

window.onload = function () {
  // Create map object
  mapboxgl.accessToken = 'pk.eyJ1IjoicGV0ZTk3IiwiYSI6ImNreW42M25zMDJlbXAyd3Awa3YxenNhemcifQ.TbWJp0QO9yGLOCi-UDUtKg';
  map = new mapboxgl.Map({
    container: 'map',
    style: darkmode ? 'mapbox://styles/mapbox/dark-v10' : 'mapbox://styles/mapbox/light-v10',
    center: [14.39, 50.1], // starting position [lng, lat]
    zoom: 12.5, // starting zoom
    minZoom: 9,
    maxZoom: 20,
    hash: false,
    preserveDrawingBuffer: true // for screenshots
  });

  // Add controls to the map
  map.addControl(new mapboxgl.FullscreenControl());
  map.addControl(new mapboxgl.NavigationControl());
  // Add geolocate control to the map.
  geolocate = new mapboxgl.GeolocateControl({
    positionOptions: {
      enableHighAccuracy: true // May increase battery usage
    },
    trackUserLocation: true,
    showUserLocation: false,
    showUserHeading: false,
    fitBoundsOptions: {}
  });
  map.addControl(geolocate);
  geolocate.on('geolocate', (data) => {
    //console.log(data);                
    // Null heading == 0    
    // BACKGROUND geolocate state = user interacted with the map and it is no longer centered around
    // ACTIVE_LOCK geolocate state = user did not interact with the map
    moveWaypointMarker([data.coords.longitude, data.coords.latitude], data.coords.heading, false);
    // Set start location on this position (if empty)
    const start = document.querySelector("#start").value;
    const end = document.querySelector("#end").value;
    if (start == "" && end == "") {
      let contextmenu = document.querySelector("#context-menu");
      contextmenu.dataset.lng = data.coords.longitude;
      contextmenu.dataset.lat = data.coords.latitude;
      selectPointInMap({}, 'startSelected')
    }
  });
  geolocate.on('trackuserlocationstart', () => {
    console.log('A trackuserlocationstart event has occurred.');
    console.log(geolocate._watchState);
  });
  geolocate.on('trackuserlocationend', () => {
    console.log('A trackuserlocationend event has occurred.');
    console.log(geolocate._watchState);
  });
  /*map.addControl(new watergis.MapboxExportControl({
    PageSize: watergis.Size.A4,
    PageOrientation: watergis.PageOrientation.Landscape,
    Format: watergis.Format.PDF,
    DPI: watergis.DPI[96],
    Crosshair: true,
    PrintableArea: true,
  }), 'top-right');*/

  // Wait for map load
  map.on('load', function () {
    // Sky layer
    map.addLayer({
      'id': 'sky',
      'type': 'sky',
      'paint': {
        'sky-type': 'atmosphere',
        'sky-atmosphere-sun-intensity': 15
      }
    });
    // Bounds polygon, shows extents of the dataset
    map.addSource('bounds', {
      type: 'geojson',
      data: null
    });
    map.addLayer({
      'id': 'bounds',
      'type': 'fill',
      'source': 'bounds',
      'paint': {
        'fill-color': 'black',
        'fill-opacity': 0.5
      }
    });
    // Show bounds  
    fetch(`${host}/getBounds`)
      .then(r => r.json())
      .then(r => {
        let boundsGeometry = turf.mask(turf.polygon(r.coordinates));
        map.getSource('bounds').setData(boundsGeometry);
        map.fitBounds(turf.bbox(r), {
          padding: 5
        });
      });
    // Source with the route and a layer
    map.addSource('route', {
      type: 'geojson',
      data: null
    });
    map.addLayer({
      'id': 'route',
      'type': 'line',
      'source': 'route',
      'layout': {
        'line-cap': 'round',
        'line-join': 'round'
      },
      'paint': {
        'line-color': colors.route,
        'line-width': 5,
        'line-opacity': 0.9
      }
    });
    // Source with the landmarks
    map.addSource('landmarks', {
      type: 'geojson',
      data: null
    });
    // Layer for line and polygon landmarks 
    map.addLayer({
      'id': 'landmarks',
      'type': 'line',
      'source': 'landmarks',
      'layout': {
        'line-cap': 'round',
        'line-join': 'round'
      },
      'paint': {
        'line-color': ['get', 'color'],
        'line-width': ['get', 'width'],
        'line-opacity': 0.8
      }
    });
    // Layer for polygon landmarks
    map.addLayer({
      'id': 'landmarks-fill',
      'type': 'fill',
      'source': 'landmarks',
      'paint': {
        'fill-color': ['get', 'color'],
        'fill-opacity': 0.5
      },
      filter: ['in', '$type', 'Polygon']
    });
    // Layer for point landmarks, filtered points from the same source    
    map.addLayer({
      'id': 'landmarks-points',
      'type': 'circle',
      'source': 'landmarks',
      'paint': {
        'circle-color': ['get', 'color'],
        'circle-radius': [
          'interpolate',
          ['linear'],
          ['zoom'],
          // When zoom is 12 or less, points are smaller.
          11,
          2,
          18,
          16],
        'circle-opacity': 0.8
      },
      filter: ['in', '$type', 'Point']
    });
    // Enable inputing coordinates by clicking in the map
    // Inspired by this example https://docs.mapbox.com/mapbox-gl-js/example/measure/
    // Detect focusing and bluring of inputs        
    // Change cursor if the selection is possible
    map.on("mousemove", e => {
      let startFocus = document.querySelector("#start:focus") != null;
      let endFocus = document.querySelector("#end:focus") != null;
      if (startFocus || endFocus) {
        map.getCanvas().style.cursor = "crosshair";
      } else {
        const bbox = [
          [e.point.x - 5, e.point.y - 5],
          [e.point.x + 5, e.point.y + 5]
        ];
        // Find features intersecting the bounding box.
        const selectedFeatures = map.queryRenderedFeatures(bbox, {
          layers: queriedLayers
        });
        selectedFeatures.length == 0 ? map.getCanvas().style.cursor = "" : map.getCanvas().style.cursor = "pointer";
      }
    });
    // Possible selection of point in the map
    map.on("click", selectPointInMap);
    map.on("contextmenu", e => selectPointInMap(e, "contextmenu"));
    // Alternative ways, if contextmenu event won't work, inspired by https://stackoverflow.com/questions/43459539/mapbox-gl-js-long-tap-press
    let contextMenuTimeout = null;
    let clearContextMenuTimeout = (e) => {
      clearTimeout(contextMenuTimeout);
      contextMenuTimeout = null;
    }
    map.on("touchstart", (e) => {
      if (contextMenuTimeout == null) contextMenuTimeout = setTimeout(() => selectPointInMap(e, "contextmenu"), 500);
    });
    map.on('touchend', clearContextMenuTimeout);
    map.on('touchcancel', clearContextMenuTimeout);
    map.on('pointerdrag', clearContextMenuTimeout);
    map.on('pointermove', clearContextMenuTimeout);
    map.on('moveend', clearContextMenuTimeout);
    map.on('gesturestart', clearContextMenuTimeout);
    map.on('gesturechange', clearContextMenuTimeout);
    map.on('gestureend', clearContextMenuTimeout);
    map.on('drag', clearContextMenuTimeout);
    map.on('zoom', clearContextMenuTimeout);
    map.on('rotate', clearContextMenuTimeout);
    // Clicking on the map container does not blur any input
    // User can drag around the map while having some input selected
    document.querySelector("canvas.mapboxgl-canvas").onmousedown = e => e.preventDefault();
    // Context menu can be used for picking start and end
    document.querySelectorAll("#context-menu p")[0].onclick = e => selectPointInMap(e, "startSelected");
    document.querySelectorAll("#context-menu p")[1].onclick = e => selectPointInMap(e, "endSelected");
    // If any input field from search panel is focused, show a backdrop highlighting this input
    document.querySelector("#start").addEventListener("focus", () => {
      document.querySelector("#backdrop").classList.add("active");
      document.querySelector("#search-start").classList.add("active");
    });
    document.querySelector("#start").addEventListener("blur", () => {
      document.querySelector("#backdrop").classList.remove("active");
      document.querySelector("#search-start").classList.remove("active");
    });
    document.querySelector("#end").addEventListener("focus", () => {
      document.querySelector("#backdrop").classList.add("active");
      document.querySelector("#search-end").classList.add("active");
    });
    document.querySelector("#end").addEventListener("blur", () => {
      document.querySelector("#backdrop").classList.remove("active");
      document.querySelector("#search-end").classList.remove("active");
    });    
    // Show debug panel, if previously enabled
    if (localStorage.getItem("debug") === "true") {      
      toggleDebugPanel(true);      
    }    
    // Enable TTS, if previously not disabled
    if (localStorage.getItem("tts") !== "false") {
      document.querySelector("#ttsSwitch").checked = true;
    }
    // Check the query parameters and fill the field if present
    handleHistoryPopstate(null);
    // If browser supports WakeLock API, use it (user can change it later in the settings menu)
    if (navigator.wakeLock !== null) {
      let wlToggle = document.querySelector("#wakeLockToggle");
      toggleWakeLock(wlToggle);
      wlToggle.disabled = false;
      wlToggle.checked = true;
    }
  });
}

function findRoute(e) {
  if (e != null) {
    e.preventDefault();
    e.stopPropagation();
  }
  const urlNew = new URL(window.location);
  const start = document.querySelector("#start").value;
  const end = document.querySelector("#end").value;
  if (map == null || start == "" || end == "") {
    return;
  }
  // If there was any route shown, remove the parameter highlighting some specific instruction
  if (route !== null && urlNew.searchParams.has("instruction")) {
    urlNew.searchParams.delete('instruction');
    urlNew.searchParams.delete('route');
    window.history.pushState({}, '', urlNew);
    handleHistoryPopstate();
    return;
  }
  const url = `${host}/getRoute${urlNew.search}`;
  routeQuery(url);
}

function findPredefinedRoute(routeId) {
  const speed = document.querySelector("#speed").value;
  const url = `${host}/getRoute?routeId=${routeId}&speed=${speed}`;
  const urlNew = new URL(window.location);
  urlNew.searchParams.delete('start');
  urlNew.searchParams.delete('end');
  urlNew.searchParams.delete('distWeight');
  urlNew.searchParams.set('speed', speed);
  urlNew.searchParams.set('route', routeId);
  window.history.pushState({}, '', urlNew);
  routeQuery(url);
}

function routeQuery(url) {
  //console.trace();
  fetch(url)
    .then(r => {
      if (!r.ok) throw r;
      return r.json();
    })
    .then(json => {
      console.log(json);
      route = json;
      showRoute();
    })
    .catch(err => {
      console.error(err);
      document.querySelector("#msg").innerHTML = "Search failed";
      document.querySelector("#msg").classList.add("error");
    });
}

function showRoute() {
  // Clear previous route
  if (route !== null) clearRoute();
  // Show the route
  map.getSource('route').setData(route['geometry']);
  // Show start and end markers
  addRouteMarkers(route['geometry']);
  // Show the instructions
  let listEl = document.querySelector("#instructions ol");
  listEl.innerHTML = "";
  route['instruction'].forEach((instruction, idx) => {
    return listEl.innerHTML += `
    <li onclick="instructionClick(${idx})" class="${instruction.json.type.toLowerCase()}" id="instruction-${idx}" data-idx="${idx}">
      <div class="icon">&nbsp;</div>
      <div class="content">
      ${instruction['tokens'].map(token => "<span class='" + token['type'] + ('id' in token ? (" id-" + token['id']) : "") + "'>" + token['text'] + "</span>").join(" ")}
      </div>
    </li>`;
  });
  // Section with instructions is shown
  document.querySelector("#instructions").classList.remove("hidden");
  // Show route length and number of instructions used
  let routeLengthText = route['length'];
  document.querySelector("#instructions-header h3").textContent = `${routeLengthText} (${route['timeString']}), ${route['numDecisionPoints']} decision points`;
  // Hide the search form for small screens
  document.querySelector("#search").classList.add("hidden");
  // Hide the message box above the search form
  document.querySelector("#msg").innerHTML = "";
  document.querySelector("#msg").classList.remove("error");
  // Enable toggle in the settings section for showing route landmarks and animation
  document.querySelector("#routeLandmarks").disabled = false;
  document.querySelector("#routeAnimationSwitch").disabled = false;
  console.log(document.querySelector("#routeAnimationSwitch").disabled);
  // Uncheck toggles in settings
  untoggleLandmarkSwitches();
  // Zoom either to the whole route, or select some instruction based on URL  
  let params = (new URL(window.location.href)).searchParams;
  let idx = Number(params.get('instruction'));
  if (params.get('instruction') != null && idx >= 0 && idx < route.instruction.length) {
    map.once('sourcedata', (e) => {
      instructionClick(idx);
    });
  } else {
    zoomTo(route.geometry.features);
  }
  // Show referenced landmarks
  showAllLandmarks(true, true);
}



function instructionClick(idx, zoom = true) {
  // Make (if any) active instruction inactive and remove all color underlines
  let prevActiveEl = document.querySelector("#instructions-list li.active");
  if (prevActiveEl != null) {
    Array.from(document.querySelectorAll("#instructions-list li.active span")).forEach(el => el.style = null);
    prevActiveEl.classList.remove("active");
  }
  if (idx < 0) return;
  // Untoggle any switch, they share the same map, it gets overwritten by this function
  else untoggleLandmarkSwitches();
  // Make the clicked instructions active
  let instructionElement = document.querySelector(`#instruction-${idx}`);
  instructionElement.classList.add("active");
  // If it is not fully visible, scroll it into view
  instructionElement.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" });
  // Get the referenced instruction
  let instructionJson = route['instruction'][idx]['json'];
  // Show it in the debug panel
  showDebugData([{ ...instructionJson, ...instructionJson.properties }], false);
  console.log(instructionJson)
  let coord = instructionJson.waypoint.coordinates;
  // Change waypoint marker 
  if (zoom) {
    if (markers['waypoint'] == null) {
      // Add marker with decision point
      markers['waypoint'] = new mapboxgl.Marker({ color: colors.route, scale: 1 })
        .setLngLat(coord)
        .addTo(map);
    } else {
      markers['waypoint'].setLngLat(coord);
    }
  }
  // Show landmarks (if available)    
  let landmarkFeatures = addFeatures(instructionJson, idx);
  let routeFeatures = [{ type: 'Feature', geometry: instructionJson.waypoint }, { type: 'Feature', geometry: instructionJson.waypointShifted }];
  routeFeatures = routeFeatures.concat(landmarkFeatures[0])
  if (instructionJson.type == "Decision") routeFeatures = routeFeatures.concat(route.geometry.features.slice(Math.min(instructionJson['startIdx'], route.geometry.features.length - 1), instructionJson['endIdx'] + 1));
  // Move to the marker and the oncoming edge
  //if(zoom) map.flyTo({center: coord});
  if (zoom) zoomTo(routeFeatures, 3);
  // Text-to-speech using browser API
  let ttsEnabled = document.querySelector("#ttsSwitch").checked;
  if (ttsEnabled) {
    if (lastTTSTimeout) clearTimeout(lastTTSTimeout);
    lastTTSTimeout = setTimeout(() => useTTS(idx), 10);
  }
  // Add URL parameter
  const urlNew = new URL(window.location);
  urlNew.searchParams.set('instruction', idx);
  window.history.pushState({}, '', urlNew);
}

// Gets exising color from the featureColors object or creates a new one
let getLandmarkColor = (id) => {
  let idx2color = i => `hsl(${50 + 151 * i},40%,50%)`;
  if (!(id in featureColors)) featureColors[id] = idx2color(Object.keys(featureColors).length)
  return featureColors[id];
};

function addFeatures(instructionJson, instructionId, rewriteData = true) {
  let features = [];
  let returnedFeatures = [];
  let textDecorationPreset = 'underline 2px';
  let lineWidth = 5;
  if ('instruction' in instructionJson && instructionJson['instruction']['landmarks'].length > 0) {
    for (lmIdx = 0; lmIdx < instructionJson['instruction']['landmarks'].length; lmIdx++) {
      let landmark = instructionJson['instruction']['landmarks'][lmIdx];
      let id = landmark['id'];
      // Highlight the landmark in the text instruction with the same color as in the map
      // If the landmark is not present in the text, it is not visualized
      let color = getLandmarkColor(id);
      let textTokens = document.querySelectorAll(`#instruction-${instructionId} span.id-${id}`);
      if (textTokens.length != 0) {
        Array.from(textTokens).forEach(el => {
          el.style.textDecoration = `${textDecorationPreset} ${color}`;
        });
        let landmarkGeojson = { type: 'Feature', geometry: route['landmark'][landmark['id']]['geometry'], properties: { id: id, color: color, width: lineWidth, ...landmark } };
        features.push(landmarkGeojson);
        let landmarkClosestLine = { type: 'Feature', geometry: landmark['closestLine'], properties: { color: color, width: 1 } };
        features.push(landmarkClosestLine);
        returnedFeatures.push(landmarkClosestLine);
      }
    }
  }
  // Show landmarks of inline next item (if present)
  if ('nextItem' in instructionJson && rewriteData) {
    let [returnedFeaturesNext, featuresNext] = addFeatures(instructionJson['nextItem'], instructionId, false);
    returnedFeatures = [...returnedFeatures, ...returnedFeaturesNext];
    features = [...features, ...featuresNext];
  }
  if (rewriteData) {
    let featuresCollection = { type: 'FeatureCollection', features: features };
    map.getSource("landmarks").setData(featuresCollection);
  }
  return [returnedFeatures, features]
}


// Modified function from https://docs.mapbox.com/mapbox-gl-js/example/zoomto-linestring/
function zoomTo(features, paddingFactor = 1) {
  // Create a 'LngLatBounds' with both corners at the first coordinate.
  const bounds = new mapboxgl.LngLatBounds();
  // Extend the 'LngLatBounds' to include every coordinate in the bounds result.
  for (feature of features) {
    if (feature.geometry.type == "Point") {
      bounds.extend(feature.geometry.coordinates);
    } else {
      coordinates = feature.geometry.coordinates;
      for (const coord of coordinates) {
        bounds.extend(coord);
      }
    }
  }

  paddingBase = Math.min(window.innerHeight, window.innerWidth);
  map.fitBounds(bounds, {
    padding: (paddingBase / 20) * paddingFactor
  });
}

function addRouteMarkers(geojson) {
  const firstCoordinate = geojson.features[0].geometry.coordinates[0];
  const lastCoordinate = geojson.features.at(-1).geometry.coordinates.at(-1);

  markers['start'] = new mapboxgl.Marker({ color: colors.start })
    .setLngLat(firstCoordinate)
    .addTo(map);

  markers['end'] = new mapboxgl.Marker({ color: colors.end })
    .setLngLat(lastCoordinate)
    .addTo(map);
}

function useTTS(idx) {
  if (!window.speechSynthesis) {
    document.querySelector("#msg").innerHTML = "Your browser does not support SpeechSynthesis API";
  }
  let synth = window.speechSynthesis;
  if (synth.speaking) synth.cancel();
  let voices = synth.getVoices();
  let engVoices = voices.filter(v => v.lang.startsWith("en"));
  let czVoices = voices.filter(v => v.lang.startsWith("cs"));
  // At least one English voice or Czech voice
  if (engVoices.length > 0 || czVoices.length > 0) {
    let engVoice = engVoices.length > 0 ? engVoices[0] : czVoices[0];
    let czVoice = czVoices.length > 0 ? czVoices.at(0) : engVoices[0];
    let lastText = "";
    let lastVoice = null;
    let speak = () => {
      // Don't say anything if you've got nothing to say (and maybe fix a bug in Edge)
      if (lastText == "") return;
      utterance = new SpeechSynthesisUtterance(lastText);
      utterance.voice = lastVoice;
      console.log(`Speaking "${lastText}" in ${lastVoice.name}`)
      // Fix for Firefox: https://stackoverflow.com/questions/39391502/js-speechsynthesis-problems-with-the-cancel-method
      synth.speak(utterance);
    }
    // Split the text by the language parts
    route.instruction[idx].tokens.forEach(token => {
      let voice = token.type.includes("name") ? czVoice : engVoice;
      // Removing diacritics https://stackoverflow.com/a/37511463, it might make the Czech names easier for TTS
      let text = (czVoice.lang.startsWith("cs")) ? token.text : token.text.normalize("NFD").replace(/\p{Diacritic}/gu, "");
      if (lastVoice == null) lastVoice = voice;
      if (lastVoice.name != voice.name) {
        speak();
        lastVoice = voice;
        lastText = "";
      }
      if (token.type == "length") lastText += " " + (text.indexOf("km") > 0 ? text.replace("km", "kilometers") : text.replace("m", "meters"));
      else lastText += (lastText.length > 0 ? " " : "") + text;
    });
    if (lastText.length > 0) speak()
  } else {
    document.querySelector("#msg").innerHTML = "Your device does not have any Text to Speech language. Use for example <a href='https://play.google.com/store/apps/details?id=com.google.android.tts'>Speech Services by Google</a>";
  }
}

function clearRoute(clearInputs = false) {
  // Remove instructions
  document.querySelector("#instructions-list").innerHTML = "";
  // Remove markers
  console.log("Removing markers");
  for (let m in markers) {
    if (markers[m] !== null) {
      markers[m].remove()
      markers[m] = null;
    };
  }
  // Clear route
  map.getSource("route").setData({ type: "FeatureCollection", features: [] });
  // If the search part is hidden, show it again
  document.querySelector("#search").classList.remove("hidden");
  // Hide the section with instructions
  document.querySelector("#instructions").classList.add("hidden");
  // Disable and uncheck the toggle in the settings section for showing route landmarks and animation
  document.querySelector("#routeLandmarks").disabled = true;
  document.querySelector("#routeLandmarks").checked = false;
  document.querySelector("#routeAnimationSwitch").disabled = true;
  document.querySelector("#routeAnimationSwitch").checked = false;
  // Clear landmarks
  map.getSource("landmarks").setData({ type: "FeatureCollection", features: [] });
  if (clearInputs) {
    // Clear values in input fields
    Array.from(document.querySelectorAll("#search input")).forEach(el => el.value = "");
    // Rewrite URL by removing all route related parameters
    const urlNew = new URL(window.location);
    urlNew.searchParams.delete('start');
    urlNew.searchParams.delete('end');
    urlNew.searchParams.delete('instruction');
    urlNew.searchParams.delete('route');
    window.history.pushState({}, '', urlNew);
  }
  // Reset simulation progress
  document.querySelector("#routeAnimation").value = 0;
  document.querySelector("#routeAnimation").disabled = true;
}

function selectPointInMap(e, type) {
  let contextmenu = document.querySelector("#context-menu");
  if (type == "contextmenu") {
    contextmenu.classList.remove("hidden");
    contextmenu.style.left = `${e.originalEvent.clientX || e.point.x}px`;
    contextmenu.style.top = `${e.originalEvent.clientY || e.point.y}px`;
    contextmenu.dataset.lng = e.lngLat.lng
    contextmenu.dataset.lat = e.lngLat.lat
    return;
  }
  if (type == "startSelected" || type == "endSelected") {
    e.lngLat = { lng: parseFloat(contextmenu.dataset.lng), lat: parseFloat(contextmenu.dataset.lat) };
  }
  document.querySelector("#context-menu").classList.add("hidden");
  let startSelected = type == "startSelected" || (document.querySelector("#start:focus") != null && type != "endSelected");
  let endSelected = type == "endSelected" || (document.querySelector("#end:focus") != null && type != "startSelected");
  let startField = document.querySelector("#start");
  let endField = document.querySelector("#end");
  if (startSelected || endSelected) {
    map.getCanvas().style.cursor = "";
    let url = `${host}/getClosestNode?lng=${e.lngLat.lng.toFixed(6)}&lat=${e.lngLat.lat.toFixed(6)}`;
    fetch(url)
      .then(r => {
        if (!r.ok) throw r;
        return r.json();
      })
      .then(json => {
        if (startSelected) {
          if (markers['start'] === null) {
            markers['start'] = new mapboxgl.Marker({ color: colors.start })
              .setLngLat([json.lng, json.lat])
              .addTo(map);
          } else {
            markers['start'].setLngLat([json.lng, json.lat])
          }
          startField.value = json.id;
          changeRoutingParameter(document.querySelector("#start"));
          startField.blur();
          //if(endField.value == "") endField.focus();          
        } else {
          if (markers['end'] === null) {
            markers['end'] = new mapboxgl.Marker({ color: colors.end })
              .setLngLat([json.lng, json.lat])
              .addTo(map);
          } else {
            markers['end'].setLngLat([json.lng, json.lat]);
          }
          endField.value = json.id;
          changeRoutingParameter(document.querySelector("#end"));
          endField.blur();
        }
        // Hide intro message
        document.querySelector("#msg").innerHTML = "";
      })
      .catch(err => console.error(err));
  } else {
    const bbox = [
      [e.point.x - 5, e.point.y - 5],
      [e.point.x + 5, e.point.y + 5]
    ];
    // Find features intersecting the bounding box.
    const selectedFeatures = map.queryRenderedFeatures(bbox, {
      layers: queriedLayers
    });
    showDebugData(selectedFeatures.map(feature => feature.properties), true);
  }
}

function showDebugData(data, isLandmark = true) {
  if(isLandmark) toggleDebugPanel(true);
  let dataEl = document.querySelector("#details-data");
  // Final HTML content is string concatenated
  dataEl.innerHTML = '';
  let skippedKeys = ['color', 'width', 'closestLine', 'instruction', 'properties'];
  // Area landmark can appear multiple times, it is in both 'landmarks' and 'landmarks-fill' layers
  // But the data should be shown just once
  let usedKeys = [];
  data.forEach(feature => {
    let props = Object.entries(feature).filter(([key, value]) => skippedKeys.indexOf(key) < 0);
    if (props.length > 0 && ('id' in feature || !isLandmark) && usedKeys.indexOf(Number(feature['id'])) < 0) {
      let id = Number(feature['id']);
      usedKeys.push(id);
      let htmlEntry = '';
      htmlEntry += `<table style="border-left: 2px solid ${feature['color']}"><tbody>`;
      props.forEach(([key, value]) => {
        if (key == 'id' && isLandmark) {
          let geojsonType;
          if (landmarkDataset != null) geojsonType = landmarkDataset[id].geometry.type;
          else geojsonType = route.landmark[id].geometry.type;
          let type = geojsonType == 'Point' ? 'node' : ((geojsonType == 'LineString' || id >= 0) ? 'way' : 'relation');
          let url = `https://www.openstreetmap.org/${type}/${Math.abs(value)}`;
          htmlEntry += `<tr><td>${key}</td><td><a href="${url}" target="_blank">${value}</a></td></tr>`;
        } else if (typeof (value) !== "object") {
          htmlEntry += `<tr><td>${key}</td><td>${value}</td></tr>`;
        }
      });
      htmlEntry += '</tbody></table><hr>';
      dataEl.innerHTML += htmlEntry;
    }
  });
}

function untoggleLandmarkSwitches() {
  let allToggleElements = Array.from(document.querySelectorAll("#settings input[type=checkbox].landmarks"));
  allToggleElements.forEach(el => el.checked = false);
}

function toggleSwitched(checkbox) {
  let allToggleElements = Array.from(document.querySelectorAll("#settings input[type=checkbox].landmarks"));
  // Uncheck other toggles, they are mutually exclusive, since they share one map layer  
  if (checkbox.checked) {
    untoggleLandmarkSwitches();
    checkbox.checked = true;
    instructionClick(-1);
  }
  if (checkbox.id == "allLandmarks" && checkbox.checked) {
    showAllLandmarks(fromRoute = false);
  } else if (checkbox.id == "routeLandmarks" && checkbox.checked) {
    showAllLandmarks(fromRoute = true);
  } else {
    let isAnyToggled = allToggleElements.some(el => el.checked);
    if (!isAnyToggled) {
      // Clear landmarks
      map.getSource("landmarks").setData({ type: "FeatureCollection", features: [] });
    }
  }
}

function toggleDebugPanel(show) {
  let container = document.querySelector("#container");  
  if (show && !container.classList.contains("debug")) {
    container.classList.add("debug");
    localStorage.setItem("debug", true);
  } else if(!show && container.classList.contains("debug")) {
    container.classList.remove("debug");
    localStorage.setItem("debug", false);
  }
  // Window is the same, but the map needs resizing
  handleMapResize();
}

function changeTTS(checkbox) {
  if (checkbox.checked) {
    localStorage.setItem("tts", true);
  } else {
    localStorage.setItem("tts", false);
  }
}

function changeRoutingParameter(inputRange) {
  // Speed value has an output field showing exact value
  if (inputRange.id === "speed") {
    document.querySelector('#speedValue').value = inputRange.value;
  }
  const urlNew = new URL(window.location);
  urlNew.searchParams.set(inputRange.id, inputRange.value);
  window.history.pushState({}, '', urlNew);
  handleHistoryPopstate(null);
}


async function showAllLandmarks(fromRoute = true, onlyReferenced = false) {
  let features = [];
  let src;
  if (fromRoute) {
    if (route == null) return false;
    src = route['landmark'];
  } else {
    if (landmarkDataset == null) {
      const url = `${host}/getAllLandmarks`;
      await fetch(url)
        .then(r => {
          if (!r.ok) throw r;
          return r.json();
        })
        .then(json => {
          landmarkDataset = json;
        });
    }
    src = landmarkDataset;
  }
  for (landmarkEntry of Object.entries(src)) {
    let id = landmarkEntry[0];
    if (onlyReferenced && document.querySelectorAll(`.id-${id}`).length == 0) continue;
    let landmark = landmarkEntry[1];
    let color = fromRoute ? getLandmarkColor(id) : (id in featureColors ? featureColors[id] : colors.route);
    features.push({ type: 'Feature', geometry: landmark.geometry, properties: { id: id, color: color, width: 3, ...landmark.properties } });
  }
  let featuresCollection = { type: 'FeatureCollection', features: features };
  map.getSource("landmarks").setData(featuresCollection);
  return true;
}

function handleHistoryPopstate(event) {
  // Check the query parameters and fill the field if present
  let params = (new URL(window.location.href)).searchParams;
  // Route ID is a parameter that shows a predefined route
  if (params.get('speed') != null) {
    document.querySelector("#speed").value = params.get('speed');
    document.querySelector("#speedValue").value = params.get('speed');
  }
  if (params.get('route') != null) findPredefinedRoute(params.get('route'));
  else {
    for (const [key, value] of params) {
      if (['speed', 'route'].indexOf(key) >= 0) continue;
      let el = document.querySelector(`#${key}`);
      if (el !== null) document.querySelector(`#${key}`).value = value;
    }
  }
  console.log(`Loading data from url: ${params}`);
  // Try submitting the route, if possible
  findRoute(null, true);
}

function handleMapResize(event) {
  setTimeout(() => map.resize(event), 1000);
}

function handleKeyboardShortcuts(event) {
  // Do not process shortcut, if there is no route or the map is still moving
  if (route == null || map.isMoving()) return;
  let nextKeys = ['ArrowRight', 'd'];
  let prevKeys = ['ArrowLeft', 'a'];
  let key = event.key;
  let prevActiveEl = document.querySelector("#instructions-list li.active");
  if (nextKeys.indexOf(key) >= 0) {
    let idx = -1;
    if (prevActiveEl != null) {
      idx = Number(prevActiveEl.dataset['idx']);
    }
    if (idx + 1 < route.instruction.length) instructionClick(idx + 1);
  }
  else if (prevKeys.indexOf(key) >= 0) {
    let idx = route.instruction.length;
    if (prevActiveEl != null) {
      idx = Number(prevActiveEl.dataset['idx']);
    }
    if (idx - 1 >= 0) instructionClick(idx - 1);
  }
}

/**
 * Moves waypoint (blue) marker, rotates map and triggers any nearby instruction
 * @param {*} coords [lat,lng] coordinates
 * @param {*} bearing Heading in degrees [-180,180]
 * @param {*} animationSpeed Value between [0,1] - 1=no animation, 0.01=slow transition
 */
function moveWaypointMarker(coords, newBearing = null) {
  if (markers['waypoint'] == null) {
    markers['waypoint'] = new mapboxgl.Marker({ color: colors.route, scale: 1 })
      .setLngLat(coords)
      .addTo(map);
  } else {
    markers['waypoint'].setLngLat(coords);
  }

  let zoomLevel = 18;
  if (!map._moving) {
    if (newBearing != null) {
      // rotate map according to the given new bearing
      let currentBearing = map.getBearing();
      let relativeBearingDelta = ((newBearing - currentBearing + 360 + 180) % 360) - 180;
      let bearing = currentBearing + relativeBearingDelta;
      // Setting eventData.geolocateSource will not cause the the GeolocateControl to change to background state
      // https://github.com/mapbox/mapbox-gl-js/blob/0d95cc525deacbb578c92ed948386fa8475cb7d5/src/ui/control/geolocate_control.js#L307
      map.flyTo({ center: coords, bearing: bearing, zoom: zoomLevel }, { geolocateSource: true })
    } else {
      map.flyTo({ center: coords, zoom: zoomLevel }, { geolocateSource: true })
    }
  }


  // Check instructions
  // and trigger the instruction on the waypoint that is shifted back (the instruction is completed before the waypoint)
  // The turf/nearest library could be used, effectivity is roughly the same: https://github.com/turf-junkyard/turf-nearest/blob/master/index.js#L63
  // The for loop is used for speed, as described in https://stackoverflow.com/a/30834687  
  if (route != null) {
    const coordsPoint = turf.point(coords)
    let distanceTrigger = 0.01; // 10 meters
    let instructionSelected = (new URL(window.location.href)).searchParams.get("instruction");
    let closestIdx = 0;
    let minDist = turf.distance(route.instruction[0].json.waypointShifted, coordsPoint);
    for (let i = 1; i < route.instruction.length; ++i) {
      let dist = turf.distance(route.instruction[i].json.waypointShifted, coordsPoint);
      if (dist < minDist) {
        minDist = dist;
        closestIdx = i;
      }
    }
    if (minDist < distanceTrigger && instructionSelected != closestIdx) {
      instructionClick(closestIdx, zoom = false);
    }
  }
}

function changeRouteAnimation(checkbox) {
  lastAnimationTime = undefined;
  if (checkbox.checked) {
    const animationDuration = route.time * 1000;

    document.querySelector("#routeAnimation").min = 0;
    document.querySelector("#routeAnimation").max = Math.ceil(animationDuration);
    document.querySelector("#routeAnimation").step = 1;
    document.querySelector("#routeAnimation").disabled = false;

    window.requestAnimationFrame(routeAnimationFrame);
  }
}

function routeAnimationFrame(time) {
  const targetRoute = turf.lineString(route.geometry.features.map(el => el.geometry.coordinates).flat());
  const animationDuration = route.time * 1000;
  // get the overall distance of each route so we can interpolate along them
  const routeDistance = turf.lineDistance(targetRoute);

  let running = document.querySelector("#routeAnimationSwitch").checked;
  if (!running) return;

  let timeStep = Number(document.querySelector("#routeAnimation").value);

  if (!lastAnimationTime) lastAnimationTime = time;

  // phase determines how far through the animation we are
  const phase = (timeStep) / animationDuration;

  // phase is normalized between 0 and 1
  // when the animation is finished, end the whole animation loop and highlight the last instruction
  if (phase > 1) {
    let instructionSelected = (new URL(window.location.href)).searchParams.get("instruction");
    if (instructionSelected != route.instruction.length - 1) instructionClick(route.instruction.length - 1, zoom = false);
    document.querySelector("#routeAnimation").value = 0;
    document.querySelector("#routeAnimationSwitch").checked = false;
    return;
  }

  // use the phase to get a point that is the appropriate distance along the route
  // this approach syncs the camera and route positions ensuring they move
  // at roughly equal rates even if they don't contain the same number of points
  const alongRoutePoint = turf.along(
    targetRoute,
    routeDistance * phase
  )
  const alongRouteCoords = alongRoutePoint.geometry.coordinates;

  let point1 = alongRoutePoint;
  let point2 = turf.along(targetRoute, routeDistance * ((timeStep + 10) / (route.time * 1000)));
  let currentBearing = map.getBearing();
  let newBearing = turf.bearing(point1, point2);  
  let relativeBearingDelta = ((newBearing - currentBearing + 360 + 180) % 360) - 180;    
  let bearing = currentBearing + 0.05*relativeBearingDelta;


  moveWaypointMarker(alongRouteCoords, bearing);

  // Move animation time slider
  document.querySelector("#routeAnimation").value = timeStep + (time - lastAnimationTime);
  lastAnimationTime = time;

  window.requestAnimationFrame(routeAnimationFrame);
}

function toggleWakeLock(wlToggle) {
  if (navigator.wakeLock) {
    if (wakeLock !== null) {
      wakeLock.release();
      wakeLock = null;
      console.log("WakeLock released");
    } else {
      navigator.wakeLock.request('screen').then((wl) => {
        wakeLock = wl;
        console.log("WakeLock acquired");
      });
    }
  }
}

// Resize map when the size of the window changes
window.addEventListener("resize", handleMapResize);
// Watch URL change using History API
window.onpopstate = handleHistoryPopstate;
// Keyboard shortcuts
document.addEventListener('keydown', handleKeyboardShortcuts);
// Load TTS voices
console.log(window.speechSynthesis.getVoices());