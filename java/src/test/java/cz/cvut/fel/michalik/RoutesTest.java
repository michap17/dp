package cz.cvut.fel.michalik;

import cz.cvut.fel.michalik.navigation.*;
import cz.cvut.fel.michalik.navigation.instruction.InstructionToken;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.locationtech.jts.geom.Geometry;

import java.io.*;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Testset = {TestCase1,TestCase2, ...}
 * Testcase = {Requirement1,}
 */

public class RoutesTest {

    /*
     * Landmarks is in CNF form: (group_1) AND (group_2) ..., where group = l1 OR l2
     * This is because one real landmark can be represented by multiple elements - polygon and point
     *   or a public transport stop can have multiple platforms, etc.
     */
    public static final List<TestCase> testCases = List.of(
            new TestCase(3, "Vokovice -> Zahradnictví Chládek",
                    List.of(
                            Pair.of("relation/8308536", "Gymnázium Arabská"),
                            Pair.of("way/22635267;way/22635266", "police station"),
                            Pair.of("node/4719188957;way/180465704;node/55538877;way/548761682;relation/2437837", "Sídliště Červený Vrch stop"),
                            Pair.of("node/1909037962;way/180465716;way/180465710;node/3414583439;relation/2415016", "Červený Vrch stop"),
                            Pair.of("relation/8267963", "Základní škola a mateřská škola Červený vrch"),
                            Pair.of("way/371733348", "ZŠ a MŠ Na Dlouhém lánu"),
                            Pair.of("way/8085377;node/282736943", "railroad tracks / crossing"),
                            Pair.of("way/318003493;way/65639051", "Zahradnictví Chládek")
                    )),
            new TestCase(7, "Veleslavín -> Džbán",
                    List.of(
                            Pair.of("node/3688348028;node/3414583403;node/3449320305;node/5316448454;node/5155737183", "Nádraží Veleslavín stop"),
                            Pair.of("node/9109958351", "fuel station MOL"),
                            Pair.of("relation/8268336", "ZŠ Vokovice"),
                            Pair.of("node/2578636964", "playground"),
                            Pair.of("node/3732517330;way/318015760", "SK Aritma"),
                            Pair.of("way/365359592", "camp Džbán"),
                            Pair.of("way/29985104", "parking"),
                            Pair.of("relation/5431533", "Džbán")
                    )),
            new TestCase(4, "Lotyšská -> Kafkova",
                    List.of(
                            Pair.of("node/3413902181;node/3413902182;way/153528609;way/153528613", "Lotyšská stop"),
                            Pair.of("way/405845545", "CIIRC"),
                            Pair.of("node/282690498", "Pizzeria Grosseto"),
                            Pair.of("node/756569272", "Cyklo 69"),
                            Pair.of("node/55552358;node/5686152363;node/4719188956;node/4518007395;relation/2454089", "Vítězné náměstí stop"),
                            Pair.of("node/1250854420", "Kafkova stop")
                    )),
            new TestCase(0, "FEL ČVUT -> Studentský dům",
                    List.of(
                            Pair.of("way/12220199", "FEL ČVUT"), Pair.of("way/28090508", "NTK"),
                            Pair.of("way/405845543;way/100959765", "FA"), Pair.of("node/9566872590;way/99013221;way/1038858634", "FSv"),
                            Pair.of("way/25550769", "Studentský dům")
                    )),
            new TestCase(1, "Stromovka -> Troja",
                    List.of(
                            Pair.of("relation/9204470", "pond Srpeček"),
                            Pair.of("way/981696630;way/239776758;way/25738285", "railway bridge"),
                            Pair.of("way/13331942", "bridge Císařský ostrov"),
                            Pair.of("way/95350189", "Jezdecká hala"),
                            Pair.of("way/12221515", "Trojská lávka"),
                            Pair.of("node/3448110894", "Občerstvení U lávky"),
                            Pair.of("way/23607662", "Trojský zámek")
                    )),
            new TestCase(2, "Juliska -> Podbaba",
                    List.of(
                            Pair.of("way/25552026", "ÚTVS"), Pair.of("way/30147457", "Stanice techniků"),
                            Pair.of("way/1000625667", "Armáda ČR"), Pair.of("node/332218800", "Restaurace Pod Juliskou"),
                            Pair.of("node/1662077479;node/8157899805;way/153528625;way/153528630;node/3413709752", "Nádraží Podbaba stop")
                    )),
            new TestCase(5, "Chotkovy sady -> Letenské sady",
                    List.of(
                            Pair.of("way/25401460", "bridge"),
                            Pair.of("way/61973096", "Kramářova vila"),
                            Pair.of("way/28701169", "cafe Hanavský pavilon"),
                            Pair.of("node/5027320564;relation/12273886", "Stalinův pomník / Metronom"),
                            Pair.of("way/25401471", "Letenský zámeček")
                    )),
            new TestCase(6, "Veleslavín -> Kafkova",
                    List.of(
                            Pair.of("way/7653685", "playground"),
                            Pair.of("way/134664205", "Dejvický potok"),
                            Pair.of("way/371733348", "ZŠ a MŠ Na Dlouhém lánu"),
                            Pair.of("way/7652745", "Stadion Hvězda"),
                            Pair.of("way/32108335;way/32108334;way/371733345", "SK Střešovice"),
                            Pair.of("node/358463677;node/358463642", "Pod Vyhlídkou stop"),
                            Pair.of("relation/2571994", "Kino Ořechovka"),
                            Pair.of("node/5724247013", "Dělostřelecká stop"),
                            Pair.of("node/2338683459", "Klubovna")
                    )),
            new TestCase(8, "Špitálka -> Jenerálka",
                    List.of(
                            Pair.of("relation/2427455;node/92049293;node/92049291", "Špitálka stop"),
                            Pair.of("way/30132249", "power substation"),
                            Pair.of("relation/10619589;way/26377899;way/32114840;way/449447618;way/684849446;way/684849447;way/684849448;way/26377906;way/959123898;way/1011684530;way/1011684529;way/959123897", "Šárecký potok"),
                            Pair.of("way/61007950", "bridge"),
                            Pair.of("node/390377563;node/390377562", "Kalinův mlýn stop"),
                            Pair.of("node/390377561;node/390377560", "Kuliška stop"),
                            Pair.of("node/390377558;node/390377557", "Korek stop"),
                            Pair.of("node/8811002917", "restaurant Jenerálka")
                    )),
            new TestCase(9, "Stromovka -> Planetárium",
                    List.of(
                            Pair.of("node/6909296641", "artwork"),
                            Pair.of("way/1018486001", "playground"),
                            Pair.of("way/24399672", "Maroldovo panorama"),
                            Pair.of("way/22634016", "Planetárium")
                    ))
    );
    private static final boolean TEX_OUTPUT = true;
    static String FOLDER = "tests/";
    private static Graph g;

    @BeforeAll
    static void before() {
        g = new Graph();
        try {
            System.out.println("Loading graph");
            g.loadFromGeoJSON((String) Properties.getSettings().get("FILENAME"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        ;
    }

    public static List<TestCase> getTestCases() {
        return testCases;
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource("getTestCases")
    void testCase(TestCase testCase) {
        PrintStream outTable;
        if (TEX_OUTPUT) {
            try {
                outTable = new PrintStream(String.format("%s/test%02d.tex", FOLDER, testCase.id + 1));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return;
            }
        } else {
            outTable = System.out;
        }
        if (!TEX_OUTPUT) assertEquals(0, testCase.test(g, outTable), String.format("Test case %s failed", testCase));
        else {
            new File(FOLDER).mkdirs();
            testCase.test(g, outTable);
        }
    }

    @Test
    @DisplayName("Outputting routes as TeX (for appendix)")
    void testPrintRoutesTeX() throws FileNotFoundException {
        Map<Itinerary.ItineraryItem.ItineraryItemType, String> instructionTypeSymbols = Map.of(
                Itinerary.ItineraryItem.ItineraryItemType.Decision, "\\vspan1.5{\\picw=11pt \\inspic icons/decision.png }",
                Itinerary.ItineraryItem.ItineraryItemType.Approach, "\\vspan1.5{\\picw=11pt \\inspic icons/approach.png }",
                Itinerary.ItineraryItem.ItineraryItemType.Confirmation, "\\vspan1.5{\\picw=11pt \\inspic icons/confirmation.png }"
        );
        PrintStream outRoute = new PrintStream(String.format("%s/routes.tex", FOLDER));
        for(int i = 0; i < testCases.size(); ++i){
            TestCase testCase = testCases.get(i);
            int id = i + 1;
            outRoute.printf("""                            
                    \\midinsert
                        \\picw=1.0\\hsize \\cinspic screens/%d_map.png
                        \\caption/f \\ulink[https://michap17.cz.eu.org/?route=%d]{Route %d}
                    \\endinsert                    
                    \\label[appendix-route-id-%d]
                    \\sec Route %d: %s
                    \\thistable={\\tabstrut={\\vrule height 16pt depth0pt width0pt}}
                    \\table{cp{0.9\\hsize\\fL}}{             
                    """,
                    testCase.id + 1, testCase.id, id, testCase.id + 1, id, testCase.name.replace("->", "--"));
            List<Geometry> geoms = Utils.geoJSONCollectionToGeometries(new StringReader(testCase.geoJSON));
            Path path = Path.pathFromEdges(g, geoms);
            Itinerary itinerary = new Itinerary(path, g, (double) Properties.getSettings().get("DEFAULT_SPEED"));
            Route route = new Route(g, path, itinerary);
            List<Itinerary.ItineraryItem> items = route.reduceItinerary().getItineraryList();
            for(Itinerary.ItineraryItem item : items){
                String itemString = item.toString();
                if(itemString.isEmpty()) continue;
                if(itemString.length() >= 74){
                    outRoute.printf("\\vspan2.85{\\picw=11pt \\inspic icons/decision2.png }  & %s \\cr %n", itemString);
                }else {
                    outRoute.printf("%s  & %s \\cr %n", instructionTypeSymbols.get(item.getType()), itemString);
                }
            }
            outRoute.printf("}%n%n");
        }
        outRoute.close();
    }

    @Test
    @DisplayName("Enrichment statistics")
    void testAll() {
        PrintStream out;
        if (TEX_OUTPUT) {
            try {
                out = new PrintStream(String.format("%s/stats.tex", FOLDER));
                out.print("\\thistable={\\tabstrut={\\vrule height 12pt depth5pt width0pt}}\n\n\\midinsert\n\\label[route-test-stats]\n\\ctable{lrr}{\n\tData & Absolute value & Relative value \\crl\n");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return;
            }
        } else {
            out = System.out;
        }
        int errorCount = 0;
        int totalCount = 0;
        for (TestCase testCase : testCases) {
            errorCount += testCase.test(g, null);
            totalCount += testCase.landmarks.size();
        }
        int successCount = totalCount - errorCount;
        if (TEX_OUTPUT) {
            DecimalFormat df = new DecimalFormat();
            df.setMinimumFractionDigits(2);
            df.setMaximumFractionDigits(2);
            df.setRoundingMode(RoundingMode.HALF_UP);
            df.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.ENGLISH));
            out.printf("\tTest cases success rate & %d / %d & %s\\thinspace\\%% \\cr\n", successCount, totalCount, df.format(100.0 * successCount / totalCount));
            long numEdges = g.getEdges().size();
            long diLandmarks = g.getEdges().stream().filter(edge -> edge.getDecisionInstruction().getLandmarks().size() > 0).count();
            List<String> roadInfoKeywords = List.of("bridge", "stairs", "crosswalk", "sidewalk", "hill", " ");
            List<String> dpRoadInfo = new LinkedList<>();
            long roadTotal = 0;
            long roadChanged = 0;
            for (Edge edge : g.getEdges()) {
                for (Edge edge2 : edge.getTarget().getAdjacentEdges()) {
                    roadTotal += 1;
                    if (g.isSameClass(edge, edge2)) continue;
                    roadChanged += 1;
                    Itinerary.ItineraryItem item = new Itinerary.ItineraryItem(Itinerary.ItineraryItem.ItineraryItemType.Decision, edge, 0, 0);
                    item.setProperties(g.getEdgeProperties(edge.getId()));
                    item.setRoadClassChanged();
                    List<InstructionToken> roadTokens = item.getRoadClassTokens();
                    String roadText = String.join(" ", roadTokens.stream().map(token -> token.getText()).toList());
                    for (String kw : roadInfoKeywords) {
                        if (roadText.contains(kw)) {
                            dpRoadInfo.add(kw);
                            break;
                        }
                    }
                }
            }
            long ciLandmarks = g.getEdges().stream().filter(edge -> edge.getConfirmationInstruction().getLandmarks().size() > 0).count();
            long hasBestContinuation = g.getEdges().stream().filter(edge -> edge.hasBestContinuation()).count();
            long junctionsTshape = g.getEdges().stream().filter(edge -> edge.getJunctionType().equals(Graph.JunctionType.T_shape)).count();
            long junctionsYshape = g.getEdges().stream().filter(edge -> edge.getJunctionType().equals(Graph.JunctionType.Y_shape)).count();
            long junctionsFourway = g.getEdges().stream().filter(edge -> edge.getJunctionType().equals(Graph.JunctionType.Fourway)).count();
            printStatsRow(out, "Decision points with any landmark", diLandmarks, numEdges, df);
            printStatsRow(out, "Decision points with road change", roadChanged, roadTotal, df);
            printStatsRow(out, "Decision points with any road information", dpRoadInfo.size(), roadChanged, df);
            for (String kw : roadInfoKeywords) {
                String keyword = kw.equals(" ") ? "Road class" : StringUtils.capitalize(kw);
                printStatsRow(out, String.format("\\qquad %s", keyword), dpRoadInfo.stream().filter(s -> s.equals(kw)).count(), roadChanged, df);
            }
            printStatsRow(out, "Junctions with distinct shape", (junctionsTshape + junctionsYshape + junctionsFourway), numEdges, df);
            printStatsRow(out, "\\qquad T junctions", junctionsTshape, numEdges, df);
            printStatsRow(out, "\\qquad Y junctions", junctionsYshape, numEdges, df);
            printStatsRow(out, "\\qquad Four-way junctions", junctionsFourway, numEdges, df);
            printStatsRow(out, "Confirmation instructions with any landmark", ciLandmarks, numEdges, df);
            printStatsRow(out, "Edges with best continuation", hasBestContinuation, numEdges, df);
        } else {
            out.printf("\nTotal errors: %02d/%02d = %f%%\n", errorCount, totalCount, 100.0 * errorCount / totalCount);
        }
        if (TEX_OUTPUT) {
            out.print("}\n\\caption/t Statistics of map data\n\\endinsert\n\n");
        }
    }

    private void printStatsRow(PrintStream out, String text, long nominator, long denominator, DecimalFormat df) {
        out.printf("\t%s & %d / %d & %s\\thinspace\\%% \\cr\n", text, nominator, denominator, df.format(100.0 * nominator / denominator));
    }

    private static class TestCase {
        int id;
        String name;
        String geoJSON;
        List<Pair<String, String>> landmarks;

        public TestCase(int id, String name, List<Pair<String, String>> landmarks) {
            this.id = id;
            this.name = name;
            this.geoJSON = Route.getPredefinedRoutes().get(id);
            this.landmarks = landmarks;
        }

        @Test
        @DisplayName("Test")
        public int test(Graph g, PrintStream outTable) {
            List<Geometry> geoms = Utils.geoJSONCollectionToGeometries(new StringReader(this.geoJSON));
            Path path = Path.pathFromEdges(g, geoms);
            Itinerary itinerary = new Itinerary(path, g, (double) Properties.getSettings().get("DEFAULT_SPEED"));
            Route route = new Route(g, path, itinerary);
            List<Itinerary.ItineraryItem> items = route.reduceItinerary().toList().stream().map(i -> (Itinerary.ItineraryItem) i.get("json")).toList();
            List<String> missed = new LinkedList<>();
            if (outTable != null && TEX_OUTPUT) {
                outTable.printf("\\thistable={\\tabstrut={\\vrule height 12pt depth5pt width0pt}}\n\n\\midinsert\n\\label[route-test-%d]\n\\ctable{p{0.35\\hsize\\fL}p{0.45\\hsize\\fL}c}{\n\tLandmark & OpenStreetMap IDs & Present \\crl\n", id + 1);
            }
            for (Pair<String, String> landmarkPair : landmarks) {
                // List of Pairs ([node|way|relation], ID)
                List<Pair<String, Long>> landmarkIds = Arrays
                        .stream(landmarkPair.getLeft().split(";"))
                        .map(lm -> {
                            String[] temp = lm.split("/");
                            assert temp.length > 1;
                            return Pair.of(temp[0], Long.parseLong(temp[1]));
                        }).toList();
                String landmarkName = landmarkPair.getRight();
                boolean present = false;
                for (Itinerary.ItineraryItem item : items) {
                    present = isLandmarkPresent(item, landmarkIds.stream().map(lm -> lm.getRight()).toList());
                    if (present) {
                        break;
                    }
                }
                if (!present) missed.add(landmarkName);
                if (outTable != null && TEX_OUTPUT) {
                    /*String gjson = g.getAllLandmarks().get(id).geometry.toString();
                    Geometry geom = Utils.geoJSONToGeometry(gjson);
                    String type = geom.getGeometryType().equals("Point") ? "node" : ((geom.getGeometryType().equals("LineString") || id >= 0) ? "way" : "relation");*/
                    String idsString = landmarkIds.stream()
                            .map(lm -> String.format("\\ulink[https://openstreetmap.org/%s/%d]{%d}", lm.getLeft(), lm.getRight(), lm.getRight()))
                            .collect(Collectors.joining(", "));

                    outTable.printf("\t%s & %s & %s \\cr\n",
                            landmarkName, idsString, present ? "{\\localcolor\\Green \\faCheck}" : "{\\localcolor\\Red \\faTimes}");
                }
            }
            String testUrl = String.format("https://michap17.cz.eu.org/?route=%d", id);
            if (outTable != null && TEX_OUTPUT) {
                outTable.printf("}\n\\caption/t Landmarks for route \\ref[appendix-route-id-%d]\n\\endinsert\n\n", id+1);
            } else if (outTable != null) {
                missed.forEach(landmark -> outTable.printf("\t%s\n", landmark));
                outTable.printf("[%s] %02d/%02d\t(%s)\n", name, landmarks.size() - missed.size(), landmarks.size(), testUrl);
            }
            return missed.size();
        }

        private boolean isLandmarkPresent(Itinerary.ItineraryItem item, List<Long> landmarkIds) {
            // Relations have negative IDs
            return item.hasLandmark() && landmarkIds.contains(Math.abs(item.getInstruction().getLandmark().getId()));
        }

        @Override
        public String toString() {
            return name;
        }
    }

}
