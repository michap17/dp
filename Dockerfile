# syntax=docker/dockerfile:1
FROM ubuntu:jammy-20211122

# Install prerequisites
ARG DEBIAN_FRONTEND="noninteractive"
ARG TZ="Europe/Prague"
RUN  apt-get update && apt-get -y install \
  maven \
  openjdk-17-jdk \
  openjdk-17-jre \
  osm2pgsql \
  postgis \
  postgresql-14 \
  postgresql-14-pgrouting \
  postgresql-14-postgis-3* \
  proj-bin \
 && rm -rf /var/lib/apt/lists/*

USER postgres
# Copy files
COPY . /osm
WORKDIR /osm

# Start database and create a user
RUN    /etc/init.d/postgresql start &&\
      psql --command "CREATE USER petr WITH SUPERUSER PASSWORD 'post';"

USER root
# The RUN.sh file is run ...
ENTRYPOINT ["bash" "RUN.sh"]
# ... and default argument is file prague.osm
CMD ["prague.osm"]