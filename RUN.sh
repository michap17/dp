#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

if [[ $# -eq 0 ]]; then
    echo "Usage: ./RUN.sh file.osm"
    exit 1
fi

# Create an exported geojson file, if it does not exist
if [ -e "$(ls -A java/*.geojson 2>/dev/null)" ]
then
    # PostgreSQL is not running in a Docker container after starting the container
    /etc/init.d/postgresql start
    echo "Importing OSM file"
    su -c "bash import_db.sh $1" postgres    
    echo "Processing file"
    bash process_db.sh    
    echo "Done"
fi

echo "Running API"
cd java
mvn clean compile exec:java -Dexec.mainClass="cz.cvut.fel.michalik.api.Application"
cd ..



