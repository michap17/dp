#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

# Run java and write results to GeoJSON
cd java
export MAVEN_OPTS="-Xmx1G"
mvn clean compile exec:java -Dexec.mainClass="cz.cvut.fel.michalik.Main"
cd ..
