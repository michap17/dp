#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

if [[ $# -eq 0 ]]; then
    echo "Usage: ./import_db.sh file.osm"
    exit 1
fi

# Add SRTM data
OSM_FILE=${1/./_srtm.}
#./osmosis/bin/osmosis --read-xml $1 --write-srtm locDir=. --write-xml $OSM_FILE

# Export PGDATABASE variable
export PGDATABASE='prague'

dropdb prague --if-exists
createdb prague
psql -c 'CREATE EXTENSION postgis; CREATE EXTENSION hstore; CREATE EXTENSION pgrouting;'
# -j adds all tags to a hstore column called tags, even if they’re already stored in a conventional column
# --extra-attributes adds timestamps
# --multi-geometry does not split split multi-part geometries into separate database rows per part
osm2pgsql -c -j --extra-attributes --multi-geometry $OSM_FILE

# Using osm2po to extract routable graph
cd osm2po
java -Xmx1g -jar osm2po-core-5.5.2-signed.jar prefix=prague cmd=tjsp tileSize=x workDir=/tmp/osm2po ../$OSM_FILE postp.0.class=de.cm.osm2po.plugins.postp.PgRoutingWriter desktop
cd ..
# Import graph into DB
psql -f /tmp/osm2po/prague_2po_4pgr.sql
rm -rf /tmp/osm2po
# Create database with less columns and create indexes
psql -c "CREATE TABLE planet_osm_ways AS SELECT id, prague_2po_4pgr.osm_id, osm_name, source, target, osm_source_id, osm_target_id, geom_way AS way, clazz, SIGN(reverse_cost - cost) AS oneway FROM prague_2po_4pgr;"
psql -c "DROP TABLE prague_2po_4pgr;"
psql -c "ALTER TABLE planet_osm_ways ADD CONSTRAINT planet_osm_ways_pkey PRIMARY KEY(id);"
psql -c "CREATE INDEX planet_osm_ways_source_idx ON planet_osm_ways(source);"
psql -c "CREATE INDEX planet_osm_ways_target_idx ON planet_osm_ways(target);"
# Round the oneway to integer (-1,0,1)
psql -c "ALTER TABLE planet_osm_ways ALTER COLUMN oneway TYPE integer;"

# Reproject everything
psql -c "ALTER TABLE planet_osm_point ALTER COLUMN way TYPE geometry(Point, 3035) USING ST_Transform(way, 3035);"
psql -c "ALTER TABLE planet_osm_line ALTER COLUMN way TYPE geometry(LineString, 3035) USING ST_Transform(way, 3035);"
psql -c "ALTER TABLE planet_osm_roads ALTER COLUMN way TYPE geometry(LineString, 3035) USING ST_Transform(way, 3035);"
psql -c "ALTER TABLE planet_osm_polygon ALTER COLUMN way TYPE geometry(Geometry, 3035) USING ST_Transform(way, 3035);"
psql -c "ALTER TABLE planet_osm_ways ALTER COLUMN way TYPE geometry(Geometry, 3035) USING ST_Transform(way, 3035);"
