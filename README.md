# Master's Thesis
Generating context-enriched instructions for bicycle navigation

## Usage

`Dockerfile` can be used to create a docker image, which install all required dependencies (see Dependencies). Docker image can be built as follows: `docker build -t michap17/dp .`

The image can then be run as a container: `docker run --name dp -it -p 8080:8080 --mount type=bind,source="$(pwd)",target=/osm michap17/dp`

Next, the OpenStreetMap data has to be downloaded (for example, using  `wget  https://overpass-api.de/api/map?bbox=14.3378,50.0933,14.4292,50.1189 -O prague.osm`). The SRTM data can be obtained from https://lpdaac.usgs.gov/products/srtmgl3v003/ (NASA Earthdata Login Credentials may be required).

Resulting file `export.geojson` is saved in the `java` folder. The Web application then loads and builds a graph from it. 
The Web application can be manually run using the `mvn clean compile exec:java -Dexec.mainClass="cz.cvut.fel.michalik.api.Application"` command (part of the `RUN.sh` bash script)

The Web application is also available at https://michap17.cz.eu.org/.


## Project structure

 * java - our implementation
 * osm2po - osm2po tool for creating routable graph (https://osm2po.de)
 * osmosis - osmosis tool for height data enrichment (https://github.com/openstreetmap/osmosis)

## Dependencies
 * Java (both JDK and JRE) >= 17
 * PostgreSQL >= 14
 * PostGIS >= 3.0
 * osm2pgsql
 * proj-bin
 * Maven
 * pgRouting >= 3.0.0
